#!/bin/bash
set -e
curl -sL https://github.com/digitalocean/doctl/releases/download/v1.59.0/doctl-1.59.0-linux-amd64.tar.gz | tar -xzv
mv ./doctl /usr/bin
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x ./kubectl
mv ./kubectl /usr/bin
doctl auth init -t ${DO_LOGIN_TOKEN}
doctl kubernetes cluster kubeconfig save ${DO_KUBECTL_NS}