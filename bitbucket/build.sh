#!/bin/bash
set -x
CHANGED=$(git diff --exit-code HEAD~1..HEAD apps/$1)
if [ $? -eq 1 ] || [ $3 = "skipDiffCheck" ]
then
    docker login -u ${DOCKERHUB_USERNAME} -p ${DOCKERHUB_PASSWORD}
    docker build --no-cache --build-arg NPM_TOKEN=${NPM_TOKEN} -t $1 -f ./apps/$1/Dockerfile .
    docker save --output $1.docker $1
    docker load --input ./$1.docker
    # Tag containers
    export APP_IMAGE_NAME=${DOCKERHUB_USERNAME}/$2
    docker tag $1 ${APP_IMAGE_NAME}
    docker push ${APP_IMAGE_NAME}
else
    echo "No changes detected for $1. Skipping Docker build"
fi
