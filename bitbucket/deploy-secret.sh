#!/bin/bash
set -e
export TOKEN_VAR=DOPPLER_TOKEN_${BITBUCKET_DEPLOYMENT_ENVIRONMENT}
export DOPPLER_TOKEN=${!TOKEN_VAR}
(curl -Ls https://cli.doppler.com/install.sh || wget -qO- https://cli.doppler.com/install.sh) | sh
doppler secrets download --no-file --format docker > /dev/null
kubectl delete secret $1 -n sawayo-${BITBUCKET_DEPLOYMENT_ENVIRONMENT} --ignore-not-found
kubectl create secret generic $1 -n sawayo-${BITBUCKET_DEPLOYMENT_ENVIRONMENT} --from-env-file <(doppler secrets download --no-file --format docker)