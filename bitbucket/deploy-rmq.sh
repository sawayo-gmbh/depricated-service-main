#!/bin/bash
set -e
export TOKEN_VAR=DOPPLER_TOKEN_RMQ_$1
export DOPPLER_TOKEN=${!TOKEN_VAR}
(curl -Ls https://cli.doppler.com/install.sh || wget -qO- https://cli.doppler.com/install.sh) | sh
doppler secrets download --no-file --format docker > /dev/null
kubectl delete secret sawayo-rabbitmq-secret -n sawayo-$1 --ignore-not-found
kubectl create secret generic sawayo-rabbitmq-secret -n sawayo-$1 --from-env-file <(doppler secrets download --no-file --format docker)

kubectl apply -f ./k8s/yml-files/$1/rabbitmq.yml
kubectl rollout restart -n sawayo-$1 deployment sawayo-rabbitmq-deployment