#!/bin/bash
set -x
CHANGED=$(git diff --exit-code HEAD~1..HEAD apps/$2)
if [ $? -eq 1 ] || [ $2 = "service-main" ] || [ $3 = "skipDiffCheck" ]
then
    kubectl apply -f ./apps/$2/k8s/${BITBUCKET_DEPLOYMENT_ENVIRONMENT}/deployment.yaml
    kubectl rollout restart -n sawayo-${BITBUCKET_DEPLOYMENT_ENVIRONMENT} deployment $1
else
    echo "No changes detected for $2. Skipping Deployment"
fi
