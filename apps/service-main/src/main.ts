import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import helmet from 'helmet';

import { exceptionFactory, ExceptionFilter } from '@sawayo/exceptions';

import { ApplicationConfig, Config } from './config';
import { ServiceMainModule } from './service-main.module';

async function bootstrap() {
  const app = await NestFactory.create(ServiceMainModule);

  const configService = app.get<ConfigService<Config>>(ConfigService);
  const { port } = configService.get<ApplicationConfig>('application');

  app.use(helmet({ contentSecurityPolicy: false }));

  app.useGlobalFilters(new ExceptionFilter());
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory,
      forbidUnknownValues: true,
      transform: true,
    }),
  );

  await app.listen(port, '0.0.0.0');

  const logger = new Logger();

  logger.log(`service-main is running on ${await app.getUrl()}`);
}

bootstrap();
