export class GatewayResponse<D> {
  data?: D;
  error?: string;
  message?: string;
  session?: string;
}
