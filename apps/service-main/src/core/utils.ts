import { Types } from 'mongoose';

import { UserRole } from '../modules/users/infrastructure/entities/user.entity';

export interface Context {
  companyId: Types.ObjectId;
  role: UserRole;
  userId: Types.ObjectId;
}
