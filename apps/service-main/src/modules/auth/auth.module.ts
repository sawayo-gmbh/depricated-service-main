import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { GATEWAY_BUILD_SERVICE } from '@nestjs/graphql';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { TimeModule } from '@sawayo/time';

import { JwtConfigService } from '../../config';
import { UsersDomain, UsersModule } from '../users';

import { AuthenticatedDataSource, JwtAuthGuard } from './application';

@Module({
  exports: [GATEWAY_BUILD_SERVICE],
  imports: [TimeModule, PassportModule, JwtModule.registerAsync({ useClass: JwtConfigService }), UsersModule],
  providers: [
    { provide: APP_GUARD, useClass: JwtAuthGuard },
    {
      provide: AuthenticatedDataSource,
      useValue: AuthenticatedDataSource,
    },
    {
      inject: [AuthenticatedDataSource, JwtService, UsersDomain, ConfigService],
      provide: GATEWAY_BUILD_SERVICE,
      useFactory: (AuthenticatedDataSourceClass, JwtServiceClass, UsersDomainClass, ConfigServiceClass) => {
        return ({ url }) => {
          return new AuthenticatedDataSourceClass({ url }, JwtServiceClass, UsersDomainClass, ConfigServiceClass);
        };
      },
    },
  ],
})
export class AuthModule {}
