import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import FileUploadDataSource from '@profusion/apollo-federation-upload';
import { Types } from 'mongoose';

import {
  HEADER_CONTEXT_COMPANY_ID,
  HEADER_CONTEXT_IP,
  HEADER_CONTEXT_USER_ID,
  HEADER_CONTEXT_USER_ROLE,
} from '@sawayo/auth';

import { UsersDomain } from '../../users';

export class AuthenticatedDataSource extends FileUploadDataSource {
  private readonly jwtService: JwtService;
  private readonly usersDomain: UsersDomain;
  private readonly configService: ConfigService;

  constructor(config: any, jwtService: JwtService, usersDomain: UsersDomain, configService: ConfigService) {
    super(config);

    this.jwtService = jwtService;
    this.usersDomain = usersDomain;
    this.configService = configService;
  }

  private async getExtendedPayloadData(decoded: any): Promise<[companyId: Types.ObjectId, role: string]> {
    const { companyId, id: userId, role } = decoded;

    if (companyId && role) {
      return [companyId, role];
    }

    const { companyId: userCompanyId, role: userRole } = await this.usersDomain.getUserById({
      userId: Types.ObjectId(userId),
    });

    return [userCompanyId, userRole];
  }

  async willSendRequest({ context, request }) {
    const { ip, jwt } = context;

    request.http.headers.set(HEADER_CONTEXT_IP, ip);

    if (!jwt) {
      return;
    }

    const [, token] = jwt.split(' ');

    try {
      const decoded: any = await this.jwtService.verifyAsync(token);

      if (!decoded) {
        return;
      }

      const { id: userId } = decoded;
      const [companyId, role] = await this.getExtendedPayloadData(decoded);

      request.http.headers.set(HEADER_CONTEXT_COMPANY_ID, companyId);
      request.http.headers.set(HEADER_CONTEXT_USER_ID, userId);
      request.http.headers.set(HEADER_CONTEXT_USER_ROLE, role);
    } catch (ignored) {
      // Error thrown here will not be caught by exception filter
    }
  }
}
