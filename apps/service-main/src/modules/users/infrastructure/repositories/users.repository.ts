import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Repository } from '@sawayo/mongo';

import { UserEntity, UserDocument } from '../entities/user.entity';

@Injectable()
export class UsersRepository extends Repository<UserEntity, UserDocument> {
  constructor(@InjectModel(UserEntity.name) private userEntity: Model<UserDocument>) {
    super(userEntity, { baseClass: UserEntity });
  }
}
