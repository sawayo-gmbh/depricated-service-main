import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Transform } from 'class-transformer';
import { Document, Types } from 'mongoose';

export type UserDocument = UserEntity & Document;

export enum UserRole {
  COMPANY_EMPLOYEE = 'companyEmployee',
  COMPANY_OWNER = 'companyOwner',
  GUEST = 'guest',
}

export enum UserRegistrationType {
  APPLE = 'authApple',
  EMAIL = 'email',
  GOOGLE = 'authGoogle',
}

export interface GoogleAvatar {
  key: 'google';
  url: string;
}

export enum EmployeeStatus {
  ACTIVE = 'active',
  INACTIVE = 'inactive',
  UNCONFIRMED = 'unconfirmed',
}

export interface UserReport {
  key: string;
  url: string;
}

@Schema({ collection: 'users', timestamps: true, versionKey: false })
export class UserEntity {
  @Transform(({ obj }) => obj._id, { toClassOnly: true })
  readonly _id!: Types.ObjectId;

  @Prop({ required: true, unique: true })
  uniqueId: string;

  @Prop()
  firstName?: string;

  @Prop()
  lastName?: string;

  @Prop()
  birthDate?: Date;

  @Prop()
  companyId: Types.ObjectId;

  @Prop()
  chargebeeId?: string;

  @Prop({ lowercase: true, required: true, trim: true, unique: true })
  email: string;

  @Prop({ lowercase: true, trim: true })
  googleEmail?: string;

  @Prop({ lowercase: true, trim: true })
  appleEmail?: string;

  @Prop()
  password?: string;

  @Prop()
  mobilePassword?: string;

  @Prop({ default: UserRole.COMPANY_OWNER, type: String })
  role: UserRole;

  @Prop({ default: false })
  emailverified: boolean;

  @Prop({ default: UserRegistrationType.EMAIL, type: String })
  registrationType: UserRegistrationType;

  @Prop({ type: Object })
  avatar?: any;

  @Prop({ type: Object })
  googleAvatar?: GoogleAvatar;

  @Prop()
  phoneNumber: string;

  @Prop({ default: true })
  passwordIsNull: boolean;

  @Prop({ default: false })
  OTPIsSet: boolean;

  @Prop({ default: new Date() })
  lastSecurityUpdate: Date;

  @Prop({ default: new Date() })
  lastNotificationsUpdate: Date;

  @Prop({ default: new Date() })
  employeeCreatedAt: Date;

  @Prop()
  timeTrackingStartDate?: Date;

  @Prop()
  employeeId?: string;

  @Prop()
  jobTitle?: string;

  @Prop()
  department?: string;

  @Prop({ type: [{ ref: 'EmployeeGroup', type: Types.ObjectId }] })
  groups: Types.ObjectId[];

  @Prop()
  contractExpires?: Date;

  @Prop()
  comment?: string;

  @Prop({ default: EmployeeStatus.INACTIVE })
  employeeStatus: EmployeeStatus;

  @Prop({ default: false })
  informationHasBeenUpdated: boolean;

  @Prop({ type: Types.ObjectId })
  workingModel?: Types.ObjectId;

  @Prop({ type: Object })
  report?: UserReport;

  @Prop({ type: Object })
  activeNotifications: any;

  @Prop({ default: 'en' })
  activeLanguage: string;

  @Prop()
  workingTimeAccount?: string;

  @Prop()
  inviteSentDate?: Date;

  @Prop({ type: Types.ObjectId })
  workingTimeModelsHistory?: Types.ObjectId;

  @Prop({ required: false, type: Types.ObjectId })
  office: Types.ObjectId;

  constructor(init?: Partial<UserEntity>) {
    Object.assign(this, init);
  }
}

export const UserSchema = SchemaFactory.createForClass(UserEntity);
