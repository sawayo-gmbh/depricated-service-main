import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { UsersService } from './application';
import { UsersDomain } from './domain';
import { UserEntity, UserSchema, UsersRepository } from './infrastructure';
import { UsersController } from './presentation';

@Module({
  controllers: [UsersController],
  exports: [UsersDomain],
  imports: [MongooseModule.forFeature([{ name: UserEntity.name, schema: UserSchema }])],
  providers: [UsersDomain, UsersRepository, UsersService],
})
export class UsersModule {}
