import { Controller } from '@nestjs/common';

import { UsersService } from '../application';

@Controller('user')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}
}
