import { Injectable } from '@nestjs/common';

import { UsersRepository } from '../infrastructure';

import { GetUserByEmailParameters, GetUserByIdParameters } from './users-domain.interface';

@Injectable()
export class UsersDomain {
  constructor(private readonly usersRepository: UsersRepository) {}

  async getUserByEmail({ email }: GetUserByEmailParameters) {
    return this.usersRepository.findOneOrFail({ email });
  }

  async getUserById({ userId }: GetUserByIdParameters) {
    return this.usersRepository.findOneOrFail({ _id: userId });
  }
}
