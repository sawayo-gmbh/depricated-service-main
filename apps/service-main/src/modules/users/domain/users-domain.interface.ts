import { Types } from 'mongoose';

export interface GetUserByEmailParameters {
  email: string;
}

export interface GetUserByIdParameters {
  userId: Types.ObjectId;
}
