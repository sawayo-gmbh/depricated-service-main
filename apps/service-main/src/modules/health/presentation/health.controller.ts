import { Controller, Get } from '@nestjs/common';
import { HealthCheck, HealthCheckService, MemoryHealthIndicator, MongooseHealthIndicator } from '@nestjs/terminus';

import {
  MEMORY_HEALTH_INDICATOR_HEAP_KEY,
  MEMORY_HEALTH_INDICATOR_HEAP_THRESHOLD,
  MEMORY_HEALTH_INDICATOR_RSS_KEY,
  MEMORY_HEALTH_INDICATOR_RSS_THRESOLD,
  MONGOOSE_HEALTH_INDICATOR_KEY,
} from '@sawayo/health';

import { Public } from '../../../core/decorators';

@Controller('health')
export class HealthController {
  constructor(
    private readonly healthCheckService: HealthCheckService,
    private readonly memoryHealthIndicator: MemoryHealthIndicator,
    private readonly mongooseHealthIndicator: MongooseHealthIndicator,
  ) {}

  @Public()
  @HealthCheck()
  @Get()
  async check() {
    return this.healthCheckService.check([
      () =>
        this.memoryHealthIndicator.checkHeap(MEMORY_HEALTH_INDICATOR_HEAP_KEY, MEMORY_HEALTH_INDICATOR_HEAP_THRESHOLD),
      () => this.memoryHealthIndicator.checkRSS(MEMORY_HEALTH_INDICATOR_RSS_KEY, MEMORY_HEALTH_INDICATOR_RSS_THRESOLD),
      () => this.mongooseHealthIndicator.pingCheck(MONGOOSE_HEALTH_INDICATOR_KEY),
    ]);
  }
}
