import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GATEWAY_BUILD_SERVICE, GraphQLGatewayModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';

import { configuration, GraphQLConfigService, MongooseConfigService, validate } from './config';
import { AuthModule } from './modules/auth';
import { HealthModule } from './modules/health';
import { UsersModule } from './modules/users';

@Module({
  controllers: [],
  exports: [],
  imports: [
    ConfigModule.forRoot({ isGlobal: true, load: configuration, validate }),
    MongooseModule.forRootAsync({ useClass: MongooseConfigService }),
    GraphQLGatewayModule.forRootAsync({
      imports: [AuthModule],
      inject: [GATEWAY_BUILD_SERVICE],
      useClass: GraphQLConfigService,
    }),
    AuthModule,
    HealthModule,
    UsersModule,
  ],
  providers: [],
})
export class ServiceMainModule {}
