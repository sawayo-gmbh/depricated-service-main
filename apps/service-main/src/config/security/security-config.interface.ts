export interface SecurityConfig {
  jwtSecret: string;
  authJwtSecret: string;
}

export interface SecurityConfigSlice {
  security: SecurityConfig;
}
