import { SecurityConfigSlice } from './security-config.interface';

export const securityConfiguration = (): SecurityConfigSlice => {
  return {
    security: {
      jwtSecret: process.env.SERVICE_MAIN_SECURITY_JWT_SECRET,
      authJwtSecret: process.env.SERVICE_MAIN_AUTH_JWT_SECRET,
    },
  };
};
