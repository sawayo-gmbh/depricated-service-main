import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModuleOptions, JwtOptionsFactory } from '@nestjs/jwt';

import { Config } from '../config.interface';

import { SecurityConfig } from './security-config.interface';

@Injectable()
export class JwtConfigService implements JwtOptionsFactory {
  constructor(private readonly configService: ConfigService<Config>) {}

  createJwtOptions(): JwtModuleOptions {
    const { jwtSecret } = this.configService.get<SecurityConfig>('security');

    return {
      secret: jwtSecret,
      signOptions: { expiresIn: '9h' },
    };
  }
}
