import { applicationConfiguration } from './application';
import { databaseConfiguration } from './database';
import { graphQLConfiguration } from './graphql';
import { securityConfiguration } from './security';

export const configuration = [
  applicationConfiguration,
  databaseConfiguration,
  graphQLConfiguration,
  securityConfiguration,
];
