import { IsBoolean, IsJSON, IsNumber, IsString } from 'class-validator';

export class EnvironmentVariables {
  @IsNumber()
  SERVICE_MAIN_PORT: number;

  @IsString()
  SERVICE_MAIN_MONGO_URI: string;

  @IsBoolean()
  SERVICE_MAIN_GRAPHQL_PLAYGROUND: boolean;

  @IsJSON()
  SERVICE_MAIN_GRAPHQL_SERVICE_LIST: string;

  @IsString()
  SERVICE_MAIN_SECURITY_JWT_SECRET: string;

  @IsString()
  SERVICE_MAIN_AUTH_JWT_SECRET: string;

  @IsString()
  SERVICE_MAIN_GRAPHQL_ORIGINS: string;
}
