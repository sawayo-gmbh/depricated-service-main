import { ApplicationConfigSlice } from './application';
import { DatabaseConfigSlice } from './database';
import { GraphQLConfigSlice } from './graphql';
import { SecurityConfigSlice } from './security';

export interface Config extends ApplicationConfigSlice, DatabaseConfigSlice, GraphQLConfigSlice, SecurityConfigSlice {}
