import { ServiceEndpointDefinition } from '@apollo/gateway';

export interface GraphQLConfig {
  origins: string[];
  playground: boolean;
  serviceList: ServiceEndpointDefinition[];
}

export interface GraphQLConfigSlice {
  graphql: GraphQLConfig;
}
