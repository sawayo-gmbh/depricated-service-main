import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { GatewayModuleOptions, GatewayOptionsFactory } from '@nestjs/graphql';
import { Request } from 'express';

import { Config } from '../config.interface';

import { GraphQLConfig } from './graphql-config.interface';

interface ContextParams {
  req: Request;
}

@Injectable()
export class GraphQLConfigService implements GatewayOptionsFactory {
  constructor(private readonly configService: ConfigService<Config>) {}

  createGatewayOptions(): GatewayModuleOptions {
    const { playground, serviceList, origins } = this.configService.get<GraphQLConfig>('graphql');

    return {
      gateway: {
        serviceList,
      },
      server: {
        context: ({ req: request }: ContextParams) => {
          const { headers, ip } = request;
          const { authorization } = headers;

          return {
            ip,
            jwt: authorization,
          };
        },
        cors: {
          allowedHeaders: ['Content-Type', 'Authorization'],
          origin: origins,
        },
        introspection: playground,
        playground,
      },
    };
  }
}
