import { GraphQLConfigSlice } from './graphql-config.interface';

export const graphQLConfiguration = (): GraphQLConfigSlice => {
  return {
    graphql: {
      origins: JSON.parse(process.env.SERVICE_MAIN_GRAPHQL_ORIGINS),
      playground: process.env.SERVICE_MAIN_GRAPHQL_PLAYGROUND === 'true',
      serviceList: JSON.parse(process.env.SERVICE_MAIN_GRAPHQL_SERVICE_LIST),
    },
  };
};
