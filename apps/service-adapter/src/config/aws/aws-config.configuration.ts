import { AwsConfigSlice } from './aws-config.interface';

export const awsConfiguration = (): AwsConfigSlice => {
  return {
    aws: {
      accessKeyId: process.env.SERVICE_ADAPTER_AWS_ACCESS_KEY_ID,
      bucket: process.env.SERVICE_ADAPTER_AWS_BUCKET,
      endpoint: process.env.SERVICE_ADAPTER_AWS_ENDPOINT,
      expiresIn: parseInt(process.env.SERVICE_ADAPTER_AWS_EXPIRES_IN, 10),
      keyPrefix: process.env.SERVICE_ADAPTER_AWS_KEY_PREFIX,
      region: process.env.SERVICE_ADAPTER_AWS_REGION,
      secretAccessKey: process.env.SERVICE_ADAPTER_AWS_SECRET_ACCESS_KEY,
    },
  };
};
