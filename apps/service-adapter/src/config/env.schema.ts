import { IsBoolean, IsNumber, IsString } from 'class-validator';

export class EnvironmentVariables {
  @IsNumber()
  SERVICE_ADAPTER_PORT: number;

  @IsString()
  SERVICE_ADAPTER_AWS_ACCESS_KEY_ID: string;

  @IsString()
  SERVICE_ADAPTER_AWS_BUCKET: string;

  @IsString()
  SERVICE_ADAPTER_AWS_ENDPOINT: string;

  @IsNumber()
  SERVICE_ADAPTER_AWS_EXPIRES_IN: number;

  @IsString()
  SERVICE_ADAPTER_AWS_KEY_PREFIX: string;

  @IsString()
  SERVICE_ADAPTER_AWS_REGION: string;

  @IsString()
  SERVICE_ADAPTER_AWS_SECRET_ACCESS_KEY: string;

  @IsString()
  SERVICE_ADAPTER_MONGO_URI: string;

  @IsBoolean()
  SERVICE_ADAPTER_GRAPHQL_PLAYGROUND: boolean;

  @IsString()
  SERVICE_ADAPTER_RABBITMQ_URI: string;
}
