import { ApplicationConfigSlice } from './application-config.interface';

export const applicationConfiguration = (): ApplicationConfigSlice => {
  return {
    application: {
      port: parseInt(process.env.SERVICE_ADAPTER_PORT, 10),
    },
  };
};
