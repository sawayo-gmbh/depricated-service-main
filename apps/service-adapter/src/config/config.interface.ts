import { ApplicationConfigSlice } from './application';
import { AwsConfigSlice } from './aws';
import { DatabaseConfigSlice } from './database';
import { GraphQLConfigSlice } from './graphql';
import { RabbitMQConfigSlice } from './rabbitmq';

export interface Config
  extends ApplicationConfigSlice,
    AwsConfigSlice,
    DatabaseConfigSlice,
    GraphQLConfigSlice,
    RabbitMQConfigSlice {}
