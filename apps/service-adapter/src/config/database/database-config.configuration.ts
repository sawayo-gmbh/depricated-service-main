import { DatabaseConfigSlice } from './database-config.interface';

export const databaseConfiguration = (): DatabaseConfigSlice => {
  return {
    database: {
      uri: process.env.SERVICE_ADAPTER_MONGO_URI,
    },
  };
};
