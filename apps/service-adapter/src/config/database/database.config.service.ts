import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MongooseModuleOptions, MongooseOptionsFactory } from '@nestjs/mongoose';

import { Config } from '../config.interface';

import { DatabaseConfig } from './database-config.interface';

@Injectable()
export class MongooseConfigService implements MongooseOptionsFactory {
  constructor(private readonly configService: ConfigService<Config>) {}

  createMongooseOptions(): MongooseModuleOptions {
    const { uri } = this.configService.get<DatabaseConfig>('database');

    return {
      uri,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };
  }
}
