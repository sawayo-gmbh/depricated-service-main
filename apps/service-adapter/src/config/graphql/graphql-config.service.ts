import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { GqlModuleOptions, GqlOptionsFactory } from '@nestjs/graphql';
import { ApolloLogPlugin } from 'apollo-log';

import { Config } from '../config.interface';

import { GraphQLConfig } from './graphql-config.interface';

@Injectable()
export class GraphQLConfigService implements GqlOptionsFactory {
  constructor(private readonly configService: ConfigService<Config>) {}

  createGqlOptions(): GqlModuleOptions {
    const { playground } = this.configService.get<GraphQLConfig>('graphql');

    return {
      autoSchemaFile: true,
      buildSchemaOptions: {
        orphanedTypes: [],
      },
      introspection: playground,
      playground,
      plugins: [
        ApolloLogPlugin({
          events: {
            didEncounterErrors: true,
            didResolveOperation: false,
            executionDidStart: false,
            parsingDidStart: false,
            requestDidStart: false,
            responseForOperation: false,
            validationDidStart: false,
            willSendResponse: false,
          },
          timestamp: true,
        }),
      ],
      sortSchema: true,
    };
  }
}
