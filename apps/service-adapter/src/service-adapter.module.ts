import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GraphQLFederationModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';

import { GqlAuthMiddleware } from '@sawayo/auth';
import { AwsModule } from '@sawayo/aws';
import { ObjectIdScalar } from '@sawayo/graphql';

import { AwsConfigService, configuration, GraphQLConfigService, MongooseConfigService, validate } from './config';
import { AmqpModule } from './modules/amqp';
import { HealthModule } from './modules/health';
import { UserModule } from './modules/user';

@Module({
  controllers: [],
  exports: [],
  imports: [
    ConfigModule.forRoot({ isGlobal: true, load: configuration, validate }),
    AwsModule.registerAsync({ useClass: AwsConfigService }),
    GraphQLFederationModule.forRootAsync({ useClass: GraphQLConfigService }),
    MongooseModule.forRootAsync({ useClass: MongooseConfigService }),
    AmqpModule,
    HealthModule,
    UserModule,
  ],
  providers: [ObjectIdScalar],
})
export class ServiceAdapterModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(GqlAuthMiddleware).forRoutes('/graphql');
  }
}
