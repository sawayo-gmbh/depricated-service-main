import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';

import { exceptionFactory, ExceptionFilter } from '@sawayo/exceptions';

import { Config, ApplicationConfig } from './config';
import { ServiceAdapterModule } from './service-adapter.module';

async function bootstrap() {
  const logger = new Logger();

  const app = await NestFactory.create(ServiceAdapterModule);

  const configService = app.get<ConfigService<Config>>(ConfigService);
  const { port } = configService.get<ApplicationConfig>('application');

  app.useGlobalFilters(new ExceptionFilter());
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory,
      forbidUnknownValues: false,
      transform: true,
    }),
  );

  await app.listen(port);

  logger.log(`Application is running on ${await app.getUrl()}`);
}

bootstrap();
