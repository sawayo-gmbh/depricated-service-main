import { MessageHandlerErrorBehavior, RabbitRPC } from '@golevelup/nestjs-rabbitmq';
import { Injectable, Logger } from '@nestjs/common';
import { ConsumeMessage } from 'amqplib';
import { Types } from 'mongoose';

import { Exchange, Queue, RoutingKey } from '@sawayo/event-bus';
import { GetEmployeeByIdRequest, GetEmployeeByIdResponse } from '@sawayo/event-bus/services/adapter';

import { UserService } from '../application';

@Injectable()
export class UserHandler {
  private readonly logger: Logger = new Logger(UserHandler.name);

  constructor(private readonly userService: UserService) {}

  private async handleGetEmployeeById({ employeeId }: GetEmployeeByIdRequest): Promise<GetEmployeeByIdResponse> {
    const employee: any = await this.userService.getUserById({
      userId: Types.ObjectId.createFromHexString(employeeId),
    });

    return { employee };
  }

  @RabbitRPC({
    errorBehavior: MessageHandlerErrorBehavior.NACK,
    exchange: Exchange.direct,
    queue: Queue.adapterUsers,
    routingKey: [RoutingKey.getEmployeeById],
  })
  async handleUserRpc(request: any, amqpMessage: ConsumeMessage): Promise<GetEmployeeByIdResponse> {
    const { fields } = amqpMessage;
    const { routingKey } = fields;

    if (routingKey === RoutingKey.getEmployeeById) {
      return this.handleGetEmployeeById(request);
    }

    this.logger.warn(`Unknown routing key: ${routingKey}`);

    return null;
  }
}
