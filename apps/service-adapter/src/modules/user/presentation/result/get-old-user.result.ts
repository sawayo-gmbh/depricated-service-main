import { Field, ObjectType } from '@nestjs/graphql';

import { UserDto } from '../dto';
import { GetOldUserError } from '../error';

@ObjectType()
export class GetOldUserResult {
  @Field(() => UserDto, { nullable: true })
  data?: UserDto;

  @Field(() => GetOldUserError, { nullable: true })
  error?: GetOldUserError;
}
