import { Injectable, UseGuards } from '@nestjs/common';
import { Args, Parent, Query, ResolveField, Resolver, ResolveReference } from '@nestjs/graphql';
import { Types } from 'mongoose';

import { GqlAuthGuard, Role, Roles } from '@sawayo/auth';
import { AwsService } from '@sawayo/aws';
import { FileDataDto } from '@sawayo/graphql';

import { UserService } from '../application';
import { UserFederationReference } from '../core/types';

import { UserDto } from './dto';
import { GetOldUserInput } from './input';
import { GetOldUserResult } from './result';

@Injectable()
@UseGuards(GqlAuthGuard)
@Resolver(() => UserDto)
export class UserResolver {
  constructor(private readonly userService: UserService, private readonly awsService: AwsService) {}

  // Needed to add dto to schema
  @Roles(Role.companyOwner)
  @Query(() => GetOldUserResult, { description: 'User from old data model', name: 'user' })
  async getOldUser(@Args('input') input: GetOldUserInput): Promise<GetOldUserResult> {
    const { userId } = input;

    const data = await this.userService.getUserById({ userId });

    return { data };
  }

  @ResolveField(() => FileDataDto, { name: 'avatar', nullable: true })
  async resolveAvatar(@Parent() userDto: UserDto): Promise<FileDataDto> {
    const { avatar } = userDto;

    return this.awsService.signUrl({ fileData: avatar });
  }

  @ResolveReference()
  async resolveReference({ _id: userId }: UserFederationReference) {
    return this.userService.getUserById({ userId: Types.ObjectId(userId) });
  }
}
