import { Field, InputType } from '@nestjs/graphql';
import { Types } from 'mongoose';

@InputType()
export class GetOldUserInput {
  @Field(() => Types.ObjectId, { nullable: false })
  userId: Types.ObjectId;
}
