import { Directive, Field, ObjectType } from '@nestjs/graphql';
import { Types } from 'mongoose';

import { FileData } from '@sawayo/aws';
import { FileDataDto } from '@sawayo/graphql';

@Directive('@key(fields: "_id")')
@ObjectType()
export class UserDto {
  @Field(() => Types.ObjectId, { nullable: false })
  _id: Types.ObjectId;

  @Field(() => FileDataDto, { nullable: true })
  avatar?: FileData;

  @Field(() => String, { nullable: false })
  email: string;

  @Field(() => String, { nullable: true })
  firstName?: string;

  @Field(() => String, { nullable: true })
  lastName?: string;
}
