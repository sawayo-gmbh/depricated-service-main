import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Repository } from '@sawayo/mongo';

import { UserDocument, UserEntity } from '../entities';

import { GetUserByIdParameters } from './user-repository.interface';

@Injectable()
export class UserRepository extends Repository<UserEntity, UserDocument> {
  constructor(@InjectModel(UserEntity.name) private userModel: Model<UserDocument>) {
    super(userModel, { baseClass: UserEntity });
  }

  async getUserById({ userId }: GetUserByIdParameters): Promise<UserEntity> {
    return this.findOne({ _id: userId });
  }
}
