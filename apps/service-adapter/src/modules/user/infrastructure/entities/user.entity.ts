import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Transform } from 'class-transformer';
import { Document, Types } from 'mongoose';

import { FileData } from '@sawayo/aws';

export type UserDocument = UserEntity & Document;

@Schema({ collection: 'users', timestamps: true, versionKey: false })
export class UserEntity {
  @Transform(({ obj }) => obj._id, { toClassOnly: true })
  readonly _id!: Types.ObjectId;

  @Prop({ type: Object })
  avatar?: FileData;

  @Prop()
  employeeId?: number;

  @Prop({
    lowercase: true,
    required: true,
    trim: true,
    unique: true,
  })
  email: string;

  @Prop()
  firstName?: string;

  @Prop()
  lastName?: string;

  @Prop()
  timeTrackingStartDate?: Date;
}

export const UserSchema = SchemaFactory.createForClass(UserEntity);
