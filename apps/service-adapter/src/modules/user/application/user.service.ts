import { Injectable } from '@nestjs/common';

import { UserDomain } from '../domain';

import { GetUserByIdParameters } from './user-service.interface';

@Injectable()
export class UserService {
  constructor(private readonly userDomain: UserDomain) {}

  async getUserById({ userId }: GetUserByIdParameters) {
    return this.userDomain.getUserById({ userId });
  }
}
