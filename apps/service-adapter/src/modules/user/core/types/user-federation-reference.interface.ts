export interface UserFederationReference {
  __typename: string;
  _id: string;
}
