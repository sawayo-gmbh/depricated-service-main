import { Injectable } from '@nestjs/common';

import { UserEntity, UserRepository } from '../infrastructure';

import { GetUserByIdParameters } from './user-domain.interface';

@Injectable()
export class UserDomain {
  constructor(private readonly userRepository: UserRepository) {}

  async getUserById({ userId }: GetUserByIdParameters): Promise<UserEntity> {
    return this.userRepository.getUserById({ userId });
  }
}
