import { Types } from 'mongoose';

export interface GetUserByIdParameters {
  userId: Types.ObjectId;
}
