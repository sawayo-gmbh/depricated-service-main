import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { UserService } from './application';
import { UserDomain } from './domain';
import { UserEntity, UserRepository, UserSchema } from './infrastructure';
import { UserHandler, UserResolver } from './presentation';

@Module({
  controllers: [],
  exports: [UserDomain],
  imports: [MongooseModule.forFeature([{ name: UserEntity.name, schema: UserSchema }])],
  providers: [UserDomain, UserHandler, UserRepository, UserResolver, UserService],
})
export class UserModule {}
