import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';

import { RabbitMQHealthIndicator } from '@sawayo/health';

import { HealthController } from './presentation';

@Module({
  controllers: [HealthController],
  imports: [TerminusModule],
  providers: [RabbitMQHealthIndicator],
})
export class HealthModule {}
