import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';
import { Module, Global } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { Config, RabbitMQConfig } from '../../config';

@Global()
@Module({
  controllers: [],
  exports: [RabbitMQModule],
  imports: [
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService<Config>) => {
        const { uri } = configService.get<RabbitMQConfig>('rabbitmq');

        return {
          uri,
        };
      },
    }),
  ],
  providers: [],
})
export class AmqpModule {}
