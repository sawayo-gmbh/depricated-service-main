import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Parent, Query, ResolveField, Resolver, ResolveReference } from '@nestjs/graphql';
import { Types } from 'mongoose';

import { CurrentUser, CurrentUserArg, GqlAuthGuard, Role, Roles } from '@sawayo/auth';
import { ApprovalType } from '@sawayo/event-bus';

import { DriverLicenseCheckDto } from '../../approval-reference';
import { ApprovalService } from '../application';
import { ApprovalFederationReference } from '../core/types';

import { ApprovalDto, UserDto } from './dto';
import { ApprovalInput, GetPendingApprovalsInput } from './input';
import { GetPendingApprovalsResult } from './result';

@Resolver(() => ApprovalDto)
@UseGuards(GqlAuthGuard)
export class ApprovalResolver {
  constructor(private readonly approvalService: ApprovalService) {}

  @Roles(Role.companyOwner)
  @Mutation(() => ApprovalDto, { name: 'approve' })
  async approve(@Args('data') { id }: ApprovalInput): Promise<ApprovalDto> {
    return this.approvalService.approve({ entityId: id });
  }

  @Roles(Role.companyOwner)
  @Mutation(() => ApprovalDto, { name: 'reject' })
  async reject(
    @CurrentUserArg() currentUser: CurrentUser,
    @Args('data') { comment, id }: ApprovalInput,
  ): Promise<ApprovalDto> {
    const { userId } = currentUser;

    return this.approvalService.reject({
      comment,
      entityId: id,
      userId,
    });
  }

  @Query(() => GetPendingApprovalsResult, { name: 'pendingApprovals' })
  async getPendingApprovals(
    @CurrentUserArg() currentUser: CurrentUser,
    @Args('input') input: GetPendingApprovalsInput,
  ): Promise<GetPendingApprovalsResult> {
    const { employeeId, type } = input;

    const data = await this.approvalService.getPendingApprovals({
      currentUser,
      employeeId,
      type,
    });

    return { data };
  }

  @ResolveField(() => DriverLicenseCheckDto, { name: 'driverLicenseCheck', nullable: true })
  async getDriverLicenseCheckReference(@Parent() approval: ApprovalDto) {
    const { entityId, type } = approval;

    if (type !== ApprovalType.driverLicenseCheck) {
      return null;
    }

    return { __typename: 'DriverLicenseCheckDto', _id: entityId };
  }

  @ResolveField(() => UserDto, { name: 'employee', nullable: true })
  async getEmployeeReference(@Parent() approval: ApprovalDto) {
    const { userId } = approval;

    return { __typename: 'UserDto', _id: userId };
  }

  @ResolveReference()
  async resolveReference({ entityId }: ApprovalFederationReference): Promise<ApprovalDto> {
    return this.approvalService.getApprovalByEntityId({ entityId: Types.ObjectId(entityId) });
  }
}
