import { InputType, Field } from '@nestjs/graphql';
import { IsOptional } from 'class-validator';
import { Types } from 'mongoose';

@InputType()
export class ApprovalInput {
  @Field(() => String, { nullable: true })
  @IsOptional()
  comment?: string;

  @Field(() => Types.ObjectId, { nullable: false })
  id: Types.ObjectId;
}
