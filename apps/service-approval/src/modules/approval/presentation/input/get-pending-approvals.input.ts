import { InputType, Field } from '@nestjs/graphql';
import { IsEnum, IsOptional } from 'class-validator';
import { Types } from 'mongoose';

import { ApprovalType } from '@sawayo/event-bus';

@InputType()
export class GetPendingApprovalsInput {
  @Field(() => Types.ObjectId, { nullable: true })
  employeeId?: Types.ObjectId;

  @IsOptional()
  @IsEnum(ApprovalType)
  @Field(() => ApprovalType, { nullable: true })
  type?: ApprovalType;
}
