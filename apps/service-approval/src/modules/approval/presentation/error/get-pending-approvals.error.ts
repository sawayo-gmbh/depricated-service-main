import { ObjectType } from '@nestjs/graphql';

import { BaseError } from '@sawayo/exceptions';

@ObjectType()
export class GetPendingApprovalsError extends BaseError {}
