import { MessageHandlerErrorBehavior, RabbitRPC } from '@golevelup/nestjs-rabbitmq';
import { Injectable, Logger } from '@nestjs/common';
import { ConsumeMessage } from 'amqplib';
import { Types } from 'mongoose';

import { Exchange, Queue, RoutingKey } from '@sawayo/event-bus';
import { GetApprovalByEntityIdRequest, GetApprovalByEntityIdResponse } from '@sawayo/event-bus/services/approval';

import { ApprovalService } from '../application';

@Injectable()
export class ApprovalHandler {
  private readonly logger: Logger = new Logger(ApprovalHandler.name);

  constructor(private readonly approvalService: ApprovalService) {}

  private async handleGetApprovalByEntityId({
    entityId,
  }: GetApprovalByEntityIdRequest): Promise<GetApprovalByEntityIdResponse> {
    const approval: any = await this.approvalService.getApprovalByEntityId({
      entityId: Types.ObjectId.createFromHexString(entityId),
    });

    return { approval };
  }

  @RabbitRPC({
    exchange: Exchange.direct,
    routingKey: [RoutingKey.approvalByEntityId],
    queue: Queue.approvals,
    errorBehavior: MessageHandlerErrorBehavior.NACK,
  })
  async handleApprovalsRpc(request: any, amqpMessage: ConsumeMessage): Promise<GetApprovalByEntityIdResponse> {
    const { fields } = amqpMessage;
    const { routingKey } = fields;

    if (routingKey === RoutingKey.approvalByEntityId) {
      return this.handleGetApprovalByEntityId(request);
    }

    this.logger.warn(`Unknown routing key: ${routingKey}`);

    return null;
  }
}
