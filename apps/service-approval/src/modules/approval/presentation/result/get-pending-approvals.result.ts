import { Field, ObjectType } from '@nestjs/graphql';

import { ApprovalDto } from '../dto';
import { GetPendingApprovalsError } from '../error';

@ObjectType()
export class GetPendingApprovalsResult {
  @Field(() => [ApprovalDto], { nullable: true })
  data?: ApprovalDto[];

  @Field(() => GetPendingApprovalsError, { nullable: true })
  error?: GetPendingApprovalsError;
}
