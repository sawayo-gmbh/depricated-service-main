import { Field, ObjectType } from '@nestjs/graphql';
import { Types } from 'mongoose';

@ObjectType()
export class CommentDto {
  @Field(() => Types.ObjectId, { nullable: false })
  addedBy: Types.ObjectId;

  @Field(() => String, { nullable: true })
  comment?: string;

  @Field(() => Date, { nullable: true })
  timeStamp?: Date;
}
