import { Directive, Field, ObjectType } from '@nestjs/graphql';
import { Types } from 'mongoose';

import { ApprovalType } from '@sawayo/event-bus/services/approval';
import { ApprovalStatus } from 'apps/service-approval/src/core/types';

import { DriverLicenseCheckDto } from '../../../approval-reference/presentation/dto';

import { CommentDto } from './comment.dto';
import { UserDto } from './user.dto';

@ObjectType()
@Directive('@key(fields: "entityId")')
export class ApprovalDto {
  @Field(() => Types.ObjectId, { nullable: false })
  _id!: Types.ObjectId;

  @Field(() => [CommentDto], { nullable: false })
  comments: CommentDto[];

  @Field(() => Types.ObjectId, { nullable: false })
  companyId: Types.ObjectId;

  @Field(() => Date, { nullable: false })
  createdAt: Date;

  @Directive('@external')
  @Field(() => DriverLicenseCheckDto, { nullable: true })
  driverLicenseCheck?: DriverLicenseCheckDto;

  @Directive('@external')
  @Field(() => UserDto, { nullable: true })
  employee?: UserDto;

  @Field(() => Types.ObjectId, { nullable: false })
  entityId: Types.ObjectId;

  @Field(() => ApprovalStatus, { nullable: true })
  status?: ApprovalStatus;

  @Field(() => ApprovalType, { nullable: true })
  type?: ApprovalType;

  @Field(() => Date, { nullable: false })
  updatedAt: Date;

  @Field(() => Types.ObjectId, { nullable: false })
  userId: Types.ObjectId;
}
