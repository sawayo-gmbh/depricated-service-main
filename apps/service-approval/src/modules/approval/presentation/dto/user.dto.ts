import { Directive, Field, ObjectType } from '@nestjs/graphql';
import { Types } from 'mongoose';

// NOTE Uses temporary dto for reference

@Directive('@extends')
@Directive('@key(fields: "_id")')
@ObjectType()
export class UserDto {
  @Directive('@external')
  @Field(() => Types.ObjectId, { nullable: false })
  _id: Types.ObjectId;
}
