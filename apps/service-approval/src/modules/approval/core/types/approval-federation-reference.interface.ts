export interface ApprovalFederationReference {
  __typename: string;
  entityId: string;
}
