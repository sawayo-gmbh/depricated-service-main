import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Transform } from 'class-transformer';
import { Document, Types } from 'mongoose';

import { ApprovalType } from '@sawayo/event-bus/services/approval';
import { ApprovalStatus } from 'apps/service-approval/src/core/types';

import { ApprovalDto, CommentDto } from '../../presentation/dto';

export type ApprovalDocument = ApprovalEntity & Document;

@Schema({ collection: 'approvals', timestamps: true, versionKey: false })
export class ApprovalEntity extends ApprovalDto {
  @Transform(({ obj }) => obj._id, { toClassOnly: true })
  readonly _id!: Types.ObjectId;

  @Prop([CommentDto])
  comments: CommentDto[];

  @Prop({ type: Types.ObjectId })
  companyId: Types.ObjectId;

  createdAt: Date;

  @Prop({ type: Types.ObjectId })
  entityId: Types.ObjectId;

  @Prop({ enum: ApprovalStatus, type: String })
  status?: ApprovalStatus;

  @Prop({ enum: ApprovalType, type: String })
  type?: ApprovalType;

  updatedAt: Date;

  @Prop({ type: Types.ObjectId })
  userId: Types.ObjectId;

  constructor(init?: Partial<ApprovalEntity>) {
    super();
    Object.assign(this, init);
  }
}

export const ApprovalSchema = SchemaFactory.createForClass(ApprovalEntity);
