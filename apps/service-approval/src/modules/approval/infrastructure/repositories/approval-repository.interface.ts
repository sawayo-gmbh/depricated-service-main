import { Types } from 'mongoose';

import { ApprovalType } from '@sawayo/event-bus/services/approval';
import { ApprovalStatus, Comment } from 'apps/service-approval/src/core/types';

export interface ApproveParameters {
  entityId: Types.ObjectId;
}

export interface CreateApprovalParameters {
  companyId: Types.ObjectId;
  entityId: Types.ObjectId;
  status: ApprovalStatus;
  type: ApprovalType;
  userId: Types.ObjectId;
}

export interface DeleteApprovalByEntityIdParameters {
  companyId: Types.ObjectId;
  entityId: Types.ObjectId;
}

export interface GetApprovalByEntityIdParameters {
  entityId: Types.ObjectId;
}

export interface GetApprovalByIdParameters {
  approvalId: Types.ObjectId;
}

export interface GetPendingApprovalsParameters {
  companyId: Types.ObjectId;
  employeeId: Types.ObjectId;
  type?: ApprovalType;
}

export interface RejectParameters {
  comment: Comment;
  entityId: Types.ObjectId;
}
