import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { removeUndefined, Repository } from '@sawayo/mongo';
import { ApprovalStatus } from 'apps/service-approval/src/core/types';

import { ApprovalEntity, ApprovalDocument } from '../entities/approval.entity';

import {
  ApproveParameters,
  CreateApprovalParameters,
  DeleteApprovalByEntityIdParameters,
  GetApprovalByEntityIdParameters,
  GetApprovalByIdParameters,
  GetPendingApprovalsParameters,
  RejectParameters,
} from './approval-repository.interface';

@Injectable()
export class ApprovalRepository extends Repository<ApprovalEntity, ApprovalDocument> {
  constructor(@InjectModel(ApprovalEntity.name) private approvalEntity: Model<ApprovalDocument>) {
    super(approvalEntity, { baseClass: ApprovalEntity });
  }

  async approve({ entityId }: ApproveParameters): Promise<ApprovalEntity> {
    await this.update({ entityId }, { status: ApprovalStatus.approved });

    return this.getApprovalByEntityId({ entityId });
  }

  async deleteApprovalByEntityId({ companyId, entityId }: DeleteApprovalByEntityIdParameters): Promise<void> {
    await this.delete({
      companyId,
      entityId,
    });
  }

  async createApproval({
    companyId,
    entityId,
    status,
    type,
    userId,
  }: CreateApprovalParameters): Promise<ApprovalEntity> {
    return this.create({
      comments: [],
      companyId,
      entityId,
      status,
      type,
      userId,
    });
  }

  async getApprovalByEntityId({ entityId }: GetApprovalByEntityIdParameters): Promise<ApprovalEntity> {
    return this.findOne({ entityId });
  }

  async getApprovalById({ approvalId }: GetApprovalByIdParameters): Promise<ApprovalEntity> {
    return this.findOne({ _id: approvalId });
  }

  async getPendingApprovals({ companyId, employeeId, type }: GetPendingApprovalsParameters): Promise<ApprovalEntity[]> {
    return this.approvalEntity
      .find(
        removeUndefined({
          companyId,
          status: ApprovalStatus.open,
          type,
          userId: employeeId,
        }),
      )
      .sort({ createdAt: -1 });
  }

  async reject({ comment, entityId }: RejectParameters): Promise<ApprovalEntity> {
    await this.update(
      { entityId },
      {
        $push: {
          comments: comment,
        },
        status: ApprovalStatus.rejected,
      },
    );

    return this.getApprovalByEntityId({ entityId });
  }
}
