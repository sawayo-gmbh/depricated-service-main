import { Types } from 'mongoose';

import { CurrentUser } from '@sawayo/auth';
import { ApprovalType } from '@sawayo/event-bus';

export interface ApproveParameters {
  entityId: Types.ObjectId;
}

export interface GetApprovalByEntityIdParameters {
  entityId: Types.ObjectId;
}

export interface GetPendingApprovalsParameters {
  currentUser: CurrentUser;
  employeeId: Types.ObjectId;
  type?: ApprovalType;
}

export interface RejectParameters {
  entityId: Types.ObjectId;
  comment: string;
  userId: Types.ObjectId;
}
