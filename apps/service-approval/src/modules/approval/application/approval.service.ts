import { Injectable } from '@nestjs/common';

import { Role } from '@sawayo/auth';
import { ForbiddenException } from '@sawayo/exceptions';

import { ApprovalDomain } from '../domain';
import { ApprovalDto } from '../presentation';

import {
  ApproveParameters,
  GetApprovalByEntityIdParameters,
  GetPendingApprovalsParameters,
  RejectParameters,
} from './approval-service.interface';

@Injectable()
export class ApprovalService {
  constructor(private readonly approvalDomain: ApprovalDomain) {}

  async approve({ entityId }: ApproveParameters): Promise<ApprovalDto> {
    return this.approvalDomain.approve({ entityId });
  }

  async getApprovalByEntityId({ entityId }: GetApprovalByEntityIdParameters): Promise<ApprovalDto> {
    return this.approvalDomain.getApprovalByEntityId({ entityId });
  }

  async getPendingApprovals({ currentUser, employeeId, type }: GetPendingApprovalsParameters): Promise<ApprovalDto[]> {
    const { role, userId } = currentUser;

    if (employeeId && !employeeId.equals(userId) && role !== Role.companyOwner) {
      throw new ForbiddenException();
    }

    return this.approvalDomain.getPendingApprovals({
      currentUser,
      employeeId,
      type,
    });
  }

  async reject({ entityId, comment, userId }: RejectParameters): Promise<ApprovalDto> {
    return this.approvalDomain.reject({
      comment,
      entityId,
      userId,
    });
  }
}
