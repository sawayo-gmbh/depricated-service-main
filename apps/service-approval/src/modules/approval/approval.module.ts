import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { EventBusModule } from '../event-bus';

import { ApprovalService } from './application';
import { ApprovalDomain } from './domain';
import { ApprovalEntity, ApprovalRepository, ApprovalSchema } from './infrastructure';
import { ApprovalHandler, ApprovalResolver } from './presentation';

@Module({
  controllers: [],
  exports: [ApprovalDomain],
  imports: [
    MongooseModule.forFeature([{ name: ApprovalEntity.name, schema: ApprovalSchema }]),
    forwardRef(() => EventBusModule),
  ],
  providers: [ApprovalDomain, ApprovalHandler, ApprovalRepository, ApprovalResolver, ApprovalService],
})
export class ApprovalModule {}
