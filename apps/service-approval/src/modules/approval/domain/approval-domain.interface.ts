import { Types } from 'mongoose';

import { CurrentUser } from '@sawayo/auth';
import { ApprovalType } from '@sawayo/event-bus/services/approval';
import { ApprovalStatus } from 'apps/service-approval/src/core/types';

export interface ApproveParameters {
  entityId: Types.ObjectId;
}

export interface CreateApprovalParameters {
  companyId: Types.ObjectId;
  entityId: Types.ObjectId;
  status: ApprovalStatus;
  type: ApprovalType;
  userId: Types.ObjectId;
}

export interface DeleteApprovalByEntityIdParameters {
  currentUser: CurrentUser;
  entityId: Types.ObjectId;
}

export interface GetApprovalByEntityIdParameters {
  entityId: Types.ObjectId;
}

export interface GetPendingApprovalsParameters {
  currentUser: CurrentUser;
  employeeId: Types.ObjectId;
  type?: ApprovalType;
}

export interface RejectParameters {
  comment: string;
  entityId: Types.ObjectId;
  userId: Types.ObjectId;
}
