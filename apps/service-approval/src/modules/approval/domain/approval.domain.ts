import { Injectable } from '@nestjs/common';

import { ApprovalType } from '@sawayo/event-bus/services/approval';
import { Comment } from 'apps/service-approval/src/core/types';

import {
  DriverLicenseCheckProvider,
  PushNotificationsProvider,
  WebSocketProvider,
} from '../../event-bus/infrastructure';
import { ApprovalEntity, ApprovalRepository } from '../infrastructure';

import {
  ApproveParameters,
  CreateApprovalParameters,
  DeleteApprovalByEntityIdParameters,
  GetApprovalByEntityIdParameters,
  GetPendingApprovalsParameters,
  RejectParameters,
} from './approval-domain.interface';

@Injectable()
export class ApprovalDomain {
  constructor(
    private readonly approvalRepository: ApprovalRepository,
    private readonly driverLicenseCheckProvider: DriverLicenseCheckProvider,
    private readonly pushNotificationsProvider: PushNotificationsProvider,
    private readonly webSocketProvider: WebSocketProvider,
  ) {}

  async approve({ entityId }: ApproveParameters): Promise<ApprovalEntity> {
    const approval = await this.approvalRepository.approve({ entityId });

    const { type, userId } = approval;

    if (type === ApprovalType.driverLicenseCheck) {
      await this.driverLicenseCheckProvider.sendDriverLicenseCheckApproved({ driverLicenseCheckId: entityId });
    }

    await this.pushNotificationsProvider.sendApprovedPushNotification({
      type,
      userId,
    });

    await this.webSocketProvider.sendApprovedWebSocketMessage({
      approval,
      userId,
    });

    return approval;
  }

  async createApproval({
    companyId,
    entityId,
    status,
    type,
    userId,
  }: CreateApprovalParameters): Promise<ApprovalEntity> {
    return this.approvalRepository.createApproval({
      companyId,
      entityId,
      status,
      type,
      userId,
    });
  }

  async deleteApprovalByEntityId({ currentUser, entityId }: DeleteApprovalByEntityIdParameters): Promise<void> {
    const { companyId } = currentUser;

    return this.approvalRepository.deleteApprovalByEntityId({
      companyId,
      entityId,
    });
  }

  async getApprovalByEntityId({ entityId }: GetApprovalByEntityIdParameters): Promise<ApprovalEntity> {
    return this.approvalRepository.getApprovalByEntityId({ entityId });
  }

  async getPendingApprovals({
    currentUser,
    employeeId,
    type,
  }: GetPendingApprovalsParameters): Promise<ApprovalEntity[]> {
    const { companyId } = currentUser;

    return this.approvalRepository.getPendingApprovals({
      companyId,
      employeeId,
      type,
    });
  }

  async reject({ comment, entityId, userId }: RejectParameters): Promise<ApprovalEntity> {
    const commentEntity: Comment = {
      addedBy: userId,
      comment,
      timeStamp: new Date(),
    };

    const approval = await this.approvalRepository.reject({
      entityId,
      comment: commentEntity,
    });

    const { type, userId: employeeId } = approval;

    if (type === ApprovalType.driverLicenseCheck) {
      await this.driverLicenseCheckProvider.sendDriverLicenseCheckRejected({
        userId: employeeId,
        driverLicenseCheckId: entityId,
      });
    }

    await this.pushNotificationsProvider.sendRejectedPushNotification({
      comment,
      type,
      userId: employeeId,
    });
    await this.webSocketProvider.sendRejectedWebSocketMessage({
      approval,
      userId: employeeId,
    });

    return approval;
  }
}
