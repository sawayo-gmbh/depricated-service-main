import { Directive, Field, ObjectType } from '@nestjs/graphql';
import { Types } from 'mongoose';

import { ApprovalDto } from '../../../approval';

@Directive('@extends')
@Directive('@key(fields: "_id")')
@ObjectType()
export class DriverLicenseCheckDto {
  @Directive('@external')
  @Field(() => Types.ObjectId, { nullable: false })
  _id: string;

  @Field(() => ApprovalDto, { nullable: true })
  approval?: ApprovalDto;
}
