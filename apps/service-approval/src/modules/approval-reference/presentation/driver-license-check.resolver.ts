import { Injectable } from '@nestjs/common';
import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { Types } from 'mongoose';

import { ApprovalDto } from '../../approval/presentation/dto';
import { ApprovalReferenceService } from '../application';

import { DriverLicenseCheckDto } from './dto';

@Injectable()
@Resolver(() => DriverLicenseCheckDto)
export class DriverLicenseCheckResolver {
  constructor(private readonly approvalReferenceService: ApprovalReferenceService) {}

  @ResolveField(() => ApprovalDto, { name: 'approval', nullable: true })
  async getApproval(@Parent() driverLicenseCheck: DriverLicenseCheckDto): Promise<ApprovalDto> {
    const { _id: entityId } = driverLicenseCheck;

    return this.approvalReferenceService.getApprovalByEntityId({
      entityId: Types.ObjectId(entityId),
    });
  }
}
