import { forwardRef, Module } from '@nestjs/common';

import { ApprovalModule } from '../approval';

import { ApprovalReferenceService } from './application';
import { DriverLicenseCheckResolver } from './presentation';

@Module({
  imports: [forwardRef(() => ApprovalModule)],
  providers: [ApprovalReferenceService, DriverLicenseCheckResolver],
})
export class ApprovalReferenceModule {}
