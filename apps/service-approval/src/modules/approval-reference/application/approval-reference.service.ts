import { Injectable } from '@nestjs/common';

import { ApprovalDomain } from '../../approval/domain';
import { ApprovalDto } from '../../approval/presentation/dto';

import { GetApprovalByEntityIdParameters } from './approval-reference-services.interface';

@Injectable()
export class ApprovalReferenceService {
  constructor(private readonly approvalDomain: ApprovalDomain) {}

  async getApprovalByEntityId({ entityId }: GetApprovalByEntityIdParameters): Promise<ApprovalDto> {
    return this.approvalDomain.getApprovalByEntityId({ entityId });
  }
}
