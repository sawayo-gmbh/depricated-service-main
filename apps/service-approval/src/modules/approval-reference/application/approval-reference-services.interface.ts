import { Types } from 'mongoose';

export interface GetApprovalByEntityIdParameters {
  entityId: Types.ObjectId;
}
