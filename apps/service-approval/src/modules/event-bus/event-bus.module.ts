import { forwardRef, Module } from '@nestjs/common';

import { ApprovalModule } from '../approval';

import { EventBusService } from './application';
import { DriverLicenseCheckProvider, PushNotificationsProvider, WebSocketProvider } from './infrastructure';
import { EventBusHandler } from './presentation';

@Module({
  controllers: [],
  exports: [DriverLicenseCheckProvider, PushNotificationsProvider, WebSocketProvider],
  imports: [forwardRef(() => ApprovalModule)],
  providers: [
    DriverLicenseCheckProvider,
    EventBusHandler,
    EventBusService,
    PushNotificationsProvider,
    WebSocketProvider,
  ],
})
export class EventBusModule {}
