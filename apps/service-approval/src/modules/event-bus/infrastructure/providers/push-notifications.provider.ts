import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';

import { Exchange, RoutingKey } from '@sawayo/event-bus';
import { ApprovalType } from '@sawayo/event-bus/services/approval';
import { PushNotificationEvent } from '@sawayo/event-bus/services/old';
import { ApprovalStatus } from 'apps/service-approval/src/core/types';

import {
  SendApprovedPushNotificationParameters,
  SendPushNotificationParameters,
  SendRejectedPushNotificationParameters,
} from './push-notifications-provider.interface';

@Injectable()
export class PushNotificationsProvider {
  private readonly bodyByApprovalType: Record<ApprovalType, Partial<Record<ApprovalStatus, string>>> = {
    [ApprovalType.absence]: {
      [ApprovalStatus.approved]: '',
      [ApprovalStatus.rejected]: '',
    },
    [ApprovalType.driverLicenseCheck]: {
      [ApprovalStatus.approved]: 'Dein Führerschein wurde akzeptiert',
      [ApprovalStatus.rejected]: 'Dein Führerschein wurde abgelehnt',
    },
  };

  private readonly titleByApprovalType: Record<ApprovalType, string> = {
    [ApprovalType.absence]: '',
    [ApprovalType.driverLicenseCheck]: 'Führerscheinkontrolle',
  };

  constructor(private readonly amqpConnection: AmqpConnection) {}

  async sendApprovedPushNotification({ type, userId }: SendApprovedPushNotificationParameters): Promise<void> {
    const body = this.bodyByApprovalType[type][ApprovalStatus.approved];
    const title = this.titleByApprovalType[type];

    await this.sendPushNotification({
      notification: {
        body,
        title,
      },
      userId,
    });
  }

  async sendPushNotification({ notification, userId }: SendPushNotificationParameters): Promise<void> {
    const payload: PushNotificationEvent = {
      notification,
      userId,
    };

    await this.amqpConnection.publish(Exchange.direct, RoutingKey.oldPushNotification, payload);
  }

  async sendRejectedPushNotification({ comment, type, userId }: SendRejectedPushNotificationParameters): Promise<void> {
    const body = this.bodyByApprovalType[type][ApprovalStatus.rejected];
    const title = this.titleByApprovalType[type];

    await this.sendPushNotification({
      notification: {
        body: `${body}: ${comment}`,
        title,
      },
      userId,
    });
  }
}
