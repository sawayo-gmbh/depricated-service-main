import { Types } from 'mongoose';

import { WebSocketMessageType, WebSocketNotificationType } from '@sawayo/event-bus/services/old';

import { ApprovalEntity } from '../../../approval/infrastructure';

export interface SendApprovedWebSocketMessageParameters {
  approval: ApprovalEntity;
  userId: Types.ObjectId;
}

export interface SendRejectedWebSocketMessageParameters {
  approval: ApprovalEntity;
  userId: Types.ObjectId;
}

export interface SendWebSocketMessageParameters {
  message: unknown;
  type: WebSocketMessageType;
  userId: Types.ObjectId;
}

export interface SendWebSocketNotificationParameters {
  message: string;
  payload: unknown;
  subject: string;
  type: WebSocketNotificationType;
  userId: Types.ObjectId;
}
