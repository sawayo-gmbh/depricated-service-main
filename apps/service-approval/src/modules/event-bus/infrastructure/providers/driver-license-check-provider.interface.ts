import { Types } from 'mongoose';

export interface SendDriverLicenseCheckApprovedParameters {
  driverLicenseCheckId: Types.ObjectId;
}

export interface SendDriverLicenseCheckRejectedParameters {
  userId: Types.ObjectId;
  driverLicenseCheckId: Types.ObjectId;
}
