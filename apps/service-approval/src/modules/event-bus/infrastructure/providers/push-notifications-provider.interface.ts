import { Types } from 'mongoose';

import { ApprovalType } from '@sawayo/event-bus/services/approval';
import { Notification } from '@sawayo/event-bus/services/old';

export interface SendApprovedPushNotificationParameters {
  type: ApprovalType;
  userId: Types.ObjectId;
}

export interface SendPushNotificationParameters {
  notification: Notification;
  userId: Types.ObjectId;
}

export interface SendRejectedPushNotificationParameters {
  comment: string;
  type: ApprovalType;
  userId: Types.ObjectId;
}
