import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';

import { Exchange, RoutingKey } from '@sawayo/event-bus';
import {
  SendWebSocketMessageEvent,
  SendWebSocketMessagePublisher,
  WebSocketMessageType,
  WebSocketNotificationEvent,
} from '@sawayo/event-bus/services/old';

import {
  SendApprovedWebSocketMessageParameters,
  SendRejectedWebSocketMessageParameters,
  SendWebSocketMessageParameters,
  SendWebSocketNotificationParameters,
} from './web-socket-provider.interface';

@Injectable()
export class WebSocketProvider {
  constructor(private readonly amqpConnection: AmqpConnection) {}

  async sendApprovedWebSocketMessage({ approval, userId }: SendApprovedWebSocketMessageParameters): Promise<void> {
    await this.sendWebSocketMessage({
      message: approval,
      type: WebSocketMessageType.approved,
      userId,
    });
  }

  async sendRejectedWebSocketMessage({ approval, userId }: SendRejectedWebSocketMessageParameters): Promise<void> {
    await this.sendWebSocketMessage({
      message: approval,
      type: WebSocketMessageType.rejected,
      userId,
    });
  }

  async sendWebSocketMessage({ message, type, userId }: SendWebSocketMessageParameters): Promise<void> {
    const publisher = new SendWebSocketMessagePublisher(this.amqpConnection);

    const event: SendWebSocketMessageEvent = {
      message,
      type,
      userId: userId.toHexString(),
    };

    await publisher.publish(event);
  }

  async sendWebSocketNotification({
    message,
    payload,
    subject,
    type,
    userId,
  }: SendWebSocketNotificationParameters): Promise<void> {
    const event: WebSocketNotificationEvent = {
      message,
      payload,
      subject,
      type,
      userId,
    };

    await this.amqpConnection.publish(Exchange.direct, RoutingKey.oldWebSocketNotification, event);
  }
}
