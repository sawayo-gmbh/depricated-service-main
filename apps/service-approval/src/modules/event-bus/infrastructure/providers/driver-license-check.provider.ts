import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';

import { Exchange, RoutingKey } from '@sawayo/event-bus';
import {
  DriverLicenseCheckApprovedEvent,
  DriverLicenseCheckRejectedEvent,
} from '@sawayo/event-bus/services/driver-license-check';

import {
  SendDriverLicenseCheckApprovedParameters,
  SendDriverLicenseCheckRejectedParameters,
} from './driver-license-check-provider.interface';

@Injectable()
export class DriverLicenseCheckProvider {
  constructor(private readonly amqpConnection: AmqpConnection) {}

  async sendDriverLicenseCheckApproved({
    driverLicenseCheckId,
  }: SendDriverLicenseCheckApprovedParameters): Promise<void> {
    const payload: DriverLicenseCheckApprovedEvent = {
      driverLicenseCheckId: driverLicenseCheckId.toHexString(),
    };

    await this.amqpConnection.publish(Exchange.direct, RoutingKey.driverLicenseCheckApproved, payload);
  }

  async sendDriverLicenseCheckRejected({
    userId,
    driverLicenseCheckId,
  }: SendDriverLicenseCheckRejectedParameters): Promise<void> {
    const payload: DriverLicenseCheckRejectedEvent = {
      userId: userId.toHexString(),
      driverLicenseCheckId: driverLicenseCheckId.toHexString(),
    };

    await this.amqpConnection.publish(Exchange.direct, RoutingKey.driverLicenseCheckRejected, payload);
  }
}
