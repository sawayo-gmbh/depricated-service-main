import { forwardRef, Inject, Injectable } from '@nestjs/common';

import { ApprovalStatus } from 'apps/service-approval/src/core/types';

import { ApprovalDomain } from '../../approval';

import { CreateApprovalParameters, DeleteApprovalByEntityIdParameters } from './event-bus-service.interface';

@Injectable()
export class EventBusService {
  constructor(
    @Inject(forwardRef(() => ApprovalDomain))
    private readonly approvalDomain: ApprovalDomain,
  ) {}

  async createApproval({ companyId, entityId, type, userId }: CreateApprovalParameters): Promise<void> {
    await this.approvalDomain.createApproval({
      companyId,
      entityId,
      status: ApprovalStatus.open,
      type,
      userId,
    });
  }

  async deleteApprovalByEntityId({ currentUser, entityId }: DeleteApprovalByEntityIdParameters): Promise<void> {
    await this.approvalDomain.deleteApprovalByEntityId({
      currentUser,
      entityId,
    });
  }
}
