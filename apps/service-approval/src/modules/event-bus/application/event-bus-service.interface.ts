import { Types } from 'mongoose';

import { CurrentUser } from '@sawayo/auth';
import { ApprovalType } from '@sawayo/event-bus/services/approval';

export interface CreateApprovalParameters {
  companyId: Types.ObjectId;
  entityId: Types.ObjectId;
  type: ApprovalType;
  userId: Types.ObjectId;
}

export interface DeleteApprovalByEntityIdParameters {
  currentUser: CurrentUser;
  entityId: Types.ObjectId;
}
