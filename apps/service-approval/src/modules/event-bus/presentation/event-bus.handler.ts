import { MessageHandlerErrorBehavior, RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { Injectable, Logger } from '@nestjs/common';
import { ConsumeMessage } from 'amqplib';
import { Types } from 'mongoose';

import { Exchange, Queue, RoutingKey, toCurrentUser } from '@sawayo/event-bus';
import { CreateApprovalEvent, DeleteApprovalByEntityIdEvent } from '@sawayo/event-bus/services/approval';

import { EventBusService } from '../application';

@Injectable()
export class EventBusHandler {
  private readonly logger: Logger = new Logger(EventBusHandler.name);

  constructor(private readonly eventBusService: EventBusService) {}

  private async handleApprovalCreateApproval({
    companyId,
    entityId,
    type,
    userId,
  }: CreateApprovalEvent): Promise<void> {
    await this.eventBusService.createApproval({
      companyId: Types.ObjectId(companyId),
      entityId: Types.ObjectId(entityId),
      type,
      userId: Types.ObjectId(userId),
    });
  }

  private async handleDeleteApprovalByEntityId({
    currentUser,
    entityId,
  }: DeleteApprovalByEntityIdEvent): Promise<void> {
    await this.eventBusService.deleteApprovalByEntityId({
      currentUser: toCurrentUser(currentUser),
      entityId: Types.ObjectId.createFromHexString(entityId),
    });
  }

  @RabbitSubscribe({
    errorBehavior: MessageHandlerErrorBehavior.NACK,
    exchange: Exchange.direct,
    queue: Queue.approvalApprovals,
    routingKey: [RoutingKey.approvalCreateApproval, RoutingKey.deleteApprovalByEntityId],
  })
  async handleMessage(event: any, amqpMessage: ConsumeMessage): Promise<void> {
    const { fields } = amqpMessage;
    const { routingKey } = fields;

    if (routingKey === RoutingKey.approvalCreateApproval) {
      return this.handleApprovalCreateApproval(event);
    }

    if (routingKey === RoutingKey.deleteApprovalByEntityId) {
      return this.handleDeleteApprovalByEntityId(event);
    }

    this.logger.warn(`Unknown routing key: ${routingKey}`);

    return null;
  }
}
