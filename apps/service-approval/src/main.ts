import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';

import { exceptionFactory, ExceptionFilter } from '@sawayo/exceptions';

import { ApplicationConfig, Config } from './config';
import { ApprovalServiceModule } from './service-approval.module';

async function bootstrap() {
  const app = await NestFactory.create(ApprovalServiceModule);

  const configService = app.get<ConfigService<Config>>(ConfigService);
  const { port } = configService.get<ApplicationConfig>('application');

  app.useGlobalFilters(new ExceptionFilter());
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory,
      forbidUnknownValues: true,
      transform: true,
    }),
  );

  await app.listen(port);

  const logger = new Logger();

  logger.log(`Application is running on ${await app.getUrl()}`);
}

bootstrap();
