import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GraphQLFederationModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';

import { GqlAuthMiddleware } from '@sawayo/auth';
import { ObjectIdScalar } from '@sawayo/graphql';

import { configuration, GraphQLConfigService, MongooseConfigService, validate } from './config';
import { AmqpModule } from './modules/amqp';
import { ApprovalModule } from './modules/approval';
import { ApprovalReferenceModule } from './modules/approval-reference';
import { EventBusModule } from './modules/event-bus';
import { HealthModule } from './modules/health';

@Module({
  controllers: [],
  exports: [],
  imports: [
    ConfigModule.forRoot({ isGlobal: true, load: configuration, validate }),
    GraphQLFederationModule.forRootAsync({ useClass: GraphQLConfigService }),
    MongooseModule.forRootAsync({ useClass: MongooseConfigService }),
    AmqpModule,
    ApprovalModule,
    ApprovalReferenceModule,
    EventBusModule,
    HealthModule,
  ],
  providers: [ObjectIdScalar],
})
export class ApprovalServiceModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(GqlAuthMiddleware).forRoutes('/graphql');
  }
}
