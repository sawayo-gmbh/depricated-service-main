import { GraphQLConfigSlice } from './graphql-config.interface';

export const graphqlConfiguration = (): GraphQLConfigSlice => {
  return {
    graphql: {
      playground: process.env.SERVICE_APPROVAL_GRAPHQL_PLAYGROUND === 'true',
    },
  };
};
