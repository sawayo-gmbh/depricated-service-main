import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { GqlModuleOptions, GqlOptionsFactory, registerEnumType } from '@nestjs/graphql';
import { ApolloLogPlugin } from 'apollo-log';
import { ObjectIDResolver, ObjectIDTypeDefinition } from 'graphql-scalars';

import { ApprovalType } from '@sawayo/event-bus/services/approval';
import { UserDto } from 'apps/service-approval/src/modules/approval';
import { DriverLicenseCheckDto } from 'apps/service-approval/src/modules/approval-reference';

import { ApprovalStatus } from '../../core/types';
import { Config } from '../config.interface';

import { GraphQLConfig } from './graphql-config.interface';

@Injectable()
export class GraphQLConfigService implements GqlOptionsFactory {
  constructor(private readonly configService: ConfigService<Config>) {}

  createGqlOptions(): GqlModuleOptions {
    const { playground } = this.configService.get<GraphQLConfig>('graphql');

    registerEnumType(ApprovalStatus, {
      name: 'ApprovalStatus',
    });
    registerEnumType(ApprovalType, {
      name: 'ApprovalType',
    });

    return {
      autoSchemaFile: true,
      buildSchemaOptions: {
        orphanedTypes: [DriverLicenseCheckDto, UserDto],
      },
      introspection: playground,
      playground,
      plugins: [
        ApolloLogPlugin({
          events: {
            didEncounterErrors: true,
            didResolveOperation: false,
            executionDidStart: false,
            parsingDidStart: false,
            requestDidStart: false,
            responseForOperation: false,
            validationDidStart: false,
            willSendResponse: false,
          },
          timestamp: true,
        }),
      ],
      resolvers: { ObjectID: ObjectIDResolver },
      sortSchema: true,
      typeDefs: [ObjectIDTypeDefinition],
    };
  }
}
