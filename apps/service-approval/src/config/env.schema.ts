import { IsBoolean, IsNumber, IsString } from 'class-validator';

export class EnvironmentVariables {
  @IsNumber()
  SERVICE_APPROVAL_PORT: number;

  @IsString()
  SERVICE_APPROVAL_MONGO_URI: string;

  @IsBoolean()
  SERVICE_APPROVAL_GRAPHQL_PLAYGROUND: boolean;

  @IsString()
  SERVICE_APPROVAL_RABBITMQ_URI: string;
}
