import { applicationConfiguration } from './application';
import { databaseConfiguration } from './database';
import { graphqlConfiguration } from './graphql';
import { rabbitMQConfiguration } from './rabbitmq';

export const configuration = [
  applicationConfiguration,
  databaseConfiguration,
  graphqlConfiguration,
  rabbitMQConfiguration,
];
