export interface ApplicationConfig {
  port: number;
}

export interface ApplicationConfigSlice {
  application: ApplicationConfig;
}
