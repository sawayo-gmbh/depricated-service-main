import { ApplicationConfigSlice } from './application-config.interface';

export const applicationConfiguration = (): ApplicationConfigSlice => {
  return {
    application: {
      port: parseInt(process.env.SERVICE_APPROVAL_PORT, 10),
    },
  };
};
