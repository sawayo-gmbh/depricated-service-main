import { ApplicationConfigSlice } from './application';
import { DatabaseConfigSlice } from './database';
import { GraphQLConfigSlice } from './graphql';
import { RabbitMQConfigSlice } from './rabbitmq';

export interface Config extends ApplicationConfigSlice, DatabaseConfigSlice, GraphQLConfigSlice, RabbitMQConfigSlice {}
