import { Types } from 'mongoose';

export interface Comment {
  addedBy: Types.ObjectId;
  comment?: string;
  timeStamp?: Date;
}
