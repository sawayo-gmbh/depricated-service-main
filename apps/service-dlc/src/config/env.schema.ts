import { IsBoolean, IsNumber, IsString } from 'class-validator';

export class EnvironmentVariables {
  @IsNumber()
  SERVICE_DLC_PORT: number;

  @IsString()
  SERVICE_DLC_AWS_ACCESS_KEY_ID: string;

  @IsString()
  SERVICE_DLC_AWS_BUCKET: string;

  @IsString()
  SERVICE_DLC_AWS_ENDPOINT: string;

  @IsNumber()
  SERVICE_DLC_AWS_EXPIRES_IN: number;

  @IsString()
  SERVICE_DLC_AWS_KEY_PREFIX: string;

  @IsString()
  SERVICE_DLC_AWS_REGION: string;

  @IsString()
  SERVICE_DLC_AWS_SECRET_ACCESS_KEY: string;

  @IsString()
  SERVICE_DLC_MONGO_URI: string;

  @IsBoolean()
  SERVICE_DLC_GRAPHQL_PLAYGROUND: boolean;

  @IsString()
  SERVICE_DLC_RABBITMQ_URI: string;
}
