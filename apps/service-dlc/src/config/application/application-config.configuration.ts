import { ApplicationConfigSlice } from './application-config.interface';

export const applicationConfiguration = (): ApplicationConfigSlice => {
  return {
    application: {
      port: parseInt(process.env.SERVICE_DLC_PORT, 10),
    },
  };
};
