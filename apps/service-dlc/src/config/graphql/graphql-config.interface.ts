export interface GraphQLConfig {
  playground: boolean;
}

export interface GraphQLConfigSlice {
  graphql: GraphQLConfig;
}
