import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { GqlModuleOptions, GqlOptionsFactory, registerEnumType } from '@nestjs/graphql';
import { ApolloLogPlugin } from 'apollo-log';
import { ObjectIDResolver, ObjectIDTypeDefinition } from 'graphql-scalars';

import { Trigger, Repetition } from '../../modules/driver-license-check-settings/core/types';
import { NotHoldingReason } from '../../modules/driver-license-check/core/types';
import { Config } from '../config.interface';

import { GraphQLConfig } from './graphql-config.interface';

@Injectable()
export class GraphQLConfigService implements GqlOptionsFactory {
  constructor(private readonly configService: ConfigService<Config>) {}

  createGqlOptions(): GqlModuleOptions {
    const { playground } = this.configService.get<GraphQLConfig>('graphql');

    registerEnumType(NotHoldingReason, {
      name: 'NotHoldingReason',
    });
    registerEnumType(Repetition, {
      name: 'Repetition',
    });
    registerEnumType(Trigger, {
      name: 'Trigger',
    });

    return {
      autoSchemaFile: true,
      introspection: playground,
      playground,
      plugins: [
        ApolloLogPlugin({
          events: {
            didEncounterErrors: true,
            didResolveOperation: false,
            executionDidStart: false,
            parsingDidStart: false,
            requestDidStart: false,
            responseForOperation: false,
            validationDidStart: false,
            willSendResponse: false,
          },
          timestamp: true,
        }),
      ],
      resolvers: { ObjectID: ObjectIDResolver },
      sortSchema: true,
      typeDefs: [ObjectIDTypeDefinition],
    };
  }
}
