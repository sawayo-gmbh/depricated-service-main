import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { AwsModuleOptionsFactory, AwsModuleOptions } from '@sawayo/aws';

import { Config } from '../config.interface';

import { AwsConfig } from './aws-config.interface';

@Injectable()
export class AwsConfigService implements AwsModuleOptionsFactory {
  constructor(private readonly configService: ConfigService<Config>) {}

  createAwsOptions(): AwsModuleOptions {
    const { accessKeyId, bucket, endpoint, expiresIn, keyPrefix, region, secretAccessKey } =
      this.configService.get<AwsConfig>('aws');

    return {
      acl: 'private',
      bucket,
      credentials: { accessKeyId, secretAccessKey },
      endpoint,
      expiresIn,
      keyPrefix,
      region,
    };
  }
}
