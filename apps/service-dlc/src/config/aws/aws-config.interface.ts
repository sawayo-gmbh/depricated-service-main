export interface AwsConfig {
  accessKeyId: string;
  bucket: string;
  endpoint: string;
  expiresIn: number;
  keyPrefix: string;
  region: string;
  secretAccessKey: string;
}

export interface AwsConfigSlice {
  aws: AwsConfig;
}
