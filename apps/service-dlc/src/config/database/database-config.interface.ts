export interface DatabaseConfig {
  uri: string;
}

export interface DatabaseConfigSlice {
  database: DatabaseConfig;
}
