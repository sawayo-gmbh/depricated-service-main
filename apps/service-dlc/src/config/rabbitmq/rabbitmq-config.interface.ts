export interface RabbitMQConfig {
  uri: string;
}

export interface RabbitMQConfigSlice {
  rabbitmq: RabbitMQConfig;
}
