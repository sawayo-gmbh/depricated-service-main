import { RabbitMQConfigSlice } from './rabbitmq-config.interface';

export const rabbitMQConfiguration = (): RabbitMQConfigSlice => {
  return {
    rabbitmq: {
      uri: process.env.SERVICE_DLC_RABBITMQ_URI,
    },
  };
};
