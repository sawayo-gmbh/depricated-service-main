import { applicationConfiguration } from './application';
import { awsConfiguration } from './aws';
import { databaseConfiguration } from './database';
import { graphqlConfiguration } from './graphql';
import { rabbitMQConfiguration } from './rabbitmq';

export const configuration = [
  applicationConfiguration,
  awsConfiguration,
  databaseConfiguration,
  graphqlConfiguration,
  rabbitMQConfiguration,
];
