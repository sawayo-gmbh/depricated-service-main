import { forwardRef, Inject, Injectable } from '@nestjs/common';

import { AwsService, FileData } from '@sawayo/aws';

import { DriverLicenseCheckDomain } from '../../driver-license-check';
import { DriverLicenseCheckSettingsDomain } from '../../driver-license-check-settings';
import { NextCheckDateProvider } from '../infrastructure/providers/next-check.provider';

import {
  ApproveDriverLicenseCheckParameters,
  ChangeNextCheckDateParameters,
  GetDriverLicenseCheckSettingsParameters,
  RejectDriverLicenseCheckParameters,
} from './event-bus-service.interface';

@Injectable()
export class EventBusService {
  constructor(
    @Inject(forwardRef(() => DriverLicenseCheckDomain))
    private readonly driverLicenseCheckDomain: DriverLicenseCheckDomain,
    @Inject(forwardRef(() => DriverLicenseCheckSettingsDomain))
    private readonly driverLicenseCheckSettingsDomain: DriverLicenseCheckSettingsDomain,
    private readonly nextCheckDateProvider: NextCheckDateProvider,
    private readonly awsService: AwsService,
  ) {}

  private filterEmptyFiles(files: FileData[]) {
    return files
      .map((file) => {
        if (!file || !file.key) {
          return null;
        }

        return file;
      })
      .filter(Boolean);
  }

  async approveDriverLicenseCheck({ driverLicenseCheckId }: ApproveDriverLicenseCheckParameters): Promise<void> {
    const driverLicenseCheck = await this.driverLicenseCheckDomain.getDLCById({ id: driverLicenseCheckId });

    if (!driverLicenseCheck) {
      return;
    }

    const { licenseExpires, userId, backside, frontside } = driverLicenseCheck;

    const fileDeletePromises = this.filterEmptyFiles([backside, frontside]).map(({ key }) => {
      return this.awsService.deleteFile({ key });
    });

    await Promise.all(fileDeletePromises);
    await this.driverLicenseCheckDomain.removeDriverLicenseCheckImages({ driverLicenseCheckId, userId });

    if (!licenseExpires) {
      return;
    }

    await this.driverLicenseCheckSettingsDomain.approveDriverLicenseCheck({ licenseExpires, userId });
  }

  async changeNextCheckDate({ nextCheckDate, userId }: ChangeNextCheckDateParameters): Promise<void> {
    await this.driverLicenseCheckSettingsDomain.updateNextCheckDate({
      nextCheck: nextCheckDate,
      userId,
    });
  }

  async getDLCSettings({ userId }: GetDriverLicenseCheckSettingsParameters): Promise<void> {
    const driverLicenseCheckSettings =
      await this.driverLicenseCheckSettingsDomain.getDriverLicenseCheckSettingsByUserId({
        userId,
      });

    this.nextCheckDateProvider.sendDLCSetttings({ driverLicenseCheckSettings });
  }

  async rejectDriverLicenseCheck({ driverLicenseCheckId, userId }: RejectDriverLicenseCheckParameters): Promise<void> {
    const driverLicenseCheck = await this.driverLicenseCheckDomain.getDLCById({ id: driverLicenseCheckId });

    if (!driverLicenseCheck) {
      return;
    }

    const { backside, frontside } = driverLicenseCheck;

    await this.driverLicenseCheckSettingsDomain.rejectDriverLicenseCheck({ userId });
    const fileDeletePromises = this.filterEmptyFiles([backside, frontside]).map(({ key }) => {
      return this.awsService.deleteFile({ key });
    });

    await Promise.all(fileDeletePromises);
    await this.driverLicenseCheckDomain.removeDriverLicenseCheckImages({ driverLicenseCheckId, userId });
  }
}
