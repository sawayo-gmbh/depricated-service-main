import { Types } from 'mongoose';

export interface ApproveDriverLicenseCheckParameters {
  driverLicenseCheckId: Types.ObjectId;
}

export interface ChangeNextCheckDateParameters {
  nextCheckDate: Date;
  userId: Types.ObjectId;
}

export interface GetDriverLicenseCheckSettingsParameters {
  userId: Types.ObjectId;
}

export interface RejectDriverLicenseCheckParameters {
  driverLicenseCheckId: Types.ObjectId;
  userId: Types.ObjectId;
}
