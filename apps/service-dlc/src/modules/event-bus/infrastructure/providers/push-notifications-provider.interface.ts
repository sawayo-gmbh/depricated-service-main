import { Types } from 'mongoose';

import { Notification } from '@sawayo/event-bus/services/old';

export enum NotificationType {
  beforeExpiration = 'beforeExpiration',
  manuallyTriggered = 'manuallyTriggered',
}

export interface SendBeforeExpirationPushNotificationParameters {
  userId: Types.ObjectId;
}

export interface SendManuallyTriggeredPushNotificationParameters {
  userId: Types.ObjectId;
}

export interface SendPushNotificationParameters {
  notification: Notification;
  userId: Types.ObjectId;
}
