import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';

import { Exchange, RoutingKey } from '@sawayo/event-bus';
import { CreateApprovalEvent } from '@sawayo/event-bus/services/approval';

import { CreateApprovalParameters } from './approval-service-provider.interface';

@Injectable()
export class ApprovalServiceProvider {
  constructor(private readonly amqpConnection: AmqpConnection) {}

  async createApproval({ companyId, entityId, type, userId }: CreateApprovalParameters): Promise<void> {
    const event: CreateApprovalEvent = {
      companyId: companyId.toHexString(),
      entityId: entityId.toHexString(),
      type,
      userId: userId.toHexString(),
    };

    await this.amqpConnection.publish(Exchange.direct, RoutingKey.approvalCreateApproval, event);
  }
}
