import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';

import { Exchange, RoutingKey } from '@sawayo/event-bus';
import { PushNotificationEvent } from '@sawayo/event-bus/services/old';

import {
  NotificationType,
  SendBeforeExpirationPushNotificationParameters,
  SendManuallyTriggeredPushNotificationParameters,
  SendPushNotificationParameters,
} from './push-notifications-provider.interface';

@Injectable()
export class PushNotificationsProvider {
  private readonly bodyByNotificationType: Record<NotificationType, string> = {
    [NotificationType.beforeExpiration]: 'Eine neue Führerscheinkontrolle steht an. Führe sie jetzt durch!',
    [NotificationType.manuallyTriggered]: 'Eine neue Führerscheinkontrolle steht an. Führe sie jetzt durch!',
  };

  private readonly titleByNotificationType: Record<NotificationType, string> = {
    [NotificationType.beforeExpiration]: 'Führerscheinkontrolle',
    [NotificationType.manuallyTriggered]: 'Führerscheinkontrolle',
  };

  constructor(private readonly amqpConnection: AmqpConnection) {}

  async sendBeforeExpirationPushNotification({
    userId,
  }: SendBeforeExpirationPushNotificationParameters): Promise<void> {
    const body = this.bodyByNotificationType[NotificationType.beforeExpiration];
    const title = this.titleByNotificationType[NotificationType.beforeExpiration];

    await this.sendPushNotification({
      notification: {
        body,
        title,
      },
      userId,
    });
  }

  async sendManuallyTriggeredPushNotification({
    userId,
  }: SendManuallyTriggeredPushNotificationParameters): Promise<void> {
    const body = this.bodyByNotificationType[NotificationType.manuallyTriggered];
    const title = this.titleByNotificationType[NotificationType.manuallyTriggered];

    await this.sendPushNotification({
      notification: {
        body,
        title,
      },
      userId,
    });
  }

  async sendPushNotification({ notification, userId }: SendPushNotificationParameters): Promise<void> {
    const payload: PushNotificationEvent = {
      notification,
      userId,
    };

    await this.amqpConnection.publish(Exchange.direct, RoutingKey.oldPushNotification, payload);
  }
}
