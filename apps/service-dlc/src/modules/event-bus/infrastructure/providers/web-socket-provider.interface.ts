import { Types } from 'mongoose';

import { WebSocketMessageType, WebSocketNotificationType } from '@sawayo/event-bus/services/old';

import { DriverLicenseCheckSettingsEntity } from '../../../driver-license-check-settings/infrastructure';
import { DriverLicenseCheckEntity } from '../../../driver-license-check/infrastructure';

export interface SendDriverLicenseCheckCreatedWebSocketMessageParameters {
  driverLicenseCheck: DriverLicenseCheckEntity;
  userId: Types.ObjectId;
}

export interface SendDriverLicenseCheckSettingsUpdatedWebSocketMessageParameters {
  driverLicenseCheckSettings: DriverLicenseCheckSettingsEntity;
  userId: Types.ObjectId;
}

export interface SendWebSocketMessageParameters {
  message: unknown;
  type: WebSocketMessageType;
  userId: Types.ObjectId;
}

export interface SendWebSocketNotificationParameters {
  message: string;
  payload: unknown;
  subject: string;
  type: WebSocketNotificationType;
  userId: Types.ObjectId;
}
