import { DriverLicenseCheckEntity } from '../../../driver-license-check/infrastructure';

export interface SendDriverLicenseCheckSettingsParameters {
  driverLicenseCheckSettings: DriverLicenseCheckEntity;
}
