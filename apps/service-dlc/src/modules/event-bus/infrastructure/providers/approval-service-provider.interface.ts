import { Types } from 'mongoose';

import { ApprovalType } from '@sawayo/event-bus/services/approval';

export interface CreateApprovalParameters {
  companyId: Types.ObjectId;
  entityId: Types.ObjectId;
  type: ApprovalType;
  userId: Types.ObjectId;
}
