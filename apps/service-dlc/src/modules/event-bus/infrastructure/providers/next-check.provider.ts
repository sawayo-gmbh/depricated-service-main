import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';

import { Exchange, RoutingKey } from '@sawayo/event-bus';

import { SendDriverLicenseCheckSettingsParameters } from './next-check-date-provider.interface';

@Injectable()
export class NextCheckDateProvider {
  constructor(private readonly amqpConnection: AmqpConnection) {}

  async sendDLCSetttings({ driverLicenseCheckSettings }: SendDriverLicenseCheckSettingsParameters): Promise<void> {
    const event = {
      driverLicenseCheckSettings,
    };

    await this.amqpConnection.publish(Exchange.direct, RoutingKey.sendDlcSettings, event);
  }
}
