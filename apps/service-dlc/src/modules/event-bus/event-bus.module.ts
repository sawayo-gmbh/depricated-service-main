import { forwardRef, Module } from '@nestjs/common';

import { DriverLicenseCheckModule } from '../driver-license-check';
import { DriverLicenseCheckSettingsModule } from '../driver-license-check-settings';

import { EventBusService } from './application';
import {
  ApprovalServiceProvider,
  NextCheckDateProvider,
  PushNotificationsProvider,
  WebSocketProvider,
} from './infrastructure';
import { EventBusHandler } from './presentation';

@Module({
  controllers: [],
  exports: [ApprovalServiceProvider, NextCheckDateProvider, PushNotificationsProvider, WebSocketProvider],
  imports: [forwardRef(() => DriverLicenseCheckModule), forwardRef(() => DriverLicenseCheckSettingsModule)],
  providers: [
    ApprovalServiceProvider,
    EventBusHandler,
    EventBusService,
    NextCheckDateProvider,
    PushNotificationsProvider,
    WebSocketProvider,
  ],
})
export class EventBusModule {}
