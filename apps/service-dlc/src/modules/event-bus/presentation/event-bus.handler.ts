import { MessageHandlerErrorBehavior, RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { ConsumeMessage } from 'amqplib';
import { Types } from 'mongoose';

import { Exchange, Queue, RoutingKey } from '@sawayo/event-bus';
import {
  DriverLicenseCheckApprovedEvent,
  DriverLicenseCheckRejectedEvent,
} from '@sawayo/event-bus/services/driver-license-check';

import { EventBusService } from '../application';

@Injectable()
export class EventBusHandler {
  constructor(private readonly eventBusService: EventBusService) {}

  private async handleChangeNextCheckDate({ nextCheckDate, userId }): Promise<void> {
    await this.eventBusService.changeNextCheckDate({
      nextCheckDate,
      userId: Types.ObjectId(userId),
    });
  }

  private async handleDriverLicenseCheckApproved({
    driverLicenseCheckId,
  }: DriverLicenseCheckApprovedEvent): Promise<void> {
    return this.eventBusService.approveDriverLicenseCheck({
      driverLicenseCheckId: Types.ObjectId(driverLicenseCheckId),
    });
  }

  private async handleDriverLicenseCheckRejected({
    driverLicenseCheckId,
    userId,
  }: DriverLicenseCheckRejectedEvent): Promise<void> {
    return this.eventBusService.rejectDriverLicenseCheck({
      driverLicenseCheckId: Types.ObjectId(driverLicenseCheckId),
      userId: Types.ObjectId(userId),
    });
  }

  private async handleGetDLCSettings({ userId }): Promise<void> {
    return this.eventBusService.getDLCSettings({
      userId: Types.ObjectId(userId),
    });
  }

  @RabbitSubscribe({
    exchange: Exchange.direct,
    queue: Queue.driverLicenseCheck,
    routingKey: [RoutingKey.driverLicenseCheckApproved, RoutingKey.driverLicenseCheckRejected],
    errorBehavior: MessageHandlerErrorBehavior.NACK,
  })
  async handleDriverLicenseCheckQueueMessage(event: any, amqpMessage: ConsumeMessage): Promise<void> {
    const { fields } = amqpMessage;
    const { routingKey } = fields;

    if (routingKey === RoutingKey.driverLicenseCheckApproved) {
      await this.handleDriverLicenseCheckApproved(event);
    }

    if (routingKey === RoutingKey.driverLicenseCheckRejected) {
      await this.handleDriverLicenseCheckRejected(event);
    }
  }

  @RabbitSubscribe({
    exchange: Exchange.direct,
    queue: Queue.nextCheck,
    routingKey: [RoutingKey.nextCheck, RoutingKey.dlcSettings],
    errorBehavior: MessageHandlerErrorBehavior.NACK,
  })
  async handleNextCheckQueueMessage(event: any, amqpMessage: ConsumeMessage): Promise<void> {
    const { fields } = amqpMessage;
    const { routingKey } = fields;

    if (routingKey === RoutingKey.nextCheck) {
      await this.handleChangeNextCheckDate(event);
    }

    if (routingKey === RoutingKey.dlcSettings) {
      await this.handleGetDLCSettings(event);
    }
  }
}
