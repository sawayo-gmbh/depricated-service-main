import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { TimeModule } from '@sawayo/time';

import { EventBusModule } from '../event-bus';
import { UsersModule } from '../users';

import { DriverLicenseCheckSettingsService } from './application';
import { DriverLicenseCheckSettingsDomain } from './domain';
import {
  DriverLicenseCheckSettingsEntity,
  DriverLicenseCheckSettingsRepository,
  DriverLicenseCheckSettingsSchema,
} from './infrastructure';
import { DriverLicenseCheckSettingsResolver } from './presentation';

@Module({
  controllers: [],
  exports: [DriverLicenseCheckSettingsDomain],
  imports: [
    MongooseModule.forFeature([
      { name: DriverLicenseCheckSettingsEntity.name, schema: DriverLicenseCheckSettingsSchema },
    ]),
    TimeModule,
    UsersModule,
    forwardRef(() => EventBusModule),
  ],
  providers: [
    DriverLicenseCheckSettingsDomain,
    DriverLicenseCheckSettingsRepository,
    DriverLicenseCheckSettingsResolver,
    DriverLicenseCheckSettingsService,
  ],
})
export class DriverLicenseCheckSettingsModule {}
