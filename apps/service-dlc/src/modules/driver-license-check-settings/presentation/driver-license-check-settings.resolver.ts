import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Types } from 'mongoose';

import { CurrentUser, CurrentUserArg, GqlAuthGuard, Role, Roles } from '@sawayo/auth';

import { DriverLicenseCheckSettingsService } from '../application';

import {
  CreateDriverLicenseCheckSettingsInput,
  DriverLicenseCheckSettingsDto,
  TriggerDriverLicenseCheckInput,
  UpdateDriverLicenseCheckSettingsInput,
} from './dto';

@Resolver(() => DriverLicenseCheckSettingsDto)
@UseGuards(GqlAuthGuard)
export class DriverLicenseCheckSettingsResolver {
  constructor(private readonly driverLicenceCheckSettingsService: DriverLicenseCheckSettingsService) {}

  @Mutation(() => DriverLicenseCheckSettingsDto, { name: 'createDriverLicenseCheckSettings' })
  @Roles(Role.companyOwner)
  async createDriverLicenseCheckSettings(
    @Args('input') input: CreateDriverLicenseCheckSettingsInput,
  ): Promise<DriverLicenseCheckSettingsDto> {
    const { enabled, endDate, repetition, startDate, trigger, userId } = input;

    return this.driverLicenceCheckSettingsService.createDriverLicenseCheckSettings({
      enabled,
      endDate,
      repetition,
      startDate,
      trigger,
      userId: Types.ObjectId(userId),
    });
  }

  @Query(() => DriverLicenseCheckSettingsDto, { name: 'driverLicenseCheckSettings', nullable: true })
  async getDriverLicenseCheckSettings(
    @CurrentUserArg() currentUser: CurrentUser,
    @Args('userId', { nullable: true }) userId?: string,
  ): Promise<DriverLicenseCheckSettingsDto> {
    const { role, userId: currentUserId } = currentUser;

    return this.driverLicenceCheckSettingsService.getDriverLicenseCheckSettingsByUserId({
      currentUserId,
      role,
      userId: userId && Types.ObjectId(userId),
    });
  }

  @Mutation(() => DriverLicenseCheckSettingsDto, { name: 'triggerDriverLicenseCheck' })
  @Roles(Role.companyOwner)
  async triggerDriverLicenseCheck(
    @Args('input') input: TriggerDriverLicenseCheckInput,
  ): Promise<DriverLicenseCheckSettingsDto> {
    const { userId } = input;

    return this.driverLicenceCheckSettingsService.triggerDriverLicenseCheckByUserId({ userId: Types.ObjectId(userId) });
  }

  @Mutation(() => DriverLicenseCheckSettingsDto, { name: 'updateDriverLicenseCheckSettings' })
  @Roles(Role.companyOwner)
  async updateDriverLicenseCheckSettings(
    @Args('input') input: UpdateDriverLicenseCheckSettingsInput,
  ): Promise<DriverLicenseCheckSettingsDto> {
    const { driverLicenseCheckSettingsId, enabled, endDate, repetition, startDate, trigger } = input;

    return this.driverLicenceCheckSettingsService.updateDriverLicenseCheckSettings({
      driverLicenseCheckSettingsId: Types.ObjectId(driverLicenseCheckSettingsId),
      enabled,
      endDate,
      repetition,
      startDate,
      trigger,
    });
  }
}
