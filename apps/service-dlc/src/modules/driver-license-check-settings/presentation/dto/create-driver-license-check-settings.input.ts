import { Field, InputType } from '@nestjs/graphql';
import { IsEnum, IsOptional } from 'class-validator';

import { Repetition, Trigger } from '../../core/types';

@InputType()
export class CreateDriverLicenseCheckSettingsInput {
  @Field(() => Boolean, { nullable: false })
  enabled: boolean;

  @Field(() => Date, { nullable: true })
  endDate?: Date;

  @Field(() => Repetition, { nullable: true })
  @IsOptional()
  @IsEnum(Repetition)
  repetition?: Repetition;

  @Field(() => Date, { nullable: false })
  startDate: Date;

  @Field(() => Trigger, { nullable: false })
  @IsEnum(Trigger)
  trigger: Trigger;

  @Field(() => String, { nullable: false })
  userId: string;
}
