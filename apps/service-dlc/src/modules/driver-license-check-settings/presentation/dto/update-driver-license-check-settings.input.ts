import { Field, InputType } from '@nestjs/graphql';
import { IsEnum, IsOptional } from 'class-validator';

import { Repetition, Trigger } from '../../core/types';

@InputType()
export class UpdateDriverLicenseCheckSettingsInput {
  @Field(() => String, { nullable: false })
  driverLicenseCheckSettingsId: string;

  @Field(() => Boolean, { nullable: true })
  enabled: boolean;

  @Field(() => Date, { nullable: true })
  endDate?: Date;

  @Field(() => Repetition, { nullable: true })
  @IsOptional()
  @IsEnum(Repetition)
  repetition?: Repetition;

  @Field(() => Date, { nullable: true })
  startDate: Date;

  @Field(() => Trigger, { nullable: true })
  @IsOptional()
  @IsEnum(Trigger)
  trigger: Trigger;
}
