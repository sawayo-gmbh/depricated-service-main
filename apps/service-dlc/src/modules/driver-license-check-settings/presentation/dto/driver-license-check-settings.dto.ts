import { Field, ObjectType } from '@nestjs/graphql';
import { Types } from 'mongoose';

import { Repetition, Trigger } from '../../core/types';

@ObjectType()
export class DriverLicenseCheckSettingsDto {
  @Field(() => String, { nullable: false })
  _id: Types.ObjectId;

  @Field(() => Date, { nullable: false })
  createdAt: Date;

  @Field(() => Boolean, { nullable: false })
  enabled: boolean;

  @Field(() => Date, { nullable: true })
  endDate?: Date;

  @Field(() => Date, { nullable: true })
  nextCheck?: Date;

  @Field(() => Repetition, { nullable: true })
  repetition?: Repetition;

  @Field(() => Date, { nullable: true })
  startDate?: Date;

  @Field(() => Trigger, { nullable: false })
  trigger: Trigger;

  @Field(() => Date, { nullable: false })
  updatedAt: Date;

  @Field(() => String, { nullable: false })
  userId: Types.ObjectId;
}
