import { Field, InputType } from '@nestjs/graphql';
import { IsString } from 'class-validator';

@InputType()
export class TriggerDriverLicenseCheckInput {
  @Field(() => String, { nullable: false })
  @IsString()
  userId: string;
}
