import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Transform } from 'class-transformer';
import { Document, Types } from 'mongoose';

import { Repetition, Trigger } from '../../core/types';

export type DriverLicenseCheckSettingsDocument = DriverLicenseCheckSettingsEntity & Document;

@Schema({
  collection: 'driverLicenseCheckSettings',
  timestamps: true,
  versionKey: false,
})
export class DriverLicenseCheckSettingsEntity {
  @Transform(({ obj }) => obj._id, { toClassOnly: true })
  readonly _id!: Types.ObjectId;

  @Prop()
  changed?: boolean;

  createdAt: Date;

  @Prop()
  enabled: boolean;

  @Prop()
  endDate?: Date;

  @Prop()
  nextCheck?: Date;

  @Prop()
  repetition?: Repetition;

  @Prop()
  startDate: Date;

  @Prop()
  trigger: Trigger;

  updatedAt: Date;

  @Prop({ type: Types.ObjectId })
  userId: Types.ObjectId;

  constructor(init?: Partial<DriverLicenseCheckSettingsEntity>) {
    Object.assign(this, init);
  }
}

export const DriverLicenseCheckSettingsSchema = SchemaFactory.createForClass(DriverLicenseCheckSettingsEntity);
