import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { removeUndefined, Repository } from '@sawayo/mongo';

import { Trigger } from '../../core/types';
import {
  DriverLicenseCheckSettingsEntity,
  DriverLicenseCheckSettingsDocument,
} from '../entities/driver-license-check-settings.entity';

import {
  CreateDriverLicenseCheckSettingsParameters,
  GetDriverLicenseCheckSettingsByIdParameters,
  GetDriverLicenseCheckSettingsByUserIdParameters,
  GetDriverLicenseCheckSettingsInPeriodParameters,
  UpdateDriverLicenseCheckSettingsByUserIdParameters,
  UpdateDriverLicenseCheckSettingsParameters,
} from './driver-license-check-settings-repository.interface';

@Injectable()
export class DriverLicenseCheckSettingsRepository extends Repository<
  DriverLicenseCheckSettingsEntity,
  DriverLicenseCheckSettingsDocument
> {
  constructor(
    @InjectModel(DriverLicenseCheckSettingsEntity.name)
    private driverLicenseCheckSettingsEntity: Model<DriverLicenseCheckSettingsDocument>,
  ) {
    super(driverLicenseCheckSettingsEntity, { baseClass: DriverLicenseCheckSettingsEntity });
  }

  async createDriverLicenseCheckSettings({
    enabled,
    endDate,
    nextCheck,
    repetition,
    startDate,
    trigger,
    userId,
  }: CreateDriverLicenseCheckSettingsParameters): Promise<DriverLicenseCheckSettingsEntity> {
    return this.create({
      enabled,
      endDate,
      nextCheck,
      repetition,
      startDate,
      trigger,
      userId,
    });
  }

  async getDriverLicenseCheckSettingsById({
    driverLicenseCheckSettingsId,
  }: GetDriverLicenseCheckSettingsByIdParameters): Promise<DriverLicenseCheckSettingsEntity> {
    return this.findOne({ _id: driverLicenseCheckSettingsId });
  }

  async getDriverLicenseCheckSettingsByUserId({
    userId,
  }: GetDriverLicenseCheckSettingsByUserIdParameters): Promise<DriverLicenseCheckSettingsEntity> {
    return this.findOne({ userId });
  }

  async getDriverLicenseCheckSettingsInPeriod({
    endDate,
    startDate,
  }: GetDriverLicenseCheckSettingsInPeriodParameters): Promise<DriverLicenseCheckSettingsEntity[]> {
    return this.find({
      enabled: true,
      nextCheck: { $gte: startDate, $lte: endDate },
      trigger: Trigger.automated,
    });
  }

  async updateDriverLicenseCheckSettingsByUserId({
    changed,
    enabled,
    endDate,
    nextCheck,
    repetition,
    startDate,
    trigger,
    userId,
  }: UpdateDriverLicenseCheckSettingsByUserIdParameters): Promise<DriverLicenseCheckSettingsEntity> {
    await this.update(
      { userId },
      removeUndefined({
        changed,
        enabled,
        endDate,
        nextCheck,
        repetition,
        startDate,
        trigger,
      }),
    );

    return this.getDriverLicenseCheckSettingsByUserId({ userId });
  }

  async updateDriverLicenseCheckSettings({
    changed,
    driverLicenseCheckSettingsId,
    enabled,
    endDate,
    nextCheck,
    repetition,
    startDate,
    trigger,
  }: UpdateDriverLicenseCheckSettingsParameters): Promise<DriverLicenseCheckSettingsEntity> {
    await this.update(
      { _id: driverLicenseCheckSettingsId },
      removeUndefined({
        changed,
        enabled,
        endDate,
        nextCheck,
        repetition,
        startDate,
        trigger,
      }),
    );

    return this.getDriverLicenseCheckSettingsById({ driverLicenseCheckSettingsId });
  }
}
