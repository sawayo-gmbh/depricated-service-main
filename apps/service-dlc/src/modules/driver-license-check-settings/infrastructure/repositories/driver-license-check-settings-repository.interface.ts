import { Types } from 'mongoose';

import { Repetition, Trigger } from '../../core/types';

export interface CreateDriverLicenseCheckSettingsParameters {
  enabled: boolean;
  endDate?: Date;
  nextCheck?: Date;
  repetition?: Repetition;
  startDate: Date;
  trigger: Trigger;
  userId: Types.ObjectId;
}

export interface GetDriverLicenseCheckSettingsByIdParameters {
  driverLicenseCheckSettingsId: Types.ObjectId;
}

export interface GetDriverLicenseCheckSettingsByUserIdParameters {
  userId: Types.ObjectId;
}

export interface GetDriverLicenseCheckSettingsInPeriodParameters {
  endDate: Date;
  startDate: Date;
}

export interface UpdateDriverLicenseCheckSettingsByUserIdParameters {
  changed?: boolean;
  enabled?: boolean;
  endDate?: Date;
  nextCheck?: Date;
  repetition?: Repetition;
  startDate?: Date;
  trigger?: Trigger;
  userId: Types.ObjectId;
}

export interface UpdateDriverLicenseCheckSettingsParameters {
  changed?: boolean;
  driverLicenseCheckSettingsId: Types.ObjectId;
  enabled?: boolean;
  endDate?: Date;
  nextCheck?: Date;
  repetition?: Repetition;
  startDate?: Date;
  trigger?: Trigger;
}
