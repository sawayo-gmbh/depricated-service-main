export enum Repetition {
  everyMonth = 'everyMonth',
  everySixMonths = 'everySixMonths',
  everyThreeDays = 'everyThreeDays',
  everyThreeMonths = 'everyThreeMonths',
  everyTwoMonths = 'everyTwoMonths',
  everyWeek = 'everyWeek',
}
