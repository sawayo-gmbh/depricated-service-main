export enum Trigger {
  automated = 'automated',
  manualByEmployee = 'manualByEmployee',
  manualByManager = 'manualByManager',
}
