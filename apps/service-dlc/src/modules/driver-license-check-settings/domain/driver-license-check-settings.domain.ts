import { Injectable } from '@nestjs/common';

import { TimeService } from '@sawayo/time';

import { NextCheckDateProvider, PushNotificationsProvider, WebSocketProvider } from '../../event-bus/infrastructure';
import { UserRepository } from '../../users';
import { Repetition, Trigger } from '../core/types';
import { DriverLicenseCheckSettingsRepository } from '../infrastructure';

import {
  ApproveDriverLicenseCheckParameters,
  CreateDriverLicenseCheckSettingsParameters,
  GetDriverLicenseCheckSettingsByIdParameters,
  GetDriverLicenseCheckSettingsByUserIdParameters,
  GetDriverLicenseCheckSettingsInPeriodParameters,
  RejectDriverLicenseCheckParameters,
  ShiftNextCheckByUserIdParameters,
  TriggerDriverLicenseCheckByUserIdParameters,
  UpdateDriverLicenseCheckSettingsParameters,
  UpdateNextCheckDateParameters,
} from './driver-license-check-settings-domain.interface';

@Injectable()
export class DriverLicenseCheckSettingsDomain {
  private readonly DRIVER_LICENSE_CHECK_DEADLINE_DAYS = 2;

  constructor(
    private readonly driverLicenseCheckSettingsRepository: DriverLicenseCheckSettingsRepository,
    private readonly nextCheckDateProvider: NextCheckDateProvider,
    private readonly pushNotificationsProvider: PushNotificationsProvider,
    private readonly timeService: TimeService,
    private readonly userRepository: UserRepository,
    private readonly webSocketProvider: WebSocketProvider,
  ) {}

  private getNextCheckDate(startDate: Date, repetition: Repetition): Date {
    if (!startDate) {
      return null;
    }

    if (repetition === Repetition.everyMonth) {
      return this.timeService.addMonths(startDate, 1);
    }

    if (repetition === Repetition.everySixMonths) {
      return this.timeService.addMonths(startDate, 6);
    }

    if (repetition === Repetition.everyThreeDays) {
      return this.timeService.addDays(startDate, 2);
    }

    if (repetition === Repetition.everyThreeMonths) {
      return this.timeService.addMonths(startDate, 3);
    }

    if (repetition === Repetition.everyTwoMonths) {
      return this.timeService.addMonths(startDate, 2);
    }

    if (repetition === Repetition.everyWeek) {
      return this.timeService.addWeeks(startDate, 1);
    }

    return startDate;
  }

  private getShiftedNextCheckDate(nextCheck: Date, trigger: Trigger, repetition: Repetition): Date {
    if (trigger !== Trigger.automated) {
      return null;
    }

    return this.getNextCheckDate(nextCheck, repetition);
  }

  async approveDriverLicenseCheck({ licenseExpires, userId }: ApproveDriverLicenseCheckParameters): Promise<void> {
    const driverLicenseCheckSettings =
      await this.driverLicenseCheckSettingsRepository.getDriverLicenseCheckSettingsByUserId({ userId });

    if (!driverLicenseCheckSettings) {
      return;
    }

    const { trigger } = driverLicenseCheckSettings;

    if (trigger === Trigger.manualByEmployee) {
      return;
    }

    const nextCheck = this.timeService.addDays(licenseExpires, 1);

    const driverLicenseCheckSettingsUpdated =
      await this.driverLicenseCheckSettingsRepository.updateDriverLicenseCheckSettingsByUserId({ nextCheck, userId });

    await this.nextCheckDateProvider.sendDLCSetttings({
      driverLicenseCheckSettings: driverLicenseCheckSettingsUpdated,
    });
  }

  async createDriverLicenseCheckSettings({
    enabled,
    endDate,
    repetition,
    startDate,
    trigger,
    userId,
  }: CreateDriverLicenseCheckSettingsParameters) {
    const nextCheck = trigger === Trigger.automated ? startDate : null;

    const driverLicenseCheckSettings = await this.driverLicenseCheckSettingsRepository.createDriverLicenseCheckSettings(
      {
        enabled,
        endDate,
        nextCheck,
        repetition,
        startDate,
        trigger,
        userId,
      },
    );

    if (nextCheck) {
      await this.nextCheckDateProvider.sendDLCSetttings({ driverLicenseCheckSettings });
    }

    return driverLicenseCheckSettings;
  }

  async getDriverLicenseCheckSettingsById({
    driverLicenseCheckSettingsId,
  }: GetDriverLicenseCheckSettingsByIdParameters) {
    return this.driverLicenseCheckSettingsRepository.getDriverLicenseCheckSettingsById({
      driverLicenseCheckSettingsId,
    });
  }

  async getDriverLicenseCheckSettingsByUserId({ userId }: GetDriverLicenseCheckSettingsByUserIdParameters) {
    return this.driverLicenseCheckSettingsRepository.getDriverLicenseCheckSettingsByUserId({ userId });
  }

  async getDriverLicenseCheckSettingsInPeriod({ endDate, startDate }: GetDriverLicenseCheckSettingsInPeriodParameters) {
    return this.driverLicenseCheckSettingsRepository.getDriverLicenseCheckSettingsInPeriod({
      endDate,
      startDate,
    });
  }

  async rejectDriverLicenseCheck({ userId }: RejectDriverLicenseCheckParameters): Promise<void> {
    const driverLicenseCheckSettings =
      await this.driverLicenseCheckSettingsRepository.getDriverLicenseCheckSettingsByUserId({ userId });

    if (!driverLicenseCheckSettings) {
      return;
    }

    const { trigger } = driverLicenseCheckSettings;

    if (trigger === Trigger.manualByEmployee) {
      return;
    }

    const today = this.timeService.startOfDay(new Date());
    const nextCheck = this.timeService.addDays(today, this.DRIVER_LICENSE_CHECK_DEADLINE_DAYS);

    const driverLicenseCheckSettingsUpdated =
      await this.driverLicenseCheckSettingsRepository.updateDriverLicenseCheckSettingsByUserId({ nextCheck, userId });

    await this.webSocketProvider.sendDriverLicenseCheckSettingsUpdatedWebSocketMessage({
      driverLicenseCheckSettings: driverLicenseCheckSettingsUpdated,
      userId,
    });
  }

  async shiftNextCheckByUserId({ userId }: ShiftNextCheckByUserIdParameters): Promise<void> {
    const driverLicenseCheckSettings =
      await this.driverLicenseCheckSettingsRepository.getDriverLicenseCheckSettingsByUserId({ userId });

    if (!driverLicenseCheckSettings) {
      return;
    }

    const { _id: driverLicenseCheckSettingsId, nextCheck, repetition, trigger } = driverLicenseCheckSettings;

    if (!nextCheck) {
      return;
    }

    const shiftedNextCheck = this.getShiftedNextCheckDate(nextCheck, trigger, repetition);

    const updatedDriverLicenseCheckSettings =
      await this.driverLicenseCheckSettingsRepository.updateDriverLicenseCheckSettings({
        driverLicenseCheckSettingsId,
        nextCheck: shiftedNextCheck,
      });

    await this.nextCheckDateProvider.sendDLCSetttings({
      driverLicenseCheckSettings: updatedDriverLicenseCheckSettings,
    });
  }

  async triggerDriverLicenseCheckByUserId({ userId }: TriggerDriverLicenseCheckByUserIdParameters) {
    const today = this.timeService.startOfDay(new Date());
    const nextCheck = this.timeService.addDays(today, this.DRIVER_LICENSE_CHECK_DEADLINE_DAYS);

    const driverLicenseCheckSettings =
      await this.driverLicenseCheckSettingsRepository.updateDriverLicenseCheckSettingsByUserId({ nextCheck, userId });

    await this.pushNotificationsProvider.sendManuallyTriggeredPushNotification({ userId });
    await this.webSocketProvider.sendDriverLicenseCheckSettingsUpdatedWebSocketMessage({
      driverLicenseCheckSettings,
      userId,
    });

    return driverLicenseCheckSettings;
  }

  async updateDriverLicenseCheckSettings({
    driverLicenseCheckSettingsId,
    enabled,
    endDate,
    existingSettings,
    repetition,
    startDate,
    trigger,
  }: UpdateDriverLicenseCheckSettingsParameters) {
    const { startDate: prevStartDate, trigger: prevTrigger, userId } = existingSettings;

    const isAutomatedTrigger = (trigger || prevTrigger) === Trigger.automated;

    const nextCheck = isAutomatedTrigger && startDate !== null ? startDate || prevStartDate : null;

    const driverLicenseCheckSettings = await this.driverLicenseCheckSettingsRepository.updateDriverLicenseCheckSettings(
      {
        changed: true,
        driverLicenseCheckSettingsId,
        enabled,
        endDate,
        nextCheck,
        repetition: isAutomatedTrigger ? repetition : null,
        startDate,
        trigger,
      },
    );

    if (nextCheck) {
      await this.nextCheckDateProvider.sendDLCSetttings({ driverLicenseCheckSettings });
    }

    await this.webSocketProvider.sendDriverLicenseCheckSettingsUpdatedWebSocketMessage({
      driverLicenseCheckSettings,
      userId,
    });

    return driverLicenseCheckSettings;
  }

  async updateNextCheckDate({ nextCheck, userId }: UpdateNextCheckDateParameters): Promise<void> {
    const driverLicenseCheckSettings =
      await this.driverLicenseCheckSettingsRepository.getDriverLicenseCheckSettingsByUserId({ userId });

    const { changed, createdAt, enabled, nextCheck: previousNextCheck, updatedAt } = driverLicenseCheckSettings;

    await this.driverLicenseCheckSettingsRepository.updateDriverLicenseCheckSettingsByUserId({
      changed: false,
      nextCheck,
      userId,
    });

    if (
      enabled &&
      (!this.timeService.isSame(previousNextCheck, nextCheck) ||
        this.timeService.isSame(createdAt, updatedAt) ||
        changed)
    ) {
      const differenceInDays = Math.abs(this.timeService.differenceInDays(nextCheck, new Date()));

      if (differenceInDays <= this.DRIVER_LICENSE_CHECK_DEADLINE_DAYS) {
        await this.pushNotificationsProvider.sendBeforeExpirationPushNotification({ userId });
      }
    }

    const companyOwnerId = await this.userRepository.getCompanyOwnerIdByUserId({ userId });

    const driverLicenseCheckSettingsPayload = {
      ...driverLicenseCheckSettings,
      nextCheck,
    };

    await this.webSocketProvider.sendDriverLicenseCheckSettingsUpdatedWebSocketMessage({
      driverLicenseCheckSettings: driverLicenseCheckSettingsPayload,
      userId: companyOwnerId,
    });
    await this.webSocketProvider.sendDriverLicenseCheckSettingsUpdatedWebSocketMessage({
      driverLicenseCheckSettings: driverLicenseCheckSettingsPayload,
      userId,
    });
  }
}
