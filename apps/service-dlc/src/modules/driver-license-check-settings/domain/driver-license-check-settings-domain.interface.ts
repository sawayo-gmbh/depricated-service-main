import { Types } from 'mongoose';

import { Repetition, Trigger } from '../core/types';
import { DriverLicenseCheckSettingsEntity } from '../infrastructure';

export interface ApproveDriverLicenseCheckParameters {
  licenseExpires: Date;
  userId: Types.ObjectId;
}

export interface CreateDriverLicenseCheckSettingsParameters {
  enabled: boolean;
  endDate?: Date;
  repetition?: Repetition;
  startDate: Date;
  trigger: Trigger;
  userId: Types.ObjectId;
}

export interface GetDriverLicenseCheckSettingsByIdParameters {
  driverLicenseCheckSettingsId: Types.ObjectId;
}

export interface GetDriverLicenseCheckSettingsByUserIdParameters {
  userId: Types.ObjectId;
}

export interface GetDriverLicenseCheckSettingsInPeriodParameters {
  endDate: Date;
  startDate: Date;
}

export interface RejectDriverLicenseCheckParameters {
  userId: Types.ObjectId;
}

export interface ShiftNextCheckByUserIdParameters {
  userId: Types.ObjectId;
}

export interface TriggerDriverLicenseCheckByUserIdParameters {
  userId: Types.ObjectId;
}

export interface UpdateDriverLicenseCheckSettingsParameters {
  driverLicenseCheckSettingsId: Types.ObjectId;
  enabled?: boolean;
  endDate?: Date;
  existingSettings: DriverLicenseCheckSettingsEntity;
  repetition?: Repetition;
  startDate?: Date;
  trigger?: Trigger;
}

export interface UpdateNextCheckDateParameters {
  nextCheck: Date;
  userId: Types.ObjectId;
}
