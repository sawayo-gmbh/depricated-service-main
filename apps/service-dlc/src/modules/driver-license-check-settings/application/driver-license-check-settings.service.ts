import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';

import { Role } from '@sawayo/auth';

import { Trigger } from '../core/types';
import { DriverLicenseCheckSettingsDomain } from '../domain';
import { DriverLicenseCheckSettingsDto } from '../presentation/dto';

import {
  CreateDriverLicenseCheckSettingsParameters,
  GetDriverLicenseCheckSettingsByUserIdParameters,
  TriggerDriverLicenseCheckByUserIdParameters,
  UpdateDriverLicenseCheckSettingsParameters,
} from './driver-license-check-settings-service.interface';

// TODO: Add meaningful error messages
@Injectable()
export class DriverLicenseCheckSettingsService {
  constructor(private readonly driverLicenseCheckSettingsDomain: DriverLicenseCheckSettingsDomain) {}

  async createDriverLicenseCheckSettings({
    enabled,
    endDate,
    repetition,
    startDate,
    trigger,
    userId,
  }: CreateDriverLicenseCheckSettingsParameters): Promise<DriverLicenseCheckSettingsDto> {
    const existingSettings = await this.driverLicenseCheckSettingsDomain.getDriverLicenseCheckSettingsByUserId({
      userId,
    });

    if (existingSettings) {
      throw new BadRequestException();
    }

    if (trigger === Trigger.automated && !repetition) {
      throw new BadRequestException();
    }

    return this.driverLicenseCheckSettingsDomain.createDriverLicenseCheckSettings({
      enabled,
      endDate,
      repetition,
      startDate,
      trigger,
      userId,
    });
  }

  async getDriverLicenseCheckSettingsByUserId({
    currentUserId,
    role,
    userId,
  }: GetDriverLicenseCheckSettingsByUserIdParameters): Promise<DriverLicenseCheckSettingsDto> {
    if (userId && !currentUserId.equals(userId) && role !== Role.companyOwner) {
      throw new ForbiddenException();
    }

    return this.driverLicenseCheckSettingsDomain.getDriverLicenseCheckSettingsByUserId({
      userId: userId || currentUserId,
    });
  }

  async triggerDriverLicenseCheckByUserId({
    userId,
  }: TriggerDriverLicenseCheckByUserIdParameters): Promise<DriverLicenseCheckSettingsDto> {
    const driverLicenseCheckSettings =
      await this.driverLicenseCheckSettingsDomain.getDriverLicenseCheckSettingsByUserId({
        userId,
      });

    if (!driverLicenseCheckSettings) {
      throw new NotFoundException();
    }

    const { enabled } = driverLicenseCheckSettings;

    if (!enabled) {
      throw new BadRequestException();
    }

    return this.driverLicenseCheckSettingsDomain.triggerDriverLicenseCheckByUserId({
      userId,
    });
  }

  async updateDriverLicenseCheckSettings({
    driverLicenseCheckSettingsId,
    enabled,
    endDate,
    repetition,
    startDate,
    trigger,
  }: UpdateDriverLicenseCheckSettingsParameters): Promise<DriverLicenseCheckSettingsDto> {
    const existingSettings = await this.driverLicenseCheckSettingsDomain.getDriverLicenseCheckSettingsById({
      driverLicenseCheckSettingsId,
    });

    if (!existingSettings) {
      throw new NotFoundException();
    }

    if (existingSettings.trigger !== trigger && trigger === Trigger.automated && !repetition) {
      throw new BadRequestException();
    }

    return this.driverLicenseCheckSettingsDomain.updateDriverLicenseCheckSettings({
      driverLicenseCheckSettingsId,
      enabled,
      endDate,
      existingSettings,
      repetition,
      startDate,
      trigger,
    });
  }
}
