import { Types } from 'mongoose';

import { Role } from '@sawayo/auth';

import { Repetition, Trigger } from '../core/types';

export interface CreateDriverLicenseCheckSettingsParameters {
  enabled: boolean;
  endDate?: Date;
  repetition?: Repetition;
  startDate: Date;
  trigger: Trigger;
  userId: Types.ObjectId;
}

export interface GetDriverLicenseCheckSettingsByUserIdParameters {
  currentUserId: Types.ObjectId;
  role: Role;
  userId?: Types.ObjectId;
}

export interface TriggerDriverLicenseCheckByUserIdParameters {
  userId: Types.ObjectId;
}

export interface UpdateDriverLicenseCheckSettingsParameters {
  driverLicenseCheckSettingsId: Types.ObjectId;
  enabled?: boolean;
  endDate?: Date;
  repetition?: Repetition;
  startDate?: Date;
  trigger?: Trigger;
}
