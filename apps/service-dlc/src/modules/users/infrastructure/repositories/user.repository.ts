import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

import { Role } from '@sawayo/auth';
import { Repository } from '@sawayo/mongo';

import { UserDocument, UserEntity } from '../entities';

import { GetCompanyOwnerIdByUserIdParameters } from './user-repository.interface';

@Injectable()
export class UserRepository extends Repository<UserEntity, UserDocument> {
  constructor(@InjectModel(UserEntity.name) private userEntity: Model<UserDocument>) {
    super(userEntity, { baseClass: UserEntity });
  }

  async getCompanyOwnerIdByUserId({ userId }: GetCompanyOwnerIdByUserIdParameters): Promise<Types.ObjectId> {
    const { companyId } = await this.findOneOrFail({ _id: userId }, { projectColumns: ['companyId'] });
    const { _id: companyOwnerId } = await this.findOneOrFail(
      { companyId, role: Role.companyOwner },
      { projectColumns: ['_id'] },
    );

    return companyOwnerId;
  }
}
