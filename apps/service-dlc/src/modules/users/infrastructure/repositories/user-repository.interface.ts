import { Types } from 'mongoose';

export interface GetCompanyOwnerIdByUserIdParameters {
  userId: Types.ObjectId;
}
