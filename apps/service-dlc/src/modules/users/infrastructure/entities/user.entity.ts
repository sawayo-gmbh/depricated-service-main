import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Transform } from 'class-transformer';
import { Document, Types } from 'mongoose';

import { Role } from '@sawayo/auth';

export type UserDocument = UserEntity & Document;

@Schema({ collection: 'users', timestamps: true, versionKey: false })
export class UserEntity {
  @Transform(({ obj }) => obj._id, { toClassOnly: true })
  readonly _id!: Types.ObjectId;

  @Prop()
  companyId: Types.ObjectId;

  @Prop({ default: Role.companyOwner, type: String })
  role: Role;
}

export const UserSchema = SchemaFactory.createForClass(UserEntity);
