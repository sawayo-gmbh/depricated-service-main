import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { UserEntity, UserRepository, UserSchema } from './infrastructure';

@Module({
  controllers: [],
  exports: [UserRepository],
  imports: [MongooseModule.forFeature([{ name: UserEntity.name, schema: UserSchema }])],
  providers: [UserRepository],
})
export class UsersModule {}
