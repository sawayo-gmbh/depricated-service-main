import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { TimeModule } from '@sawayo/time';

import { DriverLicenseCheckModule } from '../driver-license-check';
import { DriverLicenseCheckSettingsModule } from '../driver-license-check-settings';
import { EventBusModule } from '../event-bus';

import { ShedulerService } from './application';

@Module({
  controllers: [],
  exports: [],
  imports: [
    ScheduleModule.forRoot(),
    DriverLicenseCheckModule,
    DriverLicenseCheckSettingsModule,
    EventBusModule,
    TimeModule,
  ],
  providers: [ShedulerService],
})
export class ShedulerModule {}
