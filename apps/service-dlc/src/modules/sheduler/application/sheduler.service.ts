import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

import { TimeService } from '@sawayo/time';

import { DriverLicenseCheckDomain } from '../../driver-license-check';
import { DriverLicenseCheckSettingsDomain } from '../../driver-license-check-settings';
import { PushNotificationsProvider } from '../../event-bus/infrastructure';

@Injectable()
export class ShedulerService {
  private readonly DRIVER_LICENSE_CHECK_DEADLINE_DAYS = 2;

  constructor(
    private readonly driverLicenseCheckDomain: DriverLicenseCheckDomain,
    private readonly driverLicenseCheckSettingsDomain: DriverLicenseCheckSettingsDomain,
    private readonly pushNotificationsProvider: PushNotificationsProvider,
    private readonly timeService: TimeService,
  ) {}

  @Cron('0 0 * * *')
  async dlcExpireCheck(): Promise<void> {
    await this.driverLicenseCheckDomain.driverLisenceExpireCheck();
  }

  @Cron('0 9 * * *')
  async nextCheckDateBeforeExpirationNotifications(): Promise<void> {
    const today = new Date();
    const deadline = this.timeService.addDays(today, -this.DRIVER_LICENSE_CHECK_DEADLINE_DAYS);
    const deadlineStart = this.timeService.startOfDay(deadline);
    const deadlineEnd = this.timeService.endOfDay(deadline);

    const driverLicenseCheckSettingsList =
      await this.driverLicenseCheckSettingsDomain.getDriverLicenseCheckSettingsInPeriod({
        endDate: deadlineEnd,
        startDate: deadlineStart,
      });

    await Promise.all(
      driverLicenseCheckSettingsList.map(async (driverLicenceCheckSettings) => {
        const { userId } = driverLicenceCheckSettings;

        await this.pushNotificationsProvider.sendBeforeExpirationPushNotification({ userId });
      }),
    );
  }
}
