import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';

import { AwsHealthIndicator, RabbitMQHealthIndicator } from '@sawayo/health';

import { HealthController } from './presentation';

@Module({
  controllers: [HealthController],
  imports: [TerminusModule],
  providers: [AwsHealthIndicator, RabbitMQHealthIndicator],
})
export class HealthModule {}
