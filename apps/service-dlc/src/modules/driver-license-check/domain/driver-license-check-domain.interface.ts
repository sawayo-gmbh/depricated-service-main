import { Types } from 'mongoose';

import { FileData } from '@sawayo/aws';

import { NotHoldingReason } from '../core/types';

export interface CreateDLCParameters {
  backside?: FileData;
  companyId: Types.ObjectId;
  declineReasone?: string;
  frontside?: FileData;
  information?: string;
  licenseExpires?: Date;
  notHoldingReasone?: NotHoldingReason;
  signature?: FileData;
  startDate?: Date;
  userId: Types.ObjectId;
}

export interface GetDLCByIdParameters {
  id: Types.ObjectId;
}

export interface GetDLCByUserIdParameters {
  userId: Types.ObjectId;
}

export interface GetDriverLicenseChecksByCompanyIdParameters {
  companyId: Types.ObjectId;
}

export interface GetHistoryParameters {
  userId: Types.ObjectId;
  limit: number;
  offset: number;
}

export interface RemoveDriverLicenseCheckImagesParameters {
  driverLicenseCheckId: Types.ObjectId;
  userId: Types.ObjectId;
}
