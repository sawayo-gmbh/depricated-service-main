import { Injectable } from '@nestjs/common';
import moment from 'moment';

import { ApprovalType } from '@sawayo/event-bus/services/approval';

import { ApprovalServiceProvider, WebSocketProvider } from '../../event-bus/infrastructure';
import { UserRepository } from '../../users';
import { DriverLicenseCheckRepository } from '../infrastructure/repositories';

import {
  CreateDLCParameters,
  GetDLCByIdParameters,
  GetDLCByUserIdParameters,
  GetDriverLicenseChecksByCompanyIdParameters,
  GetHistoryParameters,
  RemoveDriverLicenseCheckImagesParameters,
} from './driver-license-check-domain.interface';

@Injectable()
export class DriverLicenseCheckDomain {
  constructor(
    private readonly approvalServiceProvider: ApprovalServiceProvider,
    private readonly driverLicenseCheckRepository: DriverLicenseCheckRepository,
    private readonly webSocketProvider: WebSocketProvider,
    private readonly userRepository: UserRepository,
  ) {}

  async createDLC({
    backside,
    companyId,
    declineReasone,
    frontside,
    information,
    licenseExpires,
    notHoldingReasone,
    signature,
    startDate,
    userId,
  }: CreateDLCParameters) {
    const driverLicenseCheck = await this.driverLicenseCheckRepository.create({
      backside,
      companyId,
      declineReasone,
      expired: false,
      frontside,
      information,
      licenseExpires,
      notHoldingReasone,
      signature,
      startDate,
      userId,
    });

    await this.approvalServiceProvider.createApproval({
      companyId,
      entityId: driverLicenseCheck._id,
      type: ApprovalType.driverLicenseCheck,
      userId,
    });

    const companyOwnerId = await this.userRepository.getCompanyOwnerIdByUserId({ userId });

    await this.webSocketProvider.sendDriverLicenseCheckCreatedWebSocketMessage({
      driverLicenseCheck,
      userId: companyOwnerId,
    });

    return driverLicenseCheck;
  }

  async driverLisenceExpireCheck(): Promise<void> {
    await this.driverLicenseCheckRepository.updateMany(
      { licenseExpires: { $lte: moment(new Date()).add(1, 'day').toDate() } },
      { expired: true },
    );
  }

  async getDLCById({ id }: GetDLCByIdParameters) {
    return this.driverLicenseCheckRepository.findOneOrFail({ _id: id });
  }

  async getDLCByUserId({ userId }: GetDLCByUserIdParameters) {
    return this.driverLicenseCheckRepository.getDLCByUserId({ userId });
  }

  async getDriverLicenseChecksByCompanyId({ companyId }: GetDriverLicenseChecksByCompanyIdParameters) {
    return this.driverLicenseCheckRepository.getDriverLicenseChecksByCompanyId({ companyId });
  }

  async getHistory({ limit = 0, offset = 0, userId }: GetHistoryParameters) {
    return this.driverLicenseCheckRepository.getHistory({
      limit,
      offset,
      userId,
    });
  }

  async removeDriverLicenseCheckImages({ driverLicenseCheckId, userId }: RemoveDriverLicenseCheckImagesParameters) {
    return this.driverLicenseCheckRepository.removeDriverLicenseCheckImages({ _id: driverLicenseCheckId, userId });
  }
}
