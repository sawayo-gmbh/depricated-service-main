import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Repository } from '@sawayo/mongo';

import { DriverLicenseCheckDocument, DriverLicenseCheckEntity } from '../entities/driver-license.entity';

import {
  GetDriverLicenseChecksByCompanyIdParameters,
  GetHistoryParameters,
  RemoveDriverLicenseCheckImagesParameters,
} from './driver-license-check-repository.interface';

@Injectable()
export class DriverLicenseCheckRepository extends Repository<DriverLicenseCheckEntity, DriverLicenseCheckDocument> {
  constructor(
    @InjectModel(DriverLicenseCheckEntity.name) private driverLicenseCheckEntity: Model<DriverLicenseCheckDocument>,
  ) {
    super(driverLicenseCheckEntity, { baseClass: DriverLicenseCheckEntity });
  }

  async getDriverLicenseChecksByCompanyId({
    companyId,
  }: GetDriverLicenseChecksByCompanyIdParameters): Promise<DriverLicenseCheckEntity[]> {
    const pipeline = [
      { $match: { companyId } },
      { $sort: { createdAt: -1 } },
      { $group: { _id: '$userId', driverLicenseCheck: { $first: '$$ROOT' } } },
      { $replaceRoot: { newRoot: '$driverLicenseCheck' } },
    ];

    return this.aggregate(pipeline);
  }

  async getHistory({ limit, offset, userId }: GetHistoryParameters): Promise<DriverLicenseCheckEntity[]> {
    return this.driverLicenseCheckEntity.find({ userId }).sort({ createdAt: -1 }).skip(offset).limit(limit);
  }

  async getDLCByUserId({ userId }): Promise<DriverLicenseCheckEntity> {
    return this.driverLicenseCheckEntity.findOne({ userId }).sort({ createdAt: -1 });
  }

  async removeDriverLicenseCheckImages({ _id, userId }: RemoveDriverLicenseCheckImagesParameters) {
    return this.driverLicenseCheckEntity.updateOne({ _id, userId }, { $unset: { backside: null, frontside: null } });
  }
}
