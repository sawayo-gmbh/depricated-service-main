import { Types } from 'mongoose';

export interface GetDriverLicenseChecksByCompanyIdParameters {
  companyId: Types.ObjectId;
}

export interface GetHistoryParameters {
  limit?: number;
  offset?: number;
  userId: Types.ObjectId;
}

export interface RemoveDriverLicenseCheckImagesParameters {
  _id: Types.ObjectId;
  userId: Types.ObjectId;
}
