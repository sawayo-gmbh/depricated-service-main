import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Transform } from 'class-transformer';
import { Document, Types } from 'mongoose';

import { FileData } from '@sawayo/aws';

import { NotHoldingReason } from '../../core/types';

export type DriverLicenseCheckDocument = DriverLicenseCheckEntity & Document;

@Schema({ collection: 'driverLicenseCheck', timestamps: true, versionKey: false })
export class DriverLicenseCheckEntity {
  @Transform(({ obj }) => obj._id, { toClassOnly: true })
  readonly _id!: Types.ObjectId;

  createdAt: Date;

  @Prop({ type: Object })
  backside?: FileData;

  @Prop({ type: Types.ObjectId })
  companyId?: Types.ObjectId;

  @Prop()
  declineReasone?: string;

  @Prop({ type: Object })
  frontside?: FileData;

  @Prop()
  information?: string;

  @Prop()
  expired?: boolean;

  @Prop()
  licenseExpires?: Date;

  @Prop()
  notHoldingReasone?: NotHoldingReason;

  @Prop({ type: Object })
  signature?: FileData;

  @Prop()
  startDate?: Date;

  updatedAt: Date;

  @Prop({ type: Types.ObjectId })
  userId: Types.ObjectId;

  constructor(init?: Partial<DriverLicenseCheckEntity>) {
    Object.assign(this, init);
  }
}

export const DriverLicenseCheckSchema = SchemaFactory.createForClass(DriverLicenseCheckEntity);
