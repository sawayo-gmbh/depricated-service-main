import { Injectable } from '@nestjs/common';

import { DriverLicenseCheckSettingsDomain } from '../../driver-license-check-settings';
import { DriverLicenseCheckDomain } from '../domain';
import { DriverLicenseCheckDto } from '../presentation';

import {
  CreateDLCParameters,
  GetDLCByIdParameters,
  GetDLCByUserIdParameters,
  GetDriverLicenseChecksByCompanyIdParameters,
  GetHistoryParameters,
} from './driver-license-check-service.interface';

@Injectable()
export class DriverLicenceCheckService {
  constructor(
    private readonly driverLicenseCheckDomain: DriverLicenseCheckDomain,
    private readonly driverLicenseCheckSettingsDomain: DriverLicenseCheckSettingsDomain,
  ) {}

  async create({
    backside,
    companyId,
    declineReasone,
    frontside,
    information,
    licenseExpires,
    notHoldingReasone,
    signature,
    startDate,
    userId,
  }: CreateDLCParameters): Promise<DriverLicenseCheckDto> {
    const driverLicenseCheck = await this.driverLicenseCheckDomain.createDLC({
      backside,
      companyId,
      declineReasone,
      frontside,
      information,
      licenseExpires,
      notHoldingReasone,
      signature,
      startDate,
      userId,
    });

    await this.driverLicenseCheckSettingsDomain.shiftNextCheckByUserId({ userId });

    return driverLicenseCheck;
  }

  async getById({ id }: GetDLCByIdParameters): Promise<DriverLicenseCheckDto> {
    return this.driverLicenseCheckDomain.getDLCById({ id });
  }

  async getByUserId({ userId }: GetDLCByUserIdParameters): Promise<DriverLicenseCheckDto> {
    return this.driverLicenseCheckDomain.getDLCByUserId({ userId });
  }

  async getDriverLicenseChecksByCompanyId({
    companyId,
  }: GetDriverLicenseChecksByCompanyIdParameters): Promise<DriverLicenseCheckDto[]> {
    return this.driverLicenseCheckDomain.getDriverLicenseChecksByCompanyId({ companyId });
  }

  async getHistory({ limit, offset = 0, userId }: GetHistoryParameters): Promise<DriverLicenseCheckDto[]> {
    const driverLicenseCheckSettings =
      await this.driverLicenseCheckSettingsDomain.getDriverLicenseCheckSettingsByUserId({ userId });

    const historyOffset = driverLicenseCheckSettings?.enabled ? offset + 1 : offset;

    return this.driverLicenseCheckDomain.getHistory({ limit, offset: historyOffset, userId });
  }
}
