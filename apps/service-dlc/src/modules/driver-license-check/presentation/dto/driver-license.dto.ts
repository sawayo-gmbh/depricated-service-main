import { Directive, Field, ObjectType } from '@nestjs/graphql';
import { Types } from 'mongoose';

import { FileDataDto } from '@sawayo/graphql';

import { NotHoldingReason } from '../../core/types';

@ObjectType()
@Directive('@key(fields: "_id")')
export class DriverLicenseCheckDto {
  @Field(() => Types.ObjectId, { nullable: false })
  _id!: Types.ObjectId;

  @Field(() => FileDataDto, { nullable: true })
  backside?: FileDataDto;

  @Field(() => Types.ObjectId, { nullable: true })
  companyId?: Types.ObjectId;

  @Field(() => Date, { nullable: false })
  createdAt: Date;

  @Field(() => String, { nullable: true })
  declineReasone?: string;

  @Field(() => FileDataDto, { nullable: true })
  frontside?: FileDataDto;

  @Field(() => String, { nullable: true })
  information?: string;

  @Field(() => Boolean, { nullable: true })
  expired?: boolean;

  @Field(() => Date, { nullable: true })
  licenseExpires?: Date;

  @Field(() => NotHoldingReason, { nullable: true })
  notHoldingReasone?: NotHoldingReason;

  @Field(() => FileDataDto, { nullable: true })
  signature?: FileDataDto;

  @Field(() => Date, { nullable: true })
  startDate?: Date;

  @Field(() => Types.ObjectId, { nullable: false })
  userId: Types.ObjectId;
}
