import { InputType, Field } from '@nestjs/graphql';
import { IsEnum, IsOptional } from 'class-validator';

import { NotHoldingReason } from '../../core/types';

@InputType()
export class CreateDLCInput {
  @Field(() => String, { nullable: true })
  @IsOptional()
  declineReasone?: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  information?: string;

  @Field(() => Boolean, { nullable: true })
  @IsOptional()
  expired?: boolean;

  @Field(() => Date, { nullable: true })
  @IsOptional()
  licenseExpires?: Date;

  @Field(() => NotHoldingReason, { nullable: true })
  @IsOptional()
  @IsEnum(NotHoldingReason)
  notHoldingReasone?: NotHoldingReason;

  @Field(() => String, { nullable: true })
  signature?: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  startDate?: Date;
}
