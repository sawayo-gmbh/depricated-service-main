import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Parent, Query, ResolveField, Resolver, ResolveReference } from '@nestjs/graphql';
import { GraphQLUpload } from 'apollo-server-express';
import { Types } from 'mongoose';

import { CurrentUser, CurrentUserArg, GqlAuthGuard, Role, Roles } from '@sawayo/auth';
import { AwsService, FileData, MimeType } from '@sawayo/aws';
import { FileDataDto, FileUpload } from '@sawayo/graphql';

import { DriverLicenceCheckService } from '../application';
import { DriverLicenseCheckFederationReference } from '../core/types';

import { CreateDLCInput, DriverLicenseCheckDto } from './dto';

@Resolver(() => DriverLicenseCheckDto)
@UseGuards(GqlAuthGuard)
export class DriverLicenceCheckResolver {
  constructor(
    private readonly awsService: AwsService,
    private readonly driverLicenceCheckService: DriverLicenceCheckService,
  ) {}

  private async signUrl(fileData: FileData): Promise<FileData> {
    if (!fileData) {
      return null;
    }

    const { key } = fileData;

    const signedUrl = await this.awsService.getSignedUrl({ key });

    return {
      key,
      url: signedUrl,
    };
  }

  @Mutation(() => DriverLicenseCheckDto, { name: 'createDLC' })
  async createDLC(
    @CurrentUserArg() currentUser: CurrentUser,
    @Args('dlc') dlc: CreateDLCInput,
    @Args({ name: 'backside', nullable: true, type: () => GraphQLUpload }) backside?: FileUpload,
    @Args({ name: 'frontside', nullable: true, type: () => GraphQLUpload }) frontside?: FileUpload,
  ): Promise<DriverLicenseCheckDto> {
    const { companyId, userId } = currentUser;
    const { signature } = dlc;

    const backsideData =
      backside &&
      (await this.awsService.putFile({
        companyId,
        file: backside.createReadStream(),
        name: this.awsService.getDefaultNameFromFileName({ name: backside.filename, prefix: 'backside' }),
      }));
    const frontsideData =
      frontside &&
      (await this.awsService.putFile({
        companyId,
        file: frontside.createReadStream(),
        name: this.awsService.getDefaultNameFromFileName({ name: frontside.filename, prefix: 'frontside' }),
      }));
    const signatureData =
      signature &&
      (await this.awsService.putFile({
        companyId,
        contentType: MimeType.svg,
        extension: 'svg',
        file: Buffer.from(signature),
        namePrefix: 'signature',
      }));

    return this.driverLicenceCheckService.create({
      ...dlc,
      backside: backsideData,
      companyId,
      expired: false,
      frontside: frontsideData,
      signature: signatureData,
      userId,
    });
  }

  @Query(() => [DriverLicenseCheckDto], { name: 'driverLicenseChecksByCompany', nullable: false })
  @Roles(Role.companyOwner)
  async getDriverLicenseChecksByCompanyId(
    @CurrentUserArg() currentUser: CurrentUser,
  ): Promise<DriverLicenseCheckDto[]> {
    const { companyId } = currentUser;

    return this.driverLicenceCheckService.getDriverLicenseChecksByCompanyId({
      companyId,
    });
  }

  @Query(() => DriverLicenseCheckDto, { name: 'getDLCById', nullable: true })
  async getDLCById(@Args('id') id: string): Promise<DriverLicenseCheckDto> {
    return this.driverLicenceCheckService.getById({ id: Types.ObjectId(id) });
  }

  @Query(() => DriverLicenseCheckDto, { name: 'getDlcByUserId', nullable: true })
  async getDLCbyUserId(@Args('userId') userId: string): Promise<DriverLicenseCheckDto> {
    return this.driverLicenceCheckService.getByUserId({ userId: Types.ObjectId(userId) });
  }

  @Query(() => [DriverLicenseCheckDto], { name: 'getHistory' })
  async getHistory(
    @Args('userId') userId: string,
    @Args({ name: 'offset', nullable: true }) offset: number,
    @Args({ name: 'limit', nullable: true }) limit: number,
  ): Promise<DriverLicenseCheckDto[]> {
    return this.driverLicenceCheckService.getHistory({ limit, offset, userId: Types.ObjectId(userId) });
  }

  @ResolveField(() => FileDataDto, { name: 'backside', nullable: true })
  async signBacksideUrl(@Parent() driverLicenseCheck: DriverLicenseCheckDto): Promise<FileDataDto> {
    const { backside } = driverLicenseCheck;

    return this.signUrl(backside);
  }

  @ResolveField(() => FileDataDto, { name: 'frontside', nullable: true })
  async signFrontsideUrl(@Parent() driverLicenseCheck: DriverLicenseCheckDto): Promise<FileDataDto> {
    const { frontside } = driverLicenseCheck;

    return this.signUrl(frontside);
  }

  @ResolveField(() => FileDataDto, { name: 'signature', nullable: true })
  async signSignatureUrl(@Parent() driverLicenseCheck: DriverLicenseCheckDto): Promise<FileDataDto> {
    const { signature } = driverLicenseCheck;

    return this.signUrl(signature);
  }

  @ResolveReference()
  async resolveReference({
    _id: driverLicenseCheckId,
  }: DriverLicenseCheckFederationReference): Promise<DriverLicenseCheckDto> {
    return this.driverLicenceCheckService.getById({ id: Types.ObjectId(driverLicenseCheckId) });
  }
}
