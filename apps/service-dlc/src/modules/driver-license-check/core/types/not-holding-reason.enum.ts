export enum NotHoldingReason {
  dontHaveLicense = 'dontHaveLicense',
  licenseExpired = 'licenseExpired',
  licenseImpounded = 'licenseImpounded',
  lostLicense = 'lostLicense',
  otherReason = 'otherReason',
}
