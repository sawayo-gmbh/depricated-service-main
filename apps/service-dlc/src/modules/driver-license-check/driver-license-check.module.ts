import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { DriverLicenseCheckSettingsModule } from '../driver-license-check-settings';
import { EventBusModule } from '../event-bus';
import { UsersModule } from '../users';

import { DriverLicenceCheckService } from './application';
import { DriverLicenseCheckDomain } from './domain';
import { DriverLicenseCheckEntity, DriverLicenseCheckRepository, DriverLicenseCheckSchema } from './infrastructure';
import { DriverLicenceCheckResolver } from './presentation';

@Module({
  controllers: [],
  exports: [DriverLicenseCheckDomain],
  imports: [
    MongooseModule.forFeature([{ name: DriverLicenseCheckEntity.name, schema: DriverLicenseCheckSchema }]),
    DriverLicenseCheckSettingsModule,
    UsersModule,
    forwardRef(() => EventBusModule),
  ],
  providers: [
    DriverLicenceCheckResolver,
    DriverLicenceCheckService,
    DriverLicenseCheckDomain,
    DriverLicenseCheckRepository,
  ],
})
export class DriverLicenseCheckModule {}
