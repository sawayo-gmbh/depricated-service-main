import { Injectable } from '@nestjs/common';
import { HealthCheckError, HealthIndicatorResult, HealthIndicator } from '@nestjs/terminus';

import { AwsService } from '@sawayo/aws';

@Injectable()
export class AwsHealthIndicator extends HealthIndicator {
  constructor(private readonly awsService: AwsService) {
    super();
  }

  async hasPermissions(key: string): Promise<HealthIndicatorResult> {
    try {
      await this.awsService.checkBucketPermissions();

      return this.getStatus(key, true);
    } catch (error) {
      const result = this.getStatus(key, false);

      throw new HealthCheckError('AWS check failed', result);
    }
  }
}
