import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { HealthCheckError, HealthIndicatorResult, HealthIndicator } from '@nestjs/terminus';

@Injectable()
export class RabbitMQHealthIndicator extends HealthIndicator {
  constructor(private readonly amqpConnection: AmqpConnection) {
    super();
  }

  async isConnected(key: string): Promise<HealthIndicatorResult> {
    const isConnected = this.amqpConnection.managedConnection.isConnected();

    const result = this.getStatus(key, isConnected);

    if (isConnected) {
      return result;
    }

    throw new HealthCheckError('RabbitMQ check failed', result);
  }
}
