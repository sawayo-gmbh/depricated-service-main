import { Module } from '@nestjs/common';

import { TimeService } from './application';

@Module({
  controllers: [],
  exports: [TimeService],
  imports: [],
  providers: [TimeService],
})
export class TimeModule {}
