export interface RangesActionParameters {
  fromDateFirst: Date;
  fromDateSecond: Date;
  toDateFirst: Date;
  toDateSecond: Date;
}
