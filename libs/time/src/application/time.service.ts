import { Injectable } from '@nestjs/common';
import * as Moment from 'moment';
import moment, { DurationInputArg1, DurationInputArg2 } from 'moment';
import { extendMoment } from 'moment-range';

import { DateRange, IsDateInRangeInterface, TimeRange } from '../core/types';

import { RangesActionParameters } from './time-service.interface';

const momentRange = extendMoment(Moment);

@Injectable()
export class TimeService {
  private readonly minutesForSplittingOneDay = 1;

  addDays(date: Date, days: number): Date {
    return moment(date).add(days, 'day').toDate();
  }

  currentYear() {
    return moment().year();
  }

  addMinutes(date: Date, minutes: number): Date {
    return moment(date).add(minutes, 'minutes').toDate();
  }

  subtractMinutes(date: Date, minutes: number): Date {
    return moment(date).subtract(minutes, 'minutes').toDate();
  }

  addMonths(date: Date, months: number): Date {
    return moment(date).add(months, 'month').toDate();
  }

  addWeeks(date: Date, weeks: number): Date {
    return moment(date).add(weeks, 'week').toDate();
  }

  getISOWeekDay(date: Date) {
    return moment(date).get('isoWeekday');
  }

  differenceInDays(first: Date, second: Date): number {
    return moment(first).diff(second, 'days');
  }

  endOfDay(date: Date): Date {
    return moment(date).utc().endOf('day').toDate();
  }

  lastDayOfPrevMonth() {
    return moment().utc().subtract(1, 'months').endOf('month').toDate();
  }

  firstDayOfPrevMonth() {
    return moment().utc().subtract(1, 'months').startOf('month').toDate();
  }

  now(): Date {
    return moment().utc().seconds(0).milliseconds(0).toDate();
  }

  splitBy(from: Date, to: Date, unit: moment.unitOfTime.Diff): TimeRange[] {
    const fromUtc = moment(from).utc();
    const toUtc = moment(to).utc();

    if (this.isSame(from, to)) {
      return [
        {
          fromTime: fromUtc.startOf(unit).toDate(),
          toTime: toUtc.startOf(unit).toDate(),
        },
      ];
    }

    const range = momentRange.range(fromUtc, toUtc);

    const dates = Array.from(range.by(unit)).map((date, index) => {
      if (!index) {
        return date;
      }

      return date.utc().startOf(unit);
    });

    dates.push(toUtc);

    return dates
      .reduce((accumulator, date, index, array) => {
        if (index === array.length - 1) {
          return accumulator;
        }

        const nextDate = dates[index + 1];

        if (!date.isSame(nextDate, unit)) {
          return [
            ...accumulator,
            {
              fromTime: moment(date).utc().toDate(),
              toTime: moment(date).utc().endOf(unit).toDate(),
            },
            {
              fromTime: moment(nextDate).utc().startOf(unit).toDate(),
              toTime: moment(nextDate).utc().toDate(),
            },
          ];
        }

        return [
          ...accumulator,
          {
            fromTime: date.toDate(),
            toTime: nextDate.toDate(),
          },
        ];
      }, [])
      .filter((rawRange) => {
        const { fromTime, toTime } = rawRange;

        return !moment(fromTime).isSame(toTime);
      });
  }

  startOfDay(date: Date): Date {
    return moment(date).utc().startOf('day').toDate();
  }

  isSame(first: Date, second: Date): boolean {
    return moment(first).isSame(second);
  }

  isSameDay(first: Date, second: Date): boolean {
    const day1 = moment(first).utc().startOf('day');
    const day2 = moment(second).utc().startOf('day');

    return day1.isSame(day2);
  }

  isSameYear(first: Date, second: Date): boolean {
    return moment(first).isSame(second, 'year');
  }

  difference(first: Date, second: Date, diff?: moment.unitOfTime.Diff): number {
    return moment(first).diff(second, diff || 'minutes');
  }

  isSafeMaxYear(date: Date): boolean {
    return moment(date).year() < 275760; //https://262.ecma-international.org/11.0/#sec-year-number
  }

  isAfter(first: Date, second: Date): boolean {
    return moment(first).isAfter(second);
  }

  isBefore(first: Date, second: Date): boolean {
    return moment(first).isBefore(second);
  }

  isSameOrBefore(first: Date, second: Date): boolean {
    return moment(first).isSameOrBefore(second);
  }

  endOfMonth(date: Date): Date {
    return moment(date).utc().endOf('month').toDate();
  }

  startOfMonth(date: Date): Date {
    return moment(date).utc().startOf('month').toDate();
  }

  endOfYear(date: Date): Date {
    return moment(date).utc().endOf('year').toDate();
  }

  endOfYearFromNumber(year: number): Date {
    return moment().utc().year(year).endOf('year').toDate();
  }

  startOfYear(date: Date): Date {
    return moment(date).utc().startOf('year').toDate();
  }

  startOfYearFromNumber(year: number): Date {
    return moment().utc().year(year).startOf('year').toDate();
  }

  isInFuture(date: Date): boolean {
    return moment().diff(moment(date)) < 0;
  }

  isValid(date: Date): boolean {
    return moment(date).isValid();
  }

  validateDate(date: Date): Date {
    if (!this.isValid(date)) {
      return null;
    }

    return date;
  }

  isDateInRange({ dateInRange, fromDate, toDate }: IsDateInRangeInterface) {
    return moment(dateInRange).isSameOrAfter(fromDate) && moment(dateInRange).isSameOrBefore(toDate);
  }

  formatDate(date: Date, format: string, utcOffset?: number) {
    if (utcOffset) {
      return moment(date).utcOffset(utcOffset).format(format);
    }

    return moment(date).format(format);
  }

  formatDateWithLocale(date: Date, locale: string, format: string) {
    return moment(date).locale(locale).format(format);
  }

  durationInFormat(value: DurationInputArg1, duration: DurationInputArg2) {
    return moment.duration(value, duration);
  }

  rangeHasOverlaps({ fromDateFirst, fromDateSecond, toDateFirst, toDateSecond }: RangesActionParameters): boolean {
    const rangeFirst = momentRange.range(fromDateFirst, toDateFirst);
    const rangeSecond = momentRange.range(fromDateSecond, toDateSecond);

    return rangeFirst.overlaps(rangeSecond);
  }

  rangeIntersection({ fromDateFirst, fromDateSecond, toDateFirst, toDateSecond }: RangesActionParameters): DateRange {
    const rangeFirst = momentRange.range(fromDateFirst, toDateFirst);
    const rangeSecond = momentRange.range(fromDateSecond, toDateSecond);

    const intersection = rangeFirst.intersect(rangeSecond);

    if (!intersection) {
      return null;
    }

    const { end, start } = intersection;

    return {
      fromDate: start.utc().toDate(),
      toDate: end.utc().toDate(),
    };
  }

  rangeSubtract({ fromDateFirst, fromDateSecond, toDateFirst, toDateSecond }: RangesActionParameters): DateRange[] {
    const rangeFirst = momentRange.range(fromDateFirst, toDateFirst);
    const rangeSecond = momentRange.range(fromDateSecond, toDateSecond);

    const result = rangeFirst.subtract(rangeSecond);

    return result.map((range) => {
      const { end, start } = range;

      return {
        fromDate: start.utc().toDate(),
        toDate: end.utc().toDate(),
      };
    });
  }

  today(): Date {
    return moment().utc().startOf('day').toDate();
  }

  setIsoWeekDay(date: Date, isoWeekDay: number): Date {
    return moment(date).isoWeekday(isoWeekDay).toDate();
  }

  startOfMinute(date: Date): Date {
    return moment(date).startOf('minute').toDate();
  }
}
