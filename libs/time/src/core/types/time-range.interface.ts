export interface TimeRange {
  fromTime: Date;
  toTime: Date;
}

export interface IsDateInRangeInterface {
  dateInRange: Date;
  toDate: Date;
  fromDate: Date;
}
