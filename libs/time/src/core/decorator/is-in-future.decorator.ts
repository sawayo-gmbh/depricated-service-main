import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';
import moment from 'moment';

export const IsDateInFuture = (validationOptions?: ValidationOptions) => {
  return (object: unknown, propertyName: string): void => {
    registerDecorator({
      name: 'IsInFuture',
      target: object.constructor,
      propertyName,
      options: validationOptions,
      validator: {
        validate(value: any): boolean {
          if (value instanceof Date) {
            return moment().diff(moment(value)) < 0;
          }

          return true;
        },
        defaultMessage(args: ValidationArguments): string {
          return `${args.property} should be in future`;
        },
      },
    });
  };
};
