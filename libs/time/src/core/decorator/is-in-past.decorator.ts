import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';
import moment from 'moment';

export const IsDateInPast = (validationOptions?: ValidationOptions) => {
  return (object: unknown, propertyName: string): void => {
    registerDecorator({
      name: 'IsInPast',
      target: object.constructor,
      propertyName,
      options: validationOptions,
      validator: {
        validate(value: any): boolean {
          if (value instanceof Date) {
            return moment().isAfter(moment(value));
          }

          return true;
        },
        defaultMessage(args: ValidationArguments): string {
          return `${args.property} should be in past`;
        },
      },
    });
  };
};
