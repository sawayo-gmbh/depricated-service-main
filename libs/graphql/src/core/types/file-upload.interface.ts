import { ReadStream } from 'fs';

export interface FileUpload {
  createReadStream(): ReadStream;
  encoding: string;
  filename: string;
  mimetype: string;
}
