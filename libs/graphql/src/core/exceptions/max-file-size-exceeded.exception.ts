import { HttpStatus } from '@nestjs/common';

import { BaseException } from '@sawayo/exceptions';

export class MaxFileSizeExceededException extends BaseException {
  constructor() {
    super('maxFileSizeExceeded', HttpStatus.BAD_REQUEST);
  }
}
