import { HttpStatus } from '@nestjs/common';

import { BaseException } from '@sawayo/exceptions';

export class MimeTypeIsNotAllowedException extends BaseException {
  constructor() {
    super('mimeTypeIsNotAllowed', HttpStatus.BAD_REQUEST);
  }
}
