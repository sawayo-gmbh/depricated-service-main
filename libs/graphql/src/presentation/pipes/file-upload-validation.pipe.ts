import { Injectable, PipeTransform } from '@nestjs/common';

import { convertReadStreamToBuffer } from '@sawayo/aws';

import { MaxFileSizeExceededException, MimeTypeIsNotAllowedException } from '../../core/exceptions';
import { FileUpload } from '../../core/types';

import { FileUploadValidationPipeOptions } from './file-upload-validation-pipe.interface';

@Injectable()
export class FileUploadValidationPipe implements PipeTransform {
  private readonly allowedMimeTypes: string[];
  private readonly maxFileSize: number;

  constructor(options?: FileUploadValidationPipeOptions) {
    this.allowedMimeTypes = options?.allowedMimeTypes;
    this.maxFileSize = options?.maxFileSize;
  }

  private async validateFileLength(value: FileUpload): Promise<void> {
    if (!this.maxFileSize) {
      return;
    }

    const readStream = value.createReadStream();
    const buffer = await convertReadStreamToBuffer(readStream);

    if (buffer.byteLength > this.maxFileSize) {
      throw new MaxFileSizeExceededException();
    }
  }

  private validateMimeType(value: FileUpload): void {
    if (!this.allowedMimeTypes) {
      return;
    }

    const { mimetype } = value;

    if (!this.allowedMimeTypes.includes(mimetype)) {
      throw new MimeTypeIsNotAllowedException();
    }
  }

  async transform(value: FileUpload) {
    if (!value) {
      return value;
    }

    this.validateMimeType(value);

    await this.validateFileLength(value);

    return value;
  }
}
