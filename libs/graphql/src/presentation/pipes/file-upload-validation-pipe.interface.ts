export interface FileUploadValidationPipeOptions {
  allowedMimeTypes?: string[];
  maxFileSize?: number;
}
