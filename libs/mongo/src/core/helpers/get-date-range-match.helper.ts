interface GetDateRangeMatchParameters {
  fromDate: Date;
  fromDateField: string;
  toDate: Date;
  toDateField: string;
}

export const getDateRangeMatch = ({ fromDate, fromDateField, toDate, toDateField }: GetDateRangeMatchParameters) => {
  return {
    $or: [
      { [fromDateField]: { $gte: fromDate, $lte: toDate } },
      { [toDateField]: { $gte: fromDate, $lte: toDate } },
      {
        [fromDateField]: { $lte: fromDate },
        [toDateField]: { $gte: toDate },
      },
    ],
  };
};

export const getDateRangeMatchWithoutEquals = ({
  fromDate,
  fromDateField,
  toDate,
  toDateField,
}: GetDateRangeMatchParameters) => {
  return {
    $or: [
      { [fromDateField]: { $gt: fromDate, $lt: toDate } },
      { [toDateField]: { $gt: fromDate, $lt: toDate } },
      {
        [fromDateField]: { $lt: fromDate },
        [toDateField]: { $gt: toDate },
      },
    ],
  };
};
