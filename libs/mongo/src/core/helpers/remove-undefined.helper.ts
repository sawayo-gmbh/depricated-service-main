export const removeUndefined = (data: Record<string, any>): Record<string, any> => {
  return Object.entries(data).reduce((accumulator: Record<string, any>, entry: any[]) => {
    const [key, value] = entry;

    if (value === undefined) {
      return accumulator;
    }

    return {
      ...accumulator,
      [key]: value,
    };
  }, {});
};
