import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { DeletedEntitiesRepository, DeletedEntity, DeletedEntitySchema } from './infrastructure';

@Module({
  exports: [DeletedEntitiesRepository],
  imports: [MongooseModule.forFeature([{ name: DeletedEntity.name, schema: DeletedEntitySchema }])],
  providers: [DeletedEntitiesRepository],
})
export class DeletedEntitiesModule {}
