import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Repository } from '../../../../core';
import { DeletedEntity, DeletedEntityDocument } from '../entities';

import { InsertDeletedEntityParameters } from './deleted-entities-repository.interface';

@Injectable()
export class DeletedEntitiesRepository extends Repository<DeletedEntity, DeletedEntityDocument> {
  constructor(
    @InjectModel(DeletedEntity.name)
    private deletedEntityModel: Model<DeletedEntityDocument>,
  ) {
    super(deletedEntityModel, { baseClass: DeletedEntity });
  }

  async insertDeletedEntity({ collectionName, entity, entityId }: InsertDeletedEntityParameters): Promise<void> {
    await this.create({
      collectionName,
      entity,
      entityId,
    });
  }
}
