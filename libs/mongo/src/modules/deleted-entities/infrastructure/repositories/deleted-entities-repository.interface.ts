import { Types } from 'mongoose';

import { CollectionName } from '../../core';

export interface InsertDeletedEntityParameters {
  collectionName: CollectionName;
  entity: unknown;
  entityId: Types.ObjectId;
}
