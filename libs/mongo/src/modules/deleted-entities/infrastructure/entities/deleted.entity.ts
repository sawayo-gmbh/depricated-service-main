import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

import { CollectionName } from '../../core';

@Schema({
  collection: 'deletedEntities',
  timestamps: true,
  versionKey: false,
})
export class DeletedEntity {
  _id: Types.ObjectId;

  @Prop({ enum: CollectionName, type: String })
  collectionName: CollectionName;

  @Prop({ type: Object })
  entity: any;

  @Prop({ type: Types.ObjectId })
  entityId: Types.ObjectId;

  createdAt: Date;
}

export type DeletedEntityDocument = DeletedEntity & Document;

export const DeletedEntitySchema = SchemaFactory.createForClass(DeletedEntity);

DeletedEntitySchema.index({ collectionName: 1, entityId: 1 }, { unique: true });
