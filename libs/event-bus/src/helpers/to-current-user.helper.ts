import { Types } from 'mongoose';

import { CurrentUser, CurrentUserRaw } from '@sawayo/auth';

export const toCurrentUser = (currentUserRaw: CurrentUserRaw): CurrentUser => {
  const { companyId, userId } = currentUserRaw;

  return {
    ...currentUserRaw,
    companyId: Types.ObjectId(companyId),
    userId: Types.ObjectId(userId),
  };
};
