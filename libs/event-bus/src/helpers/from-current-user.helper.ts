import { CurrentUser, CurrentUserRaw } from '@sawayo/auth';

export const fromCurrentUser = (currentUser: CurrentUser): CurrentUserRaw => {
  const { companyId, userId } = currentUser;

  return {
    ...currentUser,
    companyId: companyId.toHexString(),
    userId: userId.toHexString(),
  };
};
