import { Types } from 'mongoose';

export enum WebSocketNotificationType {
  test = 'test',
}

export interface WebSocketNotificationEvent {
  message: string;
  payload: unknown;
  subject: string;
  type: WebSocketNotificationType;
  userId: Types.ObjectId;
}
