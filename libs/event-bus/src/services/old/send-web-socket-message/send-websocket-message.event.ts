import { WebSocketMessageType } from '../websocket-message-type.enum';

export interface SendWebSocketMessageEvent {
  message: unknown;
  type: WebSocketMessageType;
  userId: string;
}
