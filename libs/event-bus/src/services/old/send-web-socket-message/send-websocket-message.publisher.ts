import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

import { Exchange, RoutingKey } from '@sawayo/event-bus/common';

import { AmqpPublisher } from '../../../common';

import { SendWebSocketMessageEvent } from './send-websocket-message.event';

export class SendWebSocketMessagePublisher extends AmqpPublisher<SendWebSocketMessageEvent> {
  constructor(readonly amqpConnection: AmqpConnection) {
    super(amqpConnection);
  }

  publish({ message, type, userId }: SendWebSocketMessageEvent): Promise<void> {
    const event: SendWebSocketMessageEvent = {
      message,
      type,
      userId,
    };

    return this.amqpConnection.publish(Exchange.direct, RoutingKey.oldWebSocketMessage, event);
  }
}
