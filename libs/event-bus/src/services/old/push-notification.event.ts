import { Types } from 'mongoose';

export interface Notification {
  body: string;
  title: string;
}

export interface PushNotificationEvent {
  notification: Notification;
  userId: Types.ObjectId;
}
