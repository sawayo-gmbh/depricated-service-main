export enum WebSocketMessageType {
  approved = 'approved',
  driverLicenseCheckCreated = 'driverLicenseCheck.created',
  driverLicenseCheckSettingsUpdated = 'driverLicenseCheckSettings.updated',
  rejected = 'rejected',
  timerCreated = 'timer.created',
  timerDeleted = 'timer.deleted',
  timerUpdated = 'timer.updated',
}
