import { WeekDay } from './week-day.enum';

export interface WorkTimeTemplateDuration {
  durationMinutes: number;
  fromTimeMinutes?: number;
  isActive: boolean;
  toTimeMinutes?: number;
  weekDay: WeekDay;
}
