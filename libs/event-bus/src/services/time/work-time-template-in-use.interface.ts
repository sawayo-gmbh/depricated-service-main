import { Types } from 'mongoose';

import { WorkTimeTemplateDuration, Overtime } from './';

export interface WorkTimeTemplateInUse {
  _id: Types.ObjectId;
  durations: WorkTimeTemplateDuration[];
  fromDate: Date;
  toDate: Date;
  name: string;
  overtime: Overtime;
}
