import { CurrentUserRaw } from '@sawayo/auth';

export interface GetWorkTimeTemplatesUsedInRangeRequest {
  currentUser: CurrentUserRaw;
  employeeId: string;
  fromDate: string;
  toDate: string;
}
