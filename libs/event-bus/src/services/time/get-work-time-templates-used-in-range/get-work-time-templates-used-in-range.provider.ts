import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

import { Exchange, RoutingKey } from '@sawayo/event-bus/common';

import { AmqpProvider } from '../../../common/amqp.provider';

import { GetWorkTimeTemplatesUsedInRangeRequest } from './get-work-time-templates-used-in-range.request';
import { GetWorkTimeTemplatesUsedInRangeResponse } from './get-work-time-templates-used-in-range.response';

export class GetWorkTimeTemplatesUsedInRangeProvider extends AmqpProvider<
  GetWorkTimeTemplatesUsedInRangeRequest,
  GetWorkTimeTemplatesUsedInRangeResponse
> {
  constructor(readonly amqpConnection: AmqpConnection) {
    super(amqpConnection);
  }

  request({
    currentUser,
    employeeId,
    fromDate,
    toDate,
  }: GetWorkTimeTemplatesUsedInRangeRequest): Promise<GetWorkTimeTemplatesUsedInRangeResponse> {
    const payload: GetWorkTimeTemplatesUsedInRangeRequest = {
      currentUser,
      employeeId,
      fromDate,
      toDate,
    };

    return this.amqpConnection.request<GetWorkTimeTemplatesUsedInRangeResponse>({
      exchange: Exchange.direct,
      payload,
      routingKey: RoutingKey.workTimeTemplatesUsedInRange,
    });
  }
}
