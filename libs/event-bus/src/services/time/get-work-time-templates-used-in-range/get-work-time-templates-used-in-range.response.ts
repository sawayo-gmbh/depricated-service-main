import { WorkTimeTemplateInUseRaw } from '../work-time-template-in-use-raw.interface';

export interface GetWorkTimeTemplatesUsedInRangeResponse {
  workTimeTemplatesUsedInRange: WorkTimeTemplateInUseRaw[];
}
