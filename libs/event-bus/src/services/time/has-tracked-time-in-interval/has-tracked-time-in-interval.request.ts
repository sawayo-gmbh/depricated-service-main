import { CurrentUserRaw } from '@sawayo/auth';

export interface HasTrackedTimeInIntervalRequest {
  currentUser: CurrentUserRaw;
  employeeId: string;
  fromDate: string;
  toDate: string;
}
