export interface HasTrackedTimeInIntervalResponse {
  hasTrackedTimeInInterval: boolean;
}
