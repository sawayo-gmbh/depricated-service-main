import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

import { Exchange, RoutingKey } from '@sawayo/event-bus/common';

import { AmqpProvider } from '../../../common/amqp.provider';

import { HasTrackedTimeInIntervalRequest } from './has-tracked-time-in-interval.request';
import { HasTrackedTimeInIntervalResponse } from './has-tracked-time-in-interval.response';

export class HasTrackedTimeInIntervalProvider extends AmqpProvider<
  HasTrackedTimeInIntervalRequest,
  HasTrackedTimeInIntervalResponse
> {
  constructor(readonly amqpConnection: AmqpConnection) {
    super(amqpConnection);
  }

  request({
    currentUser,
    employeeId,
    fromDate,
    toDate,
  }: HasTrackedTimeInIntervalRequest): Promise<HasTrackedTimeInIntervalResponse> {
    const payload: HasTrackedTimeInIntervalRequest = {
      currentUser,
      employeeId,
      fromDate,
      toDate,
    };

    return this.amqpConnection.request<HasTrackedTimeInIntervalResponse>({
      exchange: Exchange.direct,
      payload,
      routingKey: RoutingKey.hasTrackedTimeInInterval,
    });
  }
}
