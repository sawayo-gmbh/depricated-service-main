export interface IsIntervalClosedResponse {
  isIntervalClosed: boolean;
}
