import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

import { Exchange, RoutingKey } from '@sawayo/event-bus/common';

import { AmqpProvider } from '../../../common/amqp.provider';

import { IsIntervalClosedRequest } from './is-interval-closed.request';
import { IsIntervalClosedResponse } from './is-interval-closed.response';

export class IsIntervalClosedProvider extends AmqpProvider<IsIntervalClosedRequest, IsIntervalClosedResponse> {
  constructor(readonly amqpConnection: AmqpConnection) {
    super(amqpConnection);
  }

  request({ currentUser, employeeId, fromDate, toDate }: IsIntervalClosedRequest): Promise<IsIntervalClosedResponse> {
    const payload: IsIntervalClosedRequest = {
      currentUser,
      employeeId,
      fromDate,
      toDate,
    };

    return this.amqpConnection.request<IsIntervalClosedResponse>({
      exchange: Exchange.direct,
      payload,
      routingKey: RoutingKey.isIntervalClosed,
    });
  }
}
