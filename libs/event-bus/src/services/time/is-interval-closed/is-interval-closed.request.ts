import { CurrentUserRaw } from '@sawayo/auth';

export interface IsIntervalClosedRequest {
  currentUser: CurrentUserRaw;
  employeeId: string;
  fromDate: string;
  toDate: string;
}
