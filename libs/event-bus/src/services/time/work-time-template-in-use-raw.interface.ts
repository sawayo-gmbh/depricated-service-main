import { WorkTimeTemplateDuration, Overtime } from './';

export interface WorkTimeTemplateInUseRaw {
  _id: string;
  durations: WorkTimeTemplateDuration[];
  fromDate: string;
  toDate: string;
  name: string;
  overtime: Overtime;
}
