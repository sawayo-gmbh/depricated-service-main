export enum HalfDayPeriod {
  firstHalf = 1,
  secondHalf = 2,
}
