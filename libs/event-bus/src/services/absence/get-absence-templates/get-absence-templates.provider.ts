import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

import { Exchange, RoutingKey } from '@sawayo/event-bus/common';

import { AmqpProvider } from '../../../common/amqp.provider';

import { GetAbsenceTemplatesRequest } from './get-absence-templates.request';
import { GetAbsenceTemplatesResponse } from './get-absence-templates.response';

export class GetAbsenceTemplatesProvider extends AmqpProvider<GetAbsenceTemplatesRequest, GetAbsenceTemplatesResponse> {
  constructor(readonly amqpConnection: AmqpConnection) {
    super(amqpConnection);
  }

  request({ currentUser }: GetAbsenceTemplatesRequest): Promise<GetAbsenceTemplatesResponse> {
    const payload: GetAbsenceTemplatesRequest = {
      currentUser,
    };

    return this.amqpConnection.request<GetAbsenceTemplatesResponse>({
      exchange: Exchange.direct,
      payload,
      routingKey: RoutingKey.getAbsencesTemplates,
    });
  }
}
