import { AbsenceTemplate } from '../get-absences.response';

export interface GetAbsenceTemplatesResponse {
  readonly _id: string;
  companyId: string;
  templates: AbsenceTemplate[];
}
