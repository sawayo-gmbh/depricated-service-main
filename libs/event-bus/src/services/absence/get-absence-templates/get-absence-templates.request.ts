import { CurrentUserRaw } from '@sawayo/auth';

export interface GetAbsenceTemplatesRequest {
  currentUser: CurrentUserRaw;
}
