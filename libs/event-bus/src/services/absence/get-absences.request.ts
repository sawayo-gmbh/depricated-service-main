export interface GetAbsencesRequest {
  employeeId: string;
  fromDate: string;
  toDate: string;
}
