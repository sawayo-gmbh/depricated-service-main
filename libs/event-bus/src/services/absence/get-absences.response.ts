import { FileData } from '@sawayo/aws';

import { AbsenceTemplateType } from '../../core/enums';

import { HalfDayPeriod } from './halfday-period.enum';

export interface GetAbsencesResponse {
  _id: string;
  fromDate: string;
  toDate: string;
  halfDayPeriod?: HalfDayPeriod;
  files?: FileData[];
  deleted?: string;
  comment?: string;
  template: AbsenceTemplate;
  templateId: string;
}

export interface AbsenceTemplate {
  _id: string;
  name: string;
  timesheetKey: string;
  icon?: string;
  color?: string;
  isHoursAllowed: boolean;
  isHalfDayAllowed: boolean;
  isApprovalNeeded: boolean;
  isVisible: boolean;
  isPaid: boolean;
  proof?: AbsenceTemplateProof;
  contingency?: Contingency;
  type: AbsenceTemplateType;
}

export interface AbsenceTemplateProof {
  afterDays: number;
}

export interface Contingency {
  kind: ContingencyKind;
  carryOver?: ContingencyCarryOverPolicy;
}

export interface ContingencyCarryOverPolicy {
  limitDate: Date;
}

enum ContingencyKind {
  none = 'none',
  reduce = 'reduce',
  buildUp = 'buildUp',
}
