import { HalfDayPeriod } from '../halfday-period.enum';

export interface HasAbsenceInIntervalResponse {
  fromDate: string;
  halfDayPeriod: HalfDayPeriod;
  hasAbsenceInInterval: boolean;
  toDate: string;
}
