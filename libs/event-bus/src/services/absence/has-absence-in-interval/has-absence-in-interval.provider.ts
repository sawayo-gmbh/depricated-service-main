import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

import { Exchange, RoutingKey } from '@sawayo/event-bus/common';

import { AmqpProvider } from '../../../common/amqp.provider';

import { HasAbsenceInIntervalRequest } from './has-absence-in-interval.request';
import { HasAbsenceInIntervalResponse } from './has-absence-in-interval.response';

export class HasAbsenceInIntervalProvider extends AmqpProvider<
  HasAbsenceInIntervalRequest,
  HasAbsenceInIntervalResponse
> {
  constructor(readonly amqpConnection: AmqpConnection) {
    super(amqpConnection);
  }

  request({
    currentUser,
    employeeId,
    fromDate,
    toDate,
  }: HasAbsenceInIntervalRequest): Promise<HasAbsenceInIntervalResponse> {
    const payload: HasAbsenceInIntervalRequest = {
      currentUser,
      employeeId,
      fromDate,
      toDate,
    };

    return this.amqpConnection.request<HasAbsenceInIntervalResponse>({
      exchange: Exchange.direct,
      payload,
      routingKey: RoutingKey.hasAbsenceInInterval,
    });
  }
}
