import { CurrentUserRaw } from '@sawayo/auth';

export interface HasAbsenceInIntervalRequest {
  currentUser: CurrentUserRaw;
  employeeId: string;
  fromDate: string;
  toDate: string;
}
