import { ConverterFormat } from './converter-format.enum';

export interface ConvertFileRequest {
  format: ConverterFormat;
  key: string;
}
