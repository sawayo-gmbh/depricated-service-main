import { ApprovalStatus } from './approval-status.enum';
import { ApprovalType } from './approval-type.enum';

export interface ApprovalRaw {
  _id: string;
  companyId: string;
  entityId: string;
  status?: ApprovalStatus;
  type?: ApprovalType;
  userId: string;
}
