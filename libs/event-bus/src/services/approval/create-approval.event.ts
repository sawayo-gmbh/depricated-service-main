import { ApprovalType } from './approval-type.enum';

export interface CreateApprovalEvent {
  companyId: string;
  entityId: string;
  type: ApprovalType;
  userId: string;
}
