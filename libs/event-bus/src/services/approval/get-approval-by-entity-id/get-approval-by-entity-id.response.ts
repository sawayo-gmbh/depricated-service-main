import { ApprovalRaw } from '../approval-raw.interface';

export interface GetApprovalByEntityIdResponse {
  approval: ApprovalRaw;
}
