import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

import { Exchange, RoutingKey } from '@sawayo/event-bus/common';

import { AmqpProvider } from '../../../common/amqp.provider';

import { GetApprovalByEntityIdRequest } from './get-approval-by-entity-id.request';
import { GetApprovalByEntityIdResponse } from './get-approval-by-entity-id.response';

export class GetApprovalByEntityIdProvider extends AmqpProvider<
  GetApprovalByEntityIdRequest,
  GetApprovalByEntityIdResponse
> {
  constructor(readonly amqpConnection: AmqpConnection) {
    super(amqpConnection);
  }

  request({ currentUser, entityId }: GetApprovalByEntityIdRequest): Promise<GetApprovalByEntityIdResponse> {
    const payload: GetApprovalByEntityIdRequest = {
      currentUser,
      entityId,
    };

    return this.amqpConnection.request<GetApprovalByEntityIdResponse>({
      exchange: Exchange.direct,
      payload,
      routingKey: RoutingKey.approvalByEntityId,
    });
  }
}
