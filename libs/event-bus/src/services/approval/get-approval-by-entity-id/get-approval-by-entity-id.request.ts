import { CurrentUserRaw } from '@sawayo/auth';

export interface GetApprovalByEntityIdRequest {
  currentUser: CurrentUserRaw;
  entityId: string;
}
