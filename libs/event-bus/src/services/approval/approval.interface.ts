import { Types } from 'mongoose';

import { ApprovalStatus } from './approval-status.enum';
import { ApprovalType } from './approval-type.enum';

export interface Approval {
  _id: Types.ObjectId;
  companyId: Types.ObjectId;
  entityId: Types.ObjectId;
  status?: ApprovalStatus;
  type?: ApprovalType;
  userId: Types.ObjectId;
}
