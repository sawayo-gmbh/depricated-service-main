export enum ApprovalType {
  absence = 'ABSENCE',
  driverLicenseCheck = 'DLC',
}
