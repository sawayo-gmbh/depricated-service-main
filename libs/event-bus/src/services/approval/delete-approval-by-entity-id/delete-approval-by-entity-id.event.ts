import { CurrentUserRaw } from '@sawayo/auth';

export interface DeleteApprovalByEntityIdEvent {
  currentUser: CurrentUserRaw;
  entityId: string;
}
