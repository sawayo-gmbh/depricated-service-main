import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

import { Exchange, RoutingKey } from '@sawayo/event-bus/common';

import { AmqpPublisher } from '../../../common';

import { DeleteApprovalByEntityIdEvent } from './delete-approval-by-entity-id.event';

export class DeleteApprovalByEntityIdPublisher extends AmqpPublisher<DeleteApprovalByEntityIdEvent> {
  constructor(readonly amqpConnection: AmqpConnection) {
    super(amqpConnection);
  }

  publish({ currentUser, entityId }: DeleteApprovalByEntityIdEvent): Promise<void> {
    const event: DeleteApprovalByEntityIdEvent = {
      currentUser,
      entityId,
    };

    return this.amqpConnection.publish(Exchange.direct, RoutingKey.deleteApprovalByEntityId, event);
  }
}
