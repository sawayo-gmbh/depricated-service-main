export enum ApprovalStatus {
  approved = 'APPROVED',
  open = 'OPEN',
  rejected = 'REJECTED',
}
