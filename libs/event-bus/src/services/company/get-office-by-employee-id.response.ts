export interface GetOfficeByEmployeeIdResponse {
  _id: string;
  address?: string;
  city?: string;
  isHeadQuarter: boolean;
  name: string;
  postCode?: string;
  state?: string;
}
