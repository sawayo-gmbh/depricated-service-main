export interface DriverLicenseCheckApprovedEvent {
  driverLicenseCheckId: string;
}
