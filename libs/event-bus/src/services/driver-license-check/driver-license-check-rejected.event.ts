export interface DriverLicenseCheckRejectedEvent {
  userId: string;
  driverLicenseCheckId: string;
}
