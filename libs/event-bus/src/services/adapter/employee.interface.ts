import { Types } from 'mongoose';

import { FileData } from '@sawayo/aws';

export interface Employee {
  _id: Types.ObjectId;
  avatar?: FileData;
  email: string;
  employeeId?: number;
  firstName?: string;
  lastName?: string;
  timeTrackingStartDate?: Date;
}
