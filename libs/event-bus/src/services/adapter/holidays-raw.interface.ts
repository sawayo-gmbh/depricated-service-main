export interface HolidaysRaw {
  _id: string;
  allowTransferFromPrevYear?: boolean;
  allowTransferYearChanged?: string;
  firstYearVacationDays: number;
  userId: string;
  vacationDays: number;
}
