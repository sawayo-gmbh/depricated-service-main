import { EmployeeRaw } from '../employee-raw.interface';

export interface GetEmployeeByIdResponse {
  employee: EmployeeRaw;
}
