import { CurrentUserRaw } from '@sawayo/auth';

export interface GetEmployeeByIdRequest {
  currentUser: CurrentUserRaw;
  employeeId: string;
}
