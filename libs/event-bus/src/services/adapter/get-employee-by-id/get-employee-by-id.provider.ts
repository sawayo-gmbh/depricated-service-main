import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

import { Exchange, RoutingKey } from '@sawayo/event-bus/common';

import { AmqpProvider } from '../../../common/amqp.provider';

import { GetEmployeeByIdRequest } from './get-employee-by-id.request';
import { GetEmployeeByIdResponse } from './get-employee-by-id.response';

export class GetEmployeeByIdProvider extends AmqpProvider<GetEmployeeByIdRequest, GetEmployeeByIdResponse> {
  constructor(readonly amqpConnection: AmqpConnection) {
    super(amqpConnection);
  }

  request({ currentUser, employeeId }: GetEmployeeByIdRequest): Promise<GetEmployeeByIdResponse> {
    const payload: GetEmployeeByIdRequest = {
      currentUser,
      employeeId,
    };

    return this.amqpConnection.request<GetEmployeeByIdResponse>({
      exchange: Exchange.direct,
      payload,
      routingKey: RoutingKey.getEmployeeById,
    });
  }
}
