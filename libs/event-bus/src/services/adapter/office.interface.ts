import { Types } from 'mongoose';

export interface OfficeAdapter {
  _id: Types.ObjectId;
  address1?: string;
  address2?: string;
  city?: string;
  company: Types.ObjectId;
  isHeadQuarter: boolean;
  postCode?: string;
  state: string;
  stateDescription?: string;
  users: Types.ObjectId[];
}
