import { Types } from 'mongoose';

export interface Holidays {
  _id: Types.ObjectId;
  allowTransferFromPrevYear?: boolean;
  allowTransferYearChanged?: Date;
  firstYearVacationDays: number;
  userId: Types.ObjectId;
  vacationDays: number;
}
