export interface OfficeAdapterRaw {
  _id: string;
  address1?: string;
  address2?: string;
  city?: string;
  company: string;
  isHeadQuarter: boolean;
  postCode?: string;
  state: string;
  stateDescription?: string;
  users: string[];
}
