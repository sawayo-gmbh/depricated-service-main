import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

import { Exchange, RoutingKey } from '@sawayo/event-bus/common';

import { AmqpProvider } from '../../../common/amqp.provider';

import { GetOfficeByEmployeeIdRequest } from './get-office-by-employee-id.request';
import { GetOfficeByEmployeeIdResponse } from './get-office-by-employee-id.response';

export class GetOfficeByEmployeeIdProvider extends AmqpProvider<
  GetOfficeByEmployeeIdRequest,
  GetOfficeByEmployeeIdResponse
> {
  constructor(readonly amqpConnection: AmqpConnection) {
    super(amqpConnection);
  }

  request({ currentUser, employeeId }: GetOfficeByEmployeeIdRequest): Promise<GetOfficeByEmployeeIdResponse> {
    const payload: GetOfficeByEmployeeIdRequest = {
      currentUser,
      employeeId,
    };

    return this.amqpConnection.request<GetOfficeByEmployeeIdResponse>({
      exchange: Exchange.direct,
      payload,
      routingKey: RoutingKey.adapterGetOfficeByEmployeeId,
    });
  }
}
