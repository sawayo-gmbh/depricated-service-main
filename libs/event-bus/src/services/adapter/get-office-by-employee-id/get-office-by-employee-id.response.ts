import { OfficeAdapterRaw } from '../office-raw.interface';

export interface GetOfficeByEmployeeIdResponse {
  office: OfficeAdapterRaw;
}
