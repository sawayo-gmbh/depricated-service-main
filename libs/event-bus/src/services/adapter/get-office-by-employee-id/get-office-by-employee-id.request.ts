import { CurrentUserRaw } from '@sawayo/auth';

export interface GetOfficeByEmployeeIdRequest {
  currentUser: CurrentUserRaw;
  employeeId: string;
}
