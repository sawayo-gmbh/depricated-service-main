import { FileData } from '@sawayo/aws';

export interface EmployeeRaw {
  _id: string;
  avatar?: FileData;
  email: string;
  employeeId?: number;
  firstName?: string;
  lastName?: string;
  timeTrackingStartDate?: string;
}
