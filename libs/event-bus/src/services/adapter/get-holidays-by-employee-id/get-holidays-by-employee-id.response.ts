import { HolidaysRaw } from '../holidays-raw.interface';

export interface GetHolidaysByEmployeeIdResponse {
  holidays: HolidaysRaw;
}
