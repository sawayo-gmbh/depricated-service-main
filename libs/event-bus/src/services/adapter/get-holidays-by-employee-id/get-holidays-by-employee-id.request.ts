import { CurrentUserRaw } from '@sawayo/auth';

export interface GetHolidaysByEmployeeIdRequest {
  currentUser: CurrentUserRaw;
  employeeId: string;
}
