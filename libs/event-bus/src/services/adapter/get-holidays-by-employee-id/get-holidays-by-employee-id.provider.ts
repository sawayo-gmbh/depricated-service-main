import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

import { Exchange, RoutingKey } from '@sawayo/event-bus/common';

import { AmqpProvider } from '../../../common/amqp.provider';

import { GetHolidaysByEmployeeIdRequest } from './get-holidays-by-employee-id.request';
import { GetHolidaysByEmployeeIdResponse } from './get-holidays-by-employee-id.response';

export class GetHolidaysByEmployeeIdProvider extends AmqpProvider<
  GetHolidaysByEmployeeIdRequest,
  GetHolidaysByEmployeeIdResponse
> {
  constructor(readonly amqpConnection: AmqpConnection) {
    super(amqpConnection);
  }

  request({ currentUser, employeeId }: GetHolidaysByEmployeeIdRequest): Promise<GetHolidaysByEmployeeIdResponse> {
    const payload: GetHolidaysByEmployeeIdRequest = {
      currentUser,
      employeeId,
    };

    return this.amqpConnection.request<GetHolidaysByEmployeeIdResponse>({
      exchange: Exchange.direct,
      payload,
      routingKey: RoutingKey.getHolidaysByEmployeeId,
    });
  }
}
