export enum TrackedTimeKey {
  publicHoliday = 'F',
  afterPublicHoliday = 'Fa',
  timeCompensation = 'KuA',
}
