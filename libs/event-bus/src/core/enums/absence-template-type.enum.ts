export enum AbsenceTemplateType {
  absencePaid = 'absencePaid',
  absenceUnpaid = 'absenceUnpaid',
  custom = 'custom',
  halfDayAbsence = 'halfDayAbsence',
  sickness = 'sickness',
  specialPaid = 'specialPaid',
  specialUnpaid = 'specialUnpaid',
  workTimeAccount = 'workTimeAccount',
}
