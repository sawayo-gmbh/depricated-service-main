import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

export abstract class AmqpPublisher<R> {
  constructor(protected amqpConnection: AmqpConnection) {}

  abstract publish(input: R);
}
