export enum RoutingKey {
  adapterGetOfficeByEmployeeId = 'adapter.getOfficeByEmployeeId',
  approvalByEntityId = 'approvalByEntityId',
  approvalCreateApproval = 'approval.create_approval',
  deleteApprovalByEntityId = 'deleteApprovalByEntityId',
  dlcSettings = 'dlcSettings.get',
  driverLicenseCheckApproved = 'driverLicenseCheck.approved',
  driverLicenseCheckRejected = 'driverLicenseCheck.rejected',
  emailNotification = 'notification.email.*',
  getAbsences = 'getAbsences',
  getAbsencesTemplates = 'getAbsencesTemplates',
  getHolidayStatistics = 'getHolidayStatistics',
  getEmployeeById = 'getEmployeeById',
  getHolidaysByEmployeeId = 'getHolidaysByEmployeeId',
  hasAbsenceInInterval = 'hasAbsenceInInterval',
  hasTrackedTimeInInterval = 'hasTrackedTimeInInterval',
  isIntervalClosed = 'isIntervalClosed',
  nextCheck = 'nextCheck.changeDate',
  oldPushNotification = 'old.push_notification',
  oldWebSocketMessage = 'old.web_socket.message',
  oldWebSocketNotification = 'old.web_socket.notification',
  sendDlcSettings = 'dlcSettings.send',
  workTimeTemplatesUsedInRange = 'workTimeTemplatesUsedInRange',
}

export enum EmailNotificationRoutingKey {
  verifyUser = 'notification.email.verifyUser',
}

export enum UserEventRoutingKey {
  createOrGetUser = 'user.createOrGetUser',
  getUserById = 'user.getUserById',
  getUserByEmail = 'user.getUserByEmail',
}

export enum CompanyEventRoutingKey {
  createCompany = 'company.create',
}

export enum UserRoleEventRoutingKey {
  getUserRole = 'userRole.getUserRole',
}

export enum RoutingKeyAdapter {
  getUserAdapterData = 'get_user_adapter_data',
  getCompanyAdapterData = 'get_company_adapter_data',
}
