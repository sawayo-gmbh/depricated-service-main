import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

export abstract class AmqpProvider<R, T> {
  constructor(protected amqpConnection: AmqpConnection) {}

  abstract request(input: R): Promise<T>;
}
