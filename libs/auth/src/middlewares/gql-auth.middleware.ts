import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { Types } from 'mongoose';

import { UnauthorizedException } from '@sawayo/exceptions';

import {
  HEADER_CONTEXT_COMPANY_ID,
  HEADER_CONTEXT_IP,
  HEADER_CONTEXT_USER_ID,
  HEADER_CONTEXT_USER_ROLE,
} from '../constants';

@Injectable()
export class GqlAuthMiddleware implements NestMiddleware {
  use(request: Request, response: Response, next: NextFunction) {
    const { headers } = request;
    const {
      [HEADER_CONTEXT_COMPANY_ID]: companyId,
      [HEADER_CONTEXT_IP]: ip,
      [HEADER_CONTEXT_USER_ID]: userId,
      [HEADER_CONTEXT_USER_ROLE]: role,
    } = headers;

    if (userId) {
      try {
        (request as any).currentUser = {
          companyId: Types.ObjectId(companyId as string),
          ip,
          role,
          userId: Types.ObjectId(userId as string),
        };
      } catch (error) {
        throw new UnauthorizedException();
      }
    }

    next();
  }
}
