import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';

import { ForbiddenException, UnauthorizedException } from '@sawayo/exceptions';

import { Role } from '../types';

@Injectable()
export class GqlAuthGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const ctx = GqlExecutionContext.create(context);

    const gqlContext = ctx.getContext();

    if (!gqlContext) {
      return true;
    }

    const { currentUser } = gqlContext.req;

    if (!currentUser) {
      throw new UnauthorizedException();
    }

    const roles = this.reflector.get<Role[]>('roles', context.getHandler());

    if (!roles) {
      return true;
    }

    const canActivate = roles.includes(currentUser.role);

    if (!canActivate) {
      throw new ForbiddenException();
    }

    return true;
  }
}
