import { Role } from './role.enum';

export interface CurrentUserRaw {
  companyId: string;
  role: Role;
  userId: string;
}
