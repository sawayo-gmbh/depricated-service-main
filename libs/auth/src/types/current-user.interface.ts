import { Types } from 'mongoose';

import { Role } from './role.enum';

export interface CurrentUser {
  companyId: Types.ObjectId;
  ip?: string;
  role: Role;
  userId: Types.ObjectId;
}
