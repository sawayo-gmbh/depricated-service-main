import bcrypt from 'bcryptjs';

export class PasswordService {
  private readonly saltRounds = 10;
  async hashPassword(password: string) {
    return bcrypt.hash(password, this.saltRounds);
  }

  async comparePasswordHash(password: string, passwordHash: string) {
    return bcrypt.compare(password, passwordHash);
  }
}
