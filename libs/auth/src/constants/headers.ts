export const HEADER_CONTEXT_COMPANY_ID = 'x-company-id';
export const HEADER_CONTEXT_IP = 'x-client-ip';
export const HEADER_CONTEXT_USER_ID = 'x-user-id';
export const HEADER_CONTEXT_USER_ROLE = 'x-user-role';
