import { HttpStatus } from '@nestjs/common';

import { BaseException } from './base.exception';

export class InvalidDatesDifferenceException extends BaseException {
  constructor() {
    super('invalidDatesDifference', HttpStatus.BAD_REQUEST);
  }
}
