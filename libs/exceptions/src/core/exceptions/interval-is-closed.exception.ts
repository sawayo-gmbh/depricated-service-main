import { HttpStatus } from '@nestjs/common';

import { BaseException } from './base.exception';

export class IntervalIsClosedException extends BaseException {
  constructor() {
    super('intervalIsClosed', HttpStatus.BAD_REQUEST);
  }
}
