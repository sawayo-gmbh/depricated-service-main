import { HttpStatus } from '@nestjs/common';

import { BaseException } from './base.exception';

export class BadRequestException extends BaseException {
  constructor() {
    super('badRequest', HttpStatus.BAD_REQUEST);
  }
}
