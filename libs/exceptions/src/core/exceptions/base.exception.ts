import { HttpException } from '@nestjs/common';

export class BaseException extends HttpException {
  protected readonly serializedName: string;

  constructor(serializedName: string, status: number, response = '') {
    super(response, status);

    this.serializedName = serializedName;
  }

  getSerializedName(): string {
    return this.serializedName;
  }
}
