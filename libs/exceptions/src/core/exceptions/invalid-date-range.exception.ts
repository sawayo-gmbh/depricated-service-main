import { HttpStatus } from '@nestjs/common';

import { BaseException } from './base.exception';

export class InvalidDateRangeException extends BaseException {
  constructor() {
    super('invalidDateRange', HttpStatus.BAD_REQUEST);
  }
}
