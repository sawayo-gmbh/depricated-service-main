import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class BaseError {
  @Field(() => Boolean, { nullable: true })
  badRequest?: boolean;

  @Field(() => Boolean, { nullable: true })
  forbidden?: boolean;

  @Field(() => Boolean, { nullable: true })
  notFound?: boolean;

  @Field(() => Boolean, { nullable: true })
  unauthorized?: boolean;
}
