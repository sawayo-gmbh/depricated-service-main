import { ArgumentsHost, Catch } from '@nestjs/common';
import { GqlArgumentsHost, GqlExceptionFilter } from '@nestjs/graphql';

import { BaseException } from '../../core';

// TODO: Remove once dlc and approval services are migrated to new schema
const oldFields = [
  'approve',
  'createDLC',
  'createDriverLicenseCheckSettings',
  'driverLicenseChecksByCompany',
  'driverLicenseCheckSettings',
  'getDLCById',
  'getDlcByUserId',
  'getHistory',
  'reject',
  'triggerDriverLicenseCheck',
  'updateDriverLicenseCheckSettings',
];

@Catch(BaseException)
export class ExceptionFilter implements GqlExceptionFilter {
  isOldService(host: ArgumentsHost): boolean {
    const gqlHost = GqlArgumentsHost.create(host);

    const { fieldName } = gqlHost.getInfo();

    return oldFields.includes(fieldName);
  }

  catch(exception: BaseException, host: ArgumentsHost) {
    const oldService = this.isOldService(host);

    if (oldService) {
      return exception;
    }

    const serializedName = exception.getSerializedName();

    return {
      error: {
        [serializedName]: true,
      },
    };
  }
}
