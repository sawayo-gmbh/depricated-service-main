export enum MimeType {
  csv = 'text/csv',
  doc = 'application/msword',
  docx = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  jpeg = 'image/jpeg',
  jpg = 'image/jpg',
  ods = 'application/vnd.oasis.opendocument.spreadsheet',
  odt = 'application/vnd.oasis.opendocument.text',
  pdf = 'application/pdf',
  png = 'image/png',
  svg = 'image/svg+xml',
  xlsx = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
}
