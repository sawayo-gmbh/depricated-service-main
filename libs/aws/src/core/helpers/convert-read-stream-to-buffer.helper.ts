import { ReadStream } from 'fs';

export const convertReadStreamToBuffer = async (readStream: ReadStream): Promise<Buffer> => {
  const chunks = [];

  for await (const chunk of readStream) {
    chunks.push(chunk);
  }

  return Buffer.concat(chunks);
};
