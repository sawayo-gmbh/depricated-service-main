import { AwsModuleOptions } from './aws-module-options.interface';

export interface AwsModuleOptionsFactory {
  createAwsOptions(): Promise<AwsModuleOptions> | AwsModuleOptions;
}
