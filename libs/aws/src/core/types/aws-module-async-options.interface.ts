import { ModuleMetadata, Type } from '@nestjs/common/interfaces';

import { AwsModuleOptionsFactory } from './aws-module-options-factory.interface';
import { AwsModuleOptions } from './aws-module-options.interface';

export interface AwsModuleAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
  inject?: any[];
  useClass?: Type<AwsModuleOptionsFactory>;
  useExisting?: Type<AwsModuleOptionsFactory>;
  useFactory?: (...args: any[]) => Promise<AwsModuleOptions> | AwsModuleOptions;
}
