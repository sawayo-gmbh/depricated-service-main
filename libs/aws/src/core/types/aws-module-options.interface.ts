import { S3ClientConfig } from '@aws-sdk/client-s3';

export interface AwsModuleOptions extends S3ClientConfig {
  acl: string;
  bucket: string;
  endpoint: string;
  expiresIn: number;
  keyPrefix: string;
}
