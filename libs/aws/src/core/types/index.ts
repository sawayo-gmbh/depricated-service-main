export * from './aws-module-async-options.interface';
export * from './aws-module-options-factory.interface';
export * from './aws-module-options.interface';
export * from './file-data.interface';
