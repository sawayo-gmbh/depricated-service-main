export interface FileData {
  key: string;
  url: string;
}
