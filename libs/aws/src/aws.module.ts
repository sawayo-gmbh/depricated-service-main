import { DynamicModule, Global, Module, Provider } from '@nestjs/common';

import { AwsService } from './application';
import { AWS_MODULE_OPTIONS } from './core/constants';
import { AwsModuleAsyncOptions, AwsModuleOptions } from './core/types';

@Global()
@Module({
  controllers: [],
  exports: [AwsService],
  imports: [],
  providers: [AwsService],
})
export class AwsModule {
  static register(options: AwsModuleOptions): DynamicModule {
    return {
      module: AwsModule,
      providers: [
        {
          provide: AWS_MODULE_OPTIONS,
          useValue: options,
        },
      ],
    };
  }

  static registerAsync(options: AwsModuleAsyncOptions): DynamicModule {
    return {
      imports: options.imports || [],
      module: AwsModule,
      providers: this.createAsyncProviders(options),
    };
  }

  private static createAsyncProviders(options: AwsModuleAsyncOptions): Provider[] {
    if (options.useExisting || options.useFactory) {
      return [this.createAsyncOptionsProvider(options)];
    }

    return [
      this.createAsyncOptionsProvider(options),
      {
        provide: options.useClass,
        useClass: options.useClass,
      },
    ];
  }

  private static createAsyncOptionsProvider(options: AwsModuleAsyncOptions): Provider {
    if (options.useFactory) {
      return {
        inject: options.inject || [],
        provide: AWS_MODULE_OPTIONS,
        useFactory: options.useFactory,
      };
    }

    return {
      inject: [options.useExisting || options.useClass],
      provide: AWS_MODULE_OPTIONS,
      useFactory: async (optionsFactory) => optionsFactory.createAwsOptions(),
    };
  }
}
