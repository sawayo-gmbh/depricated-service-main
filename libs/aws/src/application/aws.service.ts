import {
  DeleteObjectCommand,
  GetObjectCommand,
  HeadBucketCommand,
  PutObjectCommand,
  S3Client,
} from '@aws-sdk/client-s3';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';
import { Inject, Injectable } from '@nestjs/common';
import { ReadStream } from 'fs';

import { AWS_MODULE_OPTIONS } from '../core/constants';
import { convertReadStreamToBuffer } from '../core/helpers';
import { AwsModuleOptions, FileData } from '../core/types';

import {
  DeleteFileParameters,
  GetDefaultNameFromFileNameParameters,
  GetExtensionFromFileNameParameters,
  GetFileParameters,
  GetKeyParameters,
  GetSignedUrlParameters,
  PutFileByKeyParameters,
  PutFileParameters,
  SignUrlParams,
} from './aws-service.interface';

@Injectable()
export class AwsService {
  private readonly acl: string;
  private readonly bucket: string;
  private readonly endpoint: string;
  private readonly expiresIn: number;
  private readonly keyPrefix: string;

  private readonly s3Client: S3Client;

  constructor(@Inject(AWS_MODULE_OPTIONS) private readonly options: AwsModuleOptions) {
    const { acl, bucket, endpoint, expiresIn, keyPrefix, ...s3ClientOptions } = options;

    this.acl = acl;
    this.bucket = bucket;
    this.endpoint = endpoint;
    this.expiresIn = expiresIn;
    this.keyPrefix = keyPrefix;

    this.s3Client = new S3Client(s3ClientOptions);
  }

  private getKey({ companyId, extension, name, namePrefix, subPath }: GetKeyParameters): string {
    const companySegment = companyId ? `${companyId}/` : '';
    const subPathSegment = subPath ? `${subPath}/` : '';
    const namePrefixSegment = namePrefix ? `${namePrefix}_` : '';
    const nameSegment = `${namePrefixSegment}${name || Date.now()}`;
    const extensionSegment = extension ? `.${extension}` : '';

    return `${this.keyPrefix}/${companySegment}${subPathSegment}${nameSegment}${extensionSegment}`;
  }

  async checkBucketPermissions(): Promise<void> {
    const command = new HeadBucketCommand({
      Bucket: this.bucket,
    });

    await this.s3Client.send(command);
  }

  async deleteFile({ key }: DeleteFileParameters): Promise<void> {
    const command = new DeleteObjectCommand({
      Bucket: this.bucket,
      Key: key,
    });

    await this.s3Client.send(command);
  }

  getKeyPrefix() {
    return this.keyPrefix;
  }

  getDefaultNameFromFileName({ name, prefix = '' }: GetDefaultNameFromFileNameParameters): string {
    const extension = this.getExtensionFromFileName({ name });

    return `${prefix}_${Date.now()}.${extension}`;
  }

  getExtensionFromFileName({ name }: GetExtensionFromFileNameParameters): string {
    const splittedFileName = name.split('.');
    const extension = splittedFileName[splittedFileName.length - 1] || '';

    return extension;
  }

  async getFile({ key }: GetFileParameters): Promise<Buffer> {
    const command = new GetObjectCommand({
      Bucket: this.bucket,
      Key: key,
    });

    const object = await this.s3Client.send(command);

    return convertReadStreamToBuffer(object.Body as ReadStream);
  }

  async getSignedUrl({ key }: GetSignedUrlParameters): Promise<string> {
    const command = new GetObjectCommand({
      Bucket: this.bucket,
      Key: key,
    });

    return getSignedUrl(this.s3Client, command, { expiresIn: this.expiresIn });
  }

  async putFile({
    companyId,
    contentType,
    extension,
    file,
    name,
    namePrefix,
    subPath,
  }: PutFileParameters): Promise<FileData> {
    const key = this.getKey({
      companyId,
      extension,
      name,
      namePrefix,
      subPath,
    });

    return this.putFileByKey({ contentType, file, key });
  }

  async putFileByKey({ contentType, file, key }: PutFileByKeyParameters): Promise<FileData> {
    let buffer = file;

    if (buffer instanceof ReadStream) {
      buffer = await convertReadStreamToBuffer(buffer);
    }

    const command = new PutObjectCommand({
      ACL: this.acl,
      Body: buffer,
      Bucket: this.bucket,
      ContentType: contentType,
      Key: key,
    });

    await this.s3Client.send(command);

    const signedUrl = await this.getSignedUrl({ key });

    return {
      key,
      url: signedUrl,
    };
  }

  async signUrl({ fileData }: SignUrlParams) {
    if (!fileData) {
      return null;
    }

    const { key } = fileData;

    const signedUrl = await this.getSignedUrl({ key });

    return {
      key,
      url: signedUrl,
    };
  }
}
