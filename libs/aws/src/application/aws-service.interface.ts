import { PutObjectRequest } from '@aws-sdk/client-s3';
import { Types } from 'mongoose';

import { FileData } from '../core/types';

export interface DeleteFileParameters {
  key: string;
}

export interface GetDefaultNameFromFileNameParameters {
  name: string;
  prefix?: string;
}

export interface GetExtensionFromFileNameParameters {
  name: string;
}

export interface GetFileParameters {
  key: string;
}

export interface GetKeyParameters {
  companyId?: Types.ObjectId;
  extension?: string;
  name?: string;
  namePrefix?: string;
  subPath?: string;
}

export interface GetSignedUrlParameters {
  key: string;
}

export interface PutFileParameters {
  companyId?: Types.ObjectId;
  contentType?: string;
  extension?: string;
  file: PutObjectRequest['Body'] | Buffer;
  name?: string;
  namePrefix?: string;
  subPath?: string;
}

export interface PutFileByKeyParameters {
  contentType?: string;
  file: PutObjectRequest['Body'] | Buffer;
  key: string;
}

export interface SignUrlParams {
  fileData: FileData;
}
